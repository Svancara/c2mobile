import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TradesComponent } from './trades.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [TradesComponent],
  exports: [TradesComponent]
})
export class TradesModule { }
