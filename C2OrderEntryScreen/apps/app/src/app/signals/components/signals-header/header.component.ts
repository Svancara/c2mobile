import { Component, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-signals-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SignalsHeaderComponent  {

  @Input() strategyId: number;
  @Input() strategyName: string = "";
  @Output() settingsEvent = new EventEmitter();

  settings() {
    this.settingsEvent.emit();
  }

}
