import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SignalsHeaderComponent } from './signals-header/header.component';
import { SignalsListComponent } from './signals-list/signals-list.component';
import { SignalDetailComponent } from './signal-detail/signal-detail.component';

import { PrimeNgModule } from '@app/primeng.module';

import { SharedModule } from '@app/shared/shared.module';
import { CoreModule } from '@app/core';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    PrimeNgModule,
    SharedModule,
  ],
  declarations: [
    SignalsHeaderComponent,
    SignalDetailComponent,
    SignalsListComponent,
  ],
  exports: [SignalsListComponent],
})
export class ComponentsModule { }
