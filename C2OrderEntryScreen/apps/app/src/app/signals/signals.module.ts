import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { PrimeNgModule } from '../primeng.module';
import { ComponentsModule } from './components/components.module';
import { reducers } from '@app/store/reducers/signals';

import { SignalsCollectionEffects } from '@app/store/effects/signals.effects';
import { SignalsService } from '@libs/signals-service/';
import { ViewSignalsContainerComponent } from './containers/signals-route-container/signals-route-container.component';

import { SignalContainerComponent } from './containers/signals-list-page/signals-page.component';
import { CommonSignalsModule } from "@libs/signals";


@NgModule({
  imports: [
    CommonModule,
    PrimeNgModule,
    ComponentsModule,
    CommonSignalsModule,
    RouterModule.forChild([
      { path: 'signals', component: SignalContainerComponent },
      {
        path: ':id',
        component: SignalContainerComponent,
        // canActivate: [StrategyLoaded],
      },
      
      { path: 'signal', component: SignalContainerComponent },
      {
        path: ':id',
        component: SignalContainerComponent,
        // canActivate: [BookExistsGuard],
      },
      
      { path: '', component: SignalContainerComponent },
    ]),

    /**
     * StoreModule.forFeature is used for composing state
     * from feature modules. These modules can be loaded
     * eagerly or lazily and will be dynamically added to
     * the existing state.
     */
    StoreModule.forFeature('workingSignals', reducers),

    /**
     * Effects.forFeature is used to register effects
     * from feature modules. Effects can be loaded
     * eagerly or lazily and will be started immediately.
     *
     * All Effects will only be instantiated once regardless of
     * whether they are registered once or multiple times.
     */
    EffectsModule.forFeature([
      SignalsCollectionEffects
    ]),
  ],
  declarations: [
    SignalContainerComponent,
    SignalContainerComponent,
    ViewSignalsContainerComponent],
    providers: [SignalsService]

})
export class SignalsModule { }
