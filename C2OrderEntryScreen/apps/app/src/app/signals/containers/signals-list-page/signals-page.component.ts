import { Component, OnInit } from '@angular/core';
//import { Store } from '@ngrx/store';
//import { Observable } from 'rxjs/Observable';
//import { Subscription } from 'rxjs/Subscription';
//import 'rxjs/add/operator/first';

//import * as fromSignals from '@app/store/reducers/signals';
//import { Load as loadSignalsAction } from '@app/store/actions/signals/signals.collection.actions';
//import { Remove as CancelSignalAction } from '@app/store/actions/signals/signals.collection.actions';
import { Signal } from '@app/models';

//import { IStrategy } from '@app/models';
//import * as fromStrategies from '@app/store/reducers/user-strategies';


import { SignalsSandbox} from '@libs/signals';

@Component({
  selector: 'app-signals-page',
  templateUrl: './signals-page.component.html',
  styleUrls: ['./signals-page.component.css'],
})
export class SignalContainerComponent implements OnInit {

  constructor(public sandbox$ : SignalsSandbox) {}

  ngOnInit() {
    this.sandbox$.loadSignals() 
  }

  cancelSignal(signal: Signal) {
    this.sandbox$.cancelSignal(signal);
  }
}
