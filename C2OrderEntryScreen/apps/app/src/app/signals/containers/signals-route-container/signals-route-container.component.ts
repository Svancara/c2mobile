import { Component, OnDestroy, ChangeDetectionStrategy } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';

import * as fromStrategies from '@app/store/reducers/user-strategies';
import * as fromSignals from '@app/store/actions/signals/signal.actions';

@Component({
  selector: 'app-view-signals-container',
  templateUrl: './signals-route-container.component.html',
  styleUrls: ['./signals-route-container.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ViewSignalsContainerComponent implements OnDestroy {
  actionsSubscription: Subscription;

  constructor(store: Store<fromStrategies.State>, route: ActivatedRoute) {
    this.actionsSubscription = route.params
      .map(params => new fromSignals.Select(params.id))
      .subscribe(store);
  }

  ngOnDestroy() {
    this.actionsSubscription.unsubscribe();
  }
}
