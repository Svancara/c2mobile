import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { PositionsHeaderComponent } from './positions-header/positions-header.component';
import { PositionsListComponent } from './positions-list/positions-list.component';


import { PrimeNgModule } from '../../primeng.module';

import { SharedModule } from '@app/shared/shared.module';
import { CoreModule } from '@app/core';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    PrimeNgModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
  ],
  declarations: [
  PositionsHeaderComponent,
  PositionsListComponent,
  //PositionDetailComponent,
],
  exports: [
    PositionsListComponent,
  ],
})
export class ComponentsModule { }
