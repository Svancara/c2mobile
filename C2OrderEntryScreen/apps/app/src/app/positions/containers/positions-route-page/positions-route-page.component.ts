import { Component, OnDestroy, ChangeDetectionStrategy } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';

import { Select } from '@app/store/actions/positions/position';
import { State } from '@app/store/reducers/positions';

@Component({
  selector: 'app-positions-route-page',
  templateUrl: './positions-route-page.component.html',
  styleUrls: ['./positions-route-page.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PositionsRoutePageComponent implements OnDestroy {
  actionsSubscription: Subscription;

  constructor(store: Store<State>, route: ActivatedRoute) {
    this.actionsSubscription = route.params
      .map(params => new Select(params.id))
      .subscribe(store);
  }

  ngOnDestroy() {
    this.actionsSubscription.unsubscribe();
  }
}
