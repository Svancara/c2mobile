import { Component, OnInit } from '@angular/core';
//import { Store } from '@ngrx/store';
//import { Observable } from 'rxjs/Observable';
//import { Subscription } from 'rxjs/Subscription';

//import 'rxjs/add/operator/first';

import { Position } from '@app/models';

//import {
//  Load as loadPositionsAction,
//  Remove as ClosePositionAction
//} from '@app/store/actions/positions/positions.collection';

//import { getAllPositions, getCollectionLoading } from '@app/store/reducers/positions';
//import { getSelectedStrategy } from '@app/store/reducers/user-strategies/';
//import { Remove } from '@app/store/actions/positions/positions.collection';

import { PositionsSandbox} from '@libs/positions';


@Component({
  selector: 'app-positions-list-page',
  templateUrl: './positions-list.container.html',
  styleUrls: ['./positions-list.container.css'],
})
export class PositionsListPageComponent implements OnInit {

  constructor(public sandbox$: PositionsSandbox) {
  }

  ngOnInit() {
    this.sandbox$.loadPositions();
  }

  /**
  * Send signal closePosition 
  */
  closePosition(position: Position) {
    this.sandbox$.closePosition(position);
  }


}
