import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { IAuthenticate } from '@app/models/user';
import * as fromAuth from '@app/store/reducers/auth';
import * as Auth from '@app/store/actions/auth.action';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styles: [],
})
export class LoginPageComponent implements OnInit {
  // The following is when using AUth mechanism from angular-architecture-patterns
  //pending$ = this.store.select(fromAuth.getLoginPagePending);
  //error$ = this.store.select(fromAuth.getLoginPageError);

  constructor(private store: Store<fromAuth.State>) { }

  ngOnInit() { }

  onLoginSubmit($event: IAuthenticate) {
    this.store.dispatch(new Auth.Login($event));
  }

  onLogout($event) {
    this.store.dispatch(new Auth.Logout());
  }
}
