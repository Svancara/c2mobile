import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
// import { FormControl, FormGroup, FormBuilder } from '@angular/forms';

import { IAuthenticate } from '@app/models/user';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css'],  
})
export class LoginFormComponent implements OnInit {

  //@Input()
  //set pending(isPending: boolean) {
  //  if (isPending) {
  //    this.form.disable();
  //  } else {
  //    this.form.enable();
  //  }
  //}

  @Input() errorMessage: string | null;
  @Output() submitted = new EventEmitter<IAuthenticate>();
  @Output() logoutSubmitted = new EventEmitter<any>();

  
  // public userform: FormGroup;

  public email: string;
  public password: string;
   

  constructor() {}

  ngOnInit() {}

  onLoginClicked() {
    const request: IAuthenticate = { email: this.email, password: this.password };
    this.submitted.emit(request);
  }

  onLogout() {
    this.logoutSubmitted.emit();
  }

  settings() {
     alert('LoginFormComponent says - NOT YET IMPLEMENTED');
  }
}
