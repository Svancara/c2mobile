import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrimeNgModule } from '../../primeng.module';

import { LoginScreenHeaderComponent } from './login-form/header/header.component';
import { LoginFormComponent } from './login-form/login-form.component';

import { SharedModule } from '@app/shared/shared.module';
import { CoreModule } from '@app/core';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    PrimeNgModule,
    SharedModule,
  ],
  declarations: [LoginScreenHeaderComponent, LoginFormComponent],
  exports: [LoginFormComponent]
})
export class ComponentsModule { }
