import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
//import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { PrimeNgModule } from '../primeng.module';

import { ComponentsModule } from './components/components.module';
import { LoginPageComponent } from './containers/login-page.component';
import { AuthService } from '@app/core/services/auth.service';
import { AuthGuard } from '@app/core/services/auth-guard.service';
import { AuthEffects } from '@app/store/effects/auth.effects';
import { reducers } from '@app/store/reducers/auth';

const COMPONENTS = [LoginPageComponent];

@NgModule({
  imports: [
    CommonModule,
   // FormControl, FormGroup, FormBuilder ,
    FormsModule,
    //ReactiveFormsModule,
    PrimeNgModule,
    ComponentsModule,
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
})
export class AuthModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: RootAuthModule,
      providers: [AuthService, AuthGuard],
    };
  }
}

@NgModule({
  imports: [
    AuthModule,
    RouterModule.forChild([
      { path: 'login', component: LoginPageComponent },
      //{ path: '', component: LoginPageComponent },
    ]),
    StoreModule.forFeature('auth', reducers),
    EffectsModule.forFeature([AuthEffects]),
  ],
})
export class RootAuthModule {}
