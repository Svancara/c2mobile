import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/pluck';
import { Component, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';

import * as fromStrategies from '@app/store/reducers/user-strategies';
import * as fromStrategy from '@app/store/actions/user-strategy.actions';

/**
 * Note: Container components are also reusable. Whether or not
 * a component is a presentation component or a container
 * component is an implementation detail.
 *
 * The View Book Page's responsibility is to map router params
 * to a 'Select' book action. Actually showing the selected
 * book remains a responsibility of the
 * SelectedBookPageComponent
 */
@Component({
  selector: 'app-view-selected-strategy-container',
  templateUrl: './view-selected-strategy-container.component.html',
  styleUrls: ['./view-selected-strategy-container.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ViewSelectedStrategyContainerComponent implements OnDestroy {

  actionsSubscription: Subscription;

  constructor(store: Store<fromStrategies.State>, route: ActivatedRoute) {
    this.actionsSubscription = route.params
      .map(params => new fromStrategy.Select(params.id))
      .subscribe(store);
  }

  ngOnDestroy() {
    if (this.actionsSubscription) {
      this.actionsSubscription.unsubscribe();
    }
  }
}
