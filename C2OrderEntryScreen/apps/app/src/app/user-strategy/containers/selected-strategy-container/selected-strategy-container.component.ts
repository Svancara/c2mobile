/**
 * Note: Container components are also reusable. Whether or not
 * a component is a presentation component or a container
 * component is an implementation detail.
 *
 * The View Book Page's responsibility is to map router params
 * to a 'Select' book action. Actually showing the selected
 * book remains a responsibility of the
 * SelectedBookPageComponent
 */

import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as fromBooks from '@app/store/reducers/user-strategies';
import * as collection from '@app/store/actions/user-strategies.collection.actions';
import { IStrategy } from '@app/models';

@Component({
  selector: 'app-selected-strategy-container',
  templateUrl: './selected-strategy-container.component.html',
  styleUrls: ['./selected-strategy-container.component.css']

  //selector: 'bc-selected-book-page',
  //changeDetection: ChangeDetectionStrategy.OnPush,
  //template: `
  //  <bc-book-detail
  //    [book]="book$ | async"
  //    [inCollection]="isSelectedBookInCollection$ | async"
  //    (add)="addToCollection($event)"
  //    (remove)="removeFromCollection($event)">
  //  </bc-book-detail>
  //`,
})
export class SelectedStrategyContainerComponent {

  strategy$: Observable<IStrategy>;

  constructor(private store: Store<fromBooks.State>) {
    this.strategy$ = store.select(fromBooks.getSelectedStrategy);
  }

  /*
  addToCollection(book: Book) {
    this.store.dispatch(new collection.AddBook(book));
  }

  removeFromCollection(book: Book) {
    this.store.dispatch(new collection.RemoveBook(book));
  }
  */
}

