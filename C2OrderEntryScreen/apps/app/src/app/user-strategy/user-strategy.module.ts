import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { ComponentsModule } from './components/components.module';
import { PrimeNgModule } from '../primeng.module';
import { reducers } from '@app/store/reducers/user-strategies';

import { SelectedStrategyContainerComponent } from './containers/selected-strategy-container/selected-strategy-container.component';
import { ViewSelectedStrategyContainerComponent } from './containers/view-selected-strategy-container/view-selected-strategy-container.component';

import { UserStrategyEffects } from '@app/store/effects/user-strategy.effects';
import { NotFoundPageComponent } from '@app/core/containers/not-found-page';

@NgModule({
  imports: [
    CommonModule,
    PrimeNgModule,
    ComponentsModule,
    RouterModule.forChild([
      { path: '', redirectTo: 'strategy', pathMatch: 'full' },
      { path: 'strategy'},
      { path: ':id', component: ViewSelectedStrategyContainerComponent },
      { path: '**', component: NotFoundPageComponent},
    ]),

    /**
     * StoreModule.forFeature is used for composing state
     * from feature modules. These modules can be loaded
     * eagerly or lazily and will be dynamically added to
     * the existing state.
     */
    StoreModule.forFeature('userStrategy', reducers),

    /**
     * Effects.forFeature is used to register effects
     * from feature modules. Effects can be loaded
     * eagerly or lazily and will be started immediately.
     *
     * All Effects will only be instantiated once regardless of
     * whether they are registered once or multiple times.
     */
    EffectsModule.forFeature([
      UserStrategyEffects
    ]),
  ],
  declarations: [
    SelectedStrategyContainerComponent,
    ViewSelectedStrategyContainerComponent],
  providers: []

})
export class UserStrategyModule{ }
