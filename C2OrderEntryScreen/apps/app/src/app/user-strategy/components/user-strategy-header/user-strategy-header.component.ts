import { Component, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-user-strategy-header',
  templateUrl: './user-strategy-header.component.html',
  styleUrls: ['./user-strategy-header.component.css']
})
export class UserStrategyHeaderComponent {

  @Input() strategyId: number;
  @Input() strategyName: string = "";
  @Output() settingsEvent = new EventEmitter();

  settings() {
    this.settingsEvent.emit();
  }

}
