import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { IStrategy } from '@app/models';

import { MenuItem } from 'primeng/components/common/menuitem';

@Component({
  selector: 'app-selected-strategy',
  templateUrl: './selected-strategy.component.html',
  styleUrls: ['./selected-strategy.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectedStrategyComponent {

  @Input() strategy: IStrategy;

  constructor() { }

 
  settings() {
    alert("SelectedStrategyComponent says: Settings clicked");
  }
}
