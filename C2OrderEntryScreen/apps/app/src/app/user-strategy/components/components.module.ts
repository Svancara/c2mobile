import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// import { UserStrategyComponent } from './user-strategy/user-strategy.component';
import { UserStrategyHeaderComponent } from './user-strategy-header/user-strategy-header.component';
import { SelectedStrategyComponent } from './selected-strategy/selected-strategy.component';

import { PrimeNgModule } from '../../primeng.module';
import { SharedModule } from '@app/shared/shared.module';
import { CoreModule } from '@app/core';

const COMPONENTS = [
  UserStrategyHeaderComponent,
  SelectedStrategyComponent,
];

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    PrimeNgModule,
    SharedModule,
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
})
export class ComponentsModule { }
