import { Component, Input, ChangeDetectionStrategy} from '@angular/core';
/*
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import 'rxjs/add/operator/first';

import { IStrategy } from '@app/models';
import * as fromStrategies from '@app/store/reducers/user-strategies';
*/
@Component({
  selector: 'app-header-container',
  templateUrl: './appheader.container.html',
  styleUrls: ['./appheader.container.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppheaderContainer {

  @Input() title = '';
  @Input() gohome: boolean = false;
  @Input() strategyId = "";
  @Input() strategyName = "";

  //public strategy$: Observable<IStrategy>;

  //constructor(private store: Store<any>) {
  //  this.strategy$ = this.store.select(fromStrategies.getSelectedStrategy);
  //}
}
