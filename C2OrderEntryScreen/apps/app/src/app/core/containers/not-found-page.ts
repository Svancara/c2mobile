import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'not-found-page',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
<div class="ui-g">
  <div class="ui-g-12">
     <p>Hey! It looks like this page doesn't exist yet.</p>    
  </div>
  <div class="ui-g-12">
      <button pButton type="button" routerLink="/" class="ui-button-warning" icon="fa-frown-o" label="Take Me Home"></button>
  </div>

</div>
`,
/*
  template: `
    <mat-card>
      <mat-card-title>404: Not Found</mat-card-title>
      <mat-card-content>
        <p>Hey! It looks like this page doesn't exist yet.</p>
      </mat-card-content>
      <mat-card-actions>
        <button mat-raised-button color="primary" routerLink="/">Take Me Home</button>
      </mat-card-actions>
    </mat-card>
  `,
  */
  styles: [
    `
    :host {
      text-align: center;
    }
  `,
  ],
})
export class NotFoundPageComponent { }
