import { Component, Input, ChangeDetectionStrategy} from '@angular/core';

//import { Strategy } from '@app/models';
import { UserStrategyRoute } from '@app/globals';
//import { HomeRoute } from '@app/globals';

@Component({
  selector: 'app-appheader',
  templateUrl: './appheader.component.html',
  styleUrls: ['./appheader.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppheaderComponent {

  public userStrategyRoute = UserStrategyRoute;
  
  @Input() title = 'Collective2';
  @Input() strategyName = '';
  @Input() strategyId: number;
  @Input() gohome: boolean = false;
  
  //@Output() settingsAction = new EventEmitter();

}
