import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppheaderComponent } from './appheader/appheader.component';
import { SharedModule } from '@app/shared/shared.module';

import { PrimeNgModule } from '@app/primeng.module';

const COMPONENTS = [
  AppheaderComponent,
];

@NgModule({
  imports: [
    CommonModule,
    PrimeNgModule,
    SharedModule
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
})
export class CoreComponentsModule { }
