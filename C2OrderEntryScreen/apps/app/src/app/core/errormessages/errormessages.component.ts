import { Component, OnInit } from '@angular/core';
import { Store } from "@ngrx/store";
//import { ApplicationState } from "../store/application-state";
import { Observable } from "rxjs/Observable";
import { Subscription } from "rxjs/Subscription";

import { Message } from "primeng/components/common/message";

@Component({
	selector: 'app-errormessages',
	templateUrl: './errormessages.component.html',
	styleUrls: ['./errormessages.component.css']
})
export class ErrorMessagesComponent implements OnInit {

	private msgs$: Observable<Message[]>;
	public msgs: Message[];
	private subscription: any;

	constructor(private store: Store<any>) {
		this.msgs$ = store.select(state => this.msgs = state.ErrorMessages);
	}

	ngOnInit() {
		//this.subscription = this.msgs$.subscribe((errors: Message[]) => this.msgs.push(errors));
		this.subscription = this.msgs$.subscribe();
	}

	//showError() {
	//	this.msgs = [];
	//	this.msgs.push({ severity: 'error', summary: 'Error Message', detail: 'Validation failed' });
	//}
}
