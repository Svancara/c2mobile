import { Action } from '@ngrx/store';
import { Message } from 'primeng/components/common/message';

export const INITIAL_ERROR_STATE: Message[] = [];
export const ERROR_HTTP_SAVE = 'ERROR_HTTP_SAVE';

export class ErrorAction implements Action {

	readonly type: string = ERROR_HTTP_SAVE;

	constructor(public message: Message) { }
}

export type Actions = ErrorAction;


