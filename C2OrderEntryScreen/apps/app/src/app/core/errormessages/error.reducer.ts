import { Action } from '@ngrx/store';
import { Message } from 'primeng/components/common/message';
// import * as _ from 'lodash';
import * as fromError from './errorAction';

export interface State {
  errorMessages: Message[];
}

const initialState: State = {
  errorMessages : [],
};


export function reducer(state = initialState, action: fromError.Actions): State {
  switch (action.type) {
    case fromError.ERROR_HTTP_SAVE:
      return {
        errorMessages : [action.message]
      }
		default:
			return state;
	}
}

export const getErrors = (state: State) => state.errorMessages;
