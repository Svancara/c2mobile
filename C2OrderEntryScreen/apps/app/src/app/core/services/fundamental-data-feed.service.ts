import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Store } from '@ngrx/store';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';

import { environment } from '@env/environment';
import { ApiSystemRequest } from '@app/models/apiKey_systemId';
import { State as AuthState } from '@app/store/reducers/auth/auth';
import * as authReducers from '@app/store/reducers/auth';
import { NotificationsService } from 'angular2-notifications';
import { Environment, GetLastQuote } from '@app/models';
import { GetFundamentalData, FundamentalDataResponse } from '@app/models/LastQuoteService.dtos';
import { LoggerService } from '@app/core/services/logger.service';
import { Utils } from '@app/utils';

@Injectable()
export class FundamentalDataService {

  private config: Environment;

  constructor(
    private store: Store<AuthState>,
    private http: HttpClient,
    private notificationsService: NotificationsService,
    private logger: LoggerService
    ) {
    this.config = environment;
  }


  //private getApiKey(): string {
  //  let apikey = '';
  //  this.store.select(authReducers.getApiKey).first().subscribe(apikeyState => apikey = apikeyState);
  //  return apikey;
  //}


  
  /**
 * Get fundamental data
 * @param strategyId
 */
//  public getFundamentalData(request: FundamentalData): Observable<FundamentalDataResponse> {
  public getFundamentalData(request: GetLastQuote): Observable<FundamentalDataResponse> {
    const url = this.config.C2FundametalDataApiUrl + "/getFundamentalData";
    //let payload: FundamentalData = request;
    let payload: GetLastQuote = request;
    payload.symbol = payload.symbol.toUpperCase();
    payload.typeofsymbol = payload.typeofsymbol.toLowerCase();
    payload.market = payload.market.toUpperCase();
    payload.apikey = Utils.getApiKey(this.store);

    //if ((payload.typeofsymbol === "forex") && (!payload.symbol.endsWith(".FXCM"))) {
    //  payload.symbol = payload.symbol + ".FXCM";
    //}


    // payload.apiKey = this.getApiKey();
    return this.http
      .post<FundamentalDataResponse>(url, payload)
      .map(response => response)
      // No errors handling here. Must be catched in the @Effect. Or it dies...
  }

}
