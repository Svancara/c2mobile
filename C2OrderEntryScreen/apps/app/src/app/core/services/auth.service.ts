import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';

import { of } from 'rxjs/observable/of';
import { _throw } from 'rxjs/observable/throw';
import { IAuthenticate } from '@app/models/user';
import { Observable } from 'rxjs/Observable';
import { environment } from '@env/environment';
import { Environment, ILoginResponse } from '@app/models';

import { State as AuthState } from '@app/store/reducers/auth/auth';
import * as authReducers from '@app/store/reducers/auth';

@Injectable()
export class AuthService {

  private config: Environment;

  public redirectUrl: string = undefined;

  constructor(
    private store: Store<AuthState>,
    private http: HttpClient) {
    this.config = environment;
  }

  //    return of(new User({ name: 'User', apikey: userEnteredData.fakeApikeyField, isLoggedIn:true }));
  //    https://api.collective2.com/world/apiv3/requestAPIkey


  login(userEnteredData: IAuthenticate): Observable<ILoginResponse> {

    const url = "https://api.collective2.com/world/apiv3/requestAPIkey";

    return this.http
      .post<ILoginResponse>(url, userEnteredData)
      .map(response => response);
  }

}
