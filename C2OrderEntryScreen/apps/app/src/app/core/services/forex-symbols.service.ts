import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { environment } from '@env/environment';
import { Environment } from '@app/models';
//import { ApiSystemRequest } from '@app/models/apiKey_systemId';
import { State as AuthState } from '@app/store/reducers/auth/auth';
import * as state from '@app/store';

import { NotificationsService } from 'angular2-notifications';
import { Utils } from '@app/utils';
import { ForexPairs, ForexPairsResponse } from '@app/models/LastQuoteService.dtos';

@Injectable()
export class ForexSymbolsService {

  private config: Environment;

  constructor(
    private store: Store<AuthState>,
    private http: HttpClient,
    private notificationsService: NotificationsService) {
    this.config = environment;
  }

  
  /**
   * getPositions
   * @param strategyId
   */
  public getForexPairs(): Observable<ForexPairsResponse> {
    const url = this.config.C2SymbolSearchApiUrl + "/forexpairs";
    let payload: ForexPairs = new ForexPairs();
    payload.apikey = Utils.getApiKey(this.store);
    return this.http
      .post<ForexPairsResponse>(url, payload)
      .map(response => response)
      // No errors handling here. Must be catched in the @Effect.
      ;
  }


}
