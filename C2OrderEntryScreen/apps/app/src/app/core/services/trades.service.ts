import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { } from '@app/models';

import { environment } from '@env/environment';
import { Environment } from '@app/models';
import { ApiSystemRequest } from '@app/models/apiKey_systemId';
import { State as AuthState } from '@app/store/reducers/auth/auth';
import * as authReducers from '@app/store/reducers/auth';

import { NotificationsService } from 'angular2-notifications';
import { Utils } from '@app/utils';
import { ClosedTradesResponse, ClosedTrades } from '@app/models/LastQuoteService.dtos';

@Injectable()
export class TradesService {

  private config: Environment;

  constructor(
    private store: Store<AuthState>,
    private http: HttpClient,
    private notificationsService: NotificationsService) {
    this.config = environment;
  }




  public getPositions(strategyId: number, lazy: boolean, first: number, rows: number, sortField: string, sortOrder: number): Observable<ClosedTradesResponse> {
    const url = this.config.C2SymbolSearchApiUrl + "/closedtrades";
    const payload: ClosedTrades = new ClosedTrades();

    payload.systemid = strategyId;
    payload.apikey = Utils.getApiKey(this.store);
    payload.Lazy = lazy;
    payload.First = first;
    payload.Rows = rows;
    payload.SortField = sortField;
    payload.SortOrder = sortOrder;
    payload.show_component_signals = "1";

    return this.http
      .post<ClosedTradesResponse>(url, payload)
      .map(response => response)
      // No errors handling here. Must be catched in the @Effect.
      ;
  }


}
