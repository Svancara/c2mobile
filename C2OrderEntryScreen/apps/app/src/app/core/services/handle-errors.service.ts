import { Injectable } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';
import { LoggerService } from '@app/core/services/logger.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class HandleErrorsService {

  constructor(
    private logger: LoggerService,
    private notificationsService: NotificationsService) { }

  public handleHttpError(status: HttpErrorResponse): Observable<any> {
    this.notificationsService.error(status.name, status.message, { timeOut: 5000, showProgressBar: true, pauseOnHover: true });
    return Observable.throw(status);
  }


}
