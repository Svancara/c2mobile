import 'rxjs/add/operator/take';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import * as Auth from '@app/store/actions/auth.action';
import * as fromAuth from '@app/store/reducers/auth';
import { NotificationsService } from 'angular2-notifications';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private store: Store<fromAuth.State>,
    private router: Router,
    private translate: TranslateService,
    private notificationsService: NotificationsService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    let url: string = state.url;
    return this.store
      .select(fromAuth.getLoggedIn)
      .map(authed => {
        if (!authed) {
          this.translate.get('ServerError401').subscribe((res: string) => {
            this.notificationsService.warn(res, '', { timeOut: 5000, showProgressBar: true, pauseOnHover: true });
            this.store.dispatch(new Auth.LoginRedirect(url));
            return false;
          });
        }
        return true;
      })
      .take(1);
  }
}
