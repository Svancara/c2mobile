import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { ISendSignal, Signal, ISendSignalResponse, Position, CancelSignalResponse } from '@app/models';
import * as loggerActions from '@app/store/actions/logger.actions';
// import { State } from '@app/store/reducers/logger.reducer';

@Injectable()
export class LoggerService {

  constructor(private store: Store<any>) { }

  public logSignal(signal: ISendSignal): void {
    this.store.dispatch(new loggerActions.SaveSignal(signal));
  }

  public logPositionClose(position: Position): void {
    this.store.dispatch(new loggerActions.ClosePosition(position));
  }

  public logSignalResponse(signalId: number, response: ISendSignalResponse): void {
    this.store.dispatch(new loggerActions.SaveSignalResponse(signalId, response));
  }

  public logSignalCancel(signal: Signal): void {
    this.store.dispatch(new loggerActions.SaveSignalCancellation(signal));
  }

  public logSignalCancelResponse(signalId: number, response : CancelSignalResponse): void {
    this.store.dispatch(new loggerActions.SaveSignalCancellationResponse(signalId, response));
  }

  public SaveErrorItem(info: any): void {
    this.store.dispatch(new loggerActions.SaveErrorItem(info));
  }

}
