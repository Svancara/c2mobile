import 'rxjs/add/operator/map';


import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import {
  SymbolSearch, SymbolSearchResponse,
  OptionSymbolsSearch, OptionSymbolsSearchResponse, 
  OptionSearchByExpirationResponse, OptionSearchByExpiration,
  OptionSearchByStrike, OptionSearchByStrikeResponse
} from '@app/models/LastQuoteService.dtos';

import { environment } from '@env/environment';
import { Environment } from '@app/models';
import { ApiSystemRequest } from '@app/models/apiKey_systemId';
// import { NotificationsService } from 'angular2-notifications';
import { Utils } from '@app/utils';
import { State as AuthState } from '@app/store/reducers/auth/auth';
import * as authReducers from '@app/store/reducers/auth';


@Injectable()
export class SearchService {

  private config: Environment;
  constructor(
    private store: Store<AuthState>,
    private http: HttpClient) {
    this.config = environment;
  }


  public symbolSearch(searchSpec: SymbolSearch): Observable<SymbolSearchResponse> {
    const url = this.config.C2SymbolSearchApiUrl + "/symbolsearch";

    /*
    let payload: SymbolSearch = new SymbolSearch();
    payload.apiKey = Utils.getApiKey(this.store);
    payload.instrument = Utils.prepInstrument(searchSpec.instrument);
    // no. Can be LSE, ASX,... payload.market =  Utils.prepMarket(searchSpec.market);
    payload.query = searchSpec.query;
    */

    let payload: SymbolSearch = searchSpec;
    payload.apiKey = Utils.getApiKey(this.store);

    return this.http
      .post<SymbolSearchResponse>(url, payload)
      .map(response => response)
      // no .catch(err => Observable.throw(err))
      ;
  }

  public optionSymbolsSearch(searchSpec: OptionSymbolsSearch): Observable<OptionSymbolsSearchResponse> {
    const url = this.config.C2SymbolSearchApiUrl + "/optionssearch";

    let payload: OptionSymbolsSearch = searchSpec;
    payload.apikey = Utils.getApiKey(this.store);

    return this.http
      .post<OptionSymbolsSearchResponse>(url, payload)
      .map(response => response)
      // no .catch(err => Observable.throw(err))
      ;
  }

  public optionsByExpirations(searchSpec: OptionSearchByExpiration): Observable<OptionSearchByExpirationResponse> {
    const url = this.config.C2SymbolSearchApiUrl + "/optionsByExpiration";

    let payload: OptionSearchByExpiration= searchSpec;
    payload.apikey = Utils.getApiKey(this.store);

    return this.http
      .post<OptionSearchByExpirationResponse>(url, payload)
      .map(response => response)
      // no .catch(err => Observable.throw(err))
      ;
  }

  
  public optionsByStrikes(searchSpec: OptionSearchByStrike): Observable<OptionSearchByStrikeResponse> {
    const url = this.config.C2SymbolSearchApiUrl + "/optionsByStrike";

    let payload: OptionSearchByStrike = searchSpec;
    payload.apikey = Utils.getApiKey(this.store);

    return this.http
      .post<OptionSearchByStrikeResponse>(url, payload)
      .map(response => response)
      // no .catch(err => Observable.throw(err))
      ;
  }


}
