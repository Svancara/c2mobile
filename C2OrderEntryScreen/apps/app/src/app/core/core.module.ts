import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { RouterModule } from '@angular/router';

import { AppheaderContainer } from './containers/appheader/appheader.container';
import { NotFoundPageComponent } from './containers/not-found-page';

import { ErrorMessagesComponent } from './errormessages/errormessages.component';

// import { MaterialModule } from '../material';
import { PrimeNgModule } from '../primeng.module';
import { CoreComponentsModule } from './components/components.module';

import { HandleErrorsService } from './services/handle-errors.service';
import { UserStrategyService } from './services/user-strategy.service';
import { SignalsService } from '@libs/signals-service/';
import { SearchService } from './services/symbol-search.service';

import { LocalStorageModule, LocalStorageService } from 'angular-2-local-storage';
import { C2MOBILE } from '@app/globals';
import { LoggerModule } from '@app/logger/logger.module';
//import { QuotesFeedsModule } from '@app/quotes-feeds/quotes-feeds.module';
import { SymbolSearchModule } from '@app/symbol-search/symbol-search.module';
import { ForexSymbolsService } from '@app/core/services/forex-symbols.service';


const COMPONENTS = [
  AppheaderContainer,
  ErrorMessagesComponent,
  NotFoundPageComponent,
];

@NgModule({
  imports: [
    CommonModule,
    CoreComponentsModule,
    LoggerModule,
    // RouterModule,
    PrimeNgModule,
    //QuotesFeedsModule,
    SymbolSearchModule,
    LocalStorageModule.withConfig({
      prefix: C2MOBILE,
      storageType: 'localStorage'
    })
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
})
export class CoreModule {
  static forRoot() {
    return {
      ngModule: CoreModule,
      providers: [
        ForexSymbolsService,
        HandleErrorsService,
        LocalStorageService,
        UserStrategyService,
        SearchService,
        SignalsService
      ],
    };
  }
}
