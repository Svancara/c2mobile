import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { LocalStorageService } from 'angular-2-local-storage';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/switchMap';
import * as logger from '@app/store/actions/logger.actions';
import { getLogItems } from '@app/store';
import { Store } from '@ngrx/store';
import { SIGNALS_LOG } from '@app/globals';
import * as storeState from '@app/store';
import { ILogItem } from '@app/store/actions/logger.actions';

@Injectable()
export class LoggerEffects {

  constructor(
    private actions$: Actions,
    private store: Store<storeState.State>,
    private localStorage: LocalStorageService) { }

  private saveLog(newItem: any): void {
    let current = this.localStorage.get<ILogItem[]>(SIGNALS_LOG);
    if (!current) {
      this.localStorage.set(SIGNALS_LOG, [newItem]);
    } else {
      this.localStorage.set(SIGNALS_LOG, [...current, newItem]);
    }
  }


  /**
   * This effect does not yield any actions back to the store. Set
   * `dispatch` to false to hint to @ngrx/effects that it should
   * ignore any elements of this effect stream.
   *
   * The `defer` observable accepts an observable factory function
   * that is called when the observable is subscribed to.
   * Wrapping the database open call in `defer` makes
   * effect easier to test.
   */

  @Effect({ dispatch: false })
  saveSignal$ = this.actions$
    .ofType(logger.SAVE_SIGNAL)
    //.map((action: logger.SaveSignal) => {
    //  // console.log("LoggerEffects saveSingal$ AAAAAAAAAAAAAAA", action.payload);
    //  return action.payload;
    //})
    .map((action: logger.SaveSignal) => {
      this.store.select(storeState.getLogItems).first().subscribe(log => {
        // console.log("LoggerEffects saveSingal$ BBBBBBBBBBBB", log);
        this.saveLog(log)
      })
    })
    // .do((x) => console.log("LoggerEffects saveSingal$ CCCCCCCCCCCCCCCC", x))
    // .map((action: logger.SaveSignal) => this.saveLog(action.payload))
  //  .map(() => new logger.LoggerDone('saveSignal$'))
  ;

  @Effect({ dispatch: false })
  saveSingalResponse$ = this.actions$
    .ofType(logger.SAVE_SIGNAL_RESPONSE)
    //.map((action: logger.SaveSignalResponse) => {
    //  console.log("LoggerEffects SaveSignalResponse", action.payload);
    //  return action.payload;
    //})
    .map(action => {
      this.store.select(storeState.getLogItems).first().subscribe(log => {
        //console.log("LoggerEffects saveSingalResponse$", log);
        //this.localStorage.set(C2MOBILE_LOG, log);
        this.saveLog(log)
      })
    })
    //.map((action: logger.SaveSignalResponse) => this.saveLog(action.payload))
  //  .map(() => new logger.LoggerDone('saveSignal$'))
  ;

  @Effect({ dispatch: false })
  logSignalCancel$ = this.actions$
    .ofType(logger.SAVE_SIGNAL_CANCELLATION)
    .do(action => this.store.select(storeState.getLogItems).first().subscribe(log => this.saveLog(log)))
  //  .map(() => new logger.LoggerDone('logSignalCancel$'))
  ;

  @Effect({ dispatch: false })
  logSignalCancelResponse$ = this.actions$
    .ofType(logger.SAVE_SIGNAL_CANCELLATION_RESPONSE)
    .do(action => this.store.select(storeState.getLogItems).first().subscribe(log => this.saveLog(log)))
  //  .map(() => new logger.LoggerDone('logSignalCancelResponse$'))
  ;

  @Effect({ dispatch: false })
  saveErrorItem$ = this.actions$
    .ofType(logger.SAVE_ERROR_ITEM)
    .do(action => this.store.select(storeState.getLogItems).first().subscribe(log => this.saveLog(log)))
  //  .map(() => new logger.LoggerDone('saveErrorItem$ '))
  ;

}
