import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { empty } from 'rxjs/observable/empty';
//import 'rxjs/add/operator/debounceTime';
//import 'rxjs/add/operator/skip';
//import 'rxjs/add/operator/takeUntil';

import { Injectable, InjectionToken, Optional, Inject } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { async } from 'rxjs/scheduler/async';
import { of } from 'rxjs/observable/of';
import { Store } from '@ngrx/store';

import { NotificationsService } from 'angular2-notifications';
import { LoggerService } from '@app/core/services/logger.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Utils } from '@app/utils';

import { SearchService } from '@app/core/services/symbol-search.service';
import * as searchActions from '@app/store/actions/search-option-by-strike.actions';
import { OptionSearchByStrike, OptionSearchByStrikeResponse} from '@app/models/LastQuoteService.dtos';

import * as searchReducer from '@app/store/reducers/search-option-by-strike.reducer';

@Injectable()
export class OptionSearchByStrikeEffects {

  constructor(
    private actions$: Actions,
    private store: Store<searchReducer.State>,
    private service: SearchService,
    private notificationsService: NotificationsService,
    private logger: LoggerService) { }


  @Effect()
  optionsByStrikes$: Observable<Action> = this.actions$
    .ofType(searchActions.LOAD_BY_STRIKE)
    .do(action => this.store.dispatch(new searchActions.Clear()))
    .map((action: searchActions.LoadByStrike) => action.payload)
    .switchMap((query: OptionSearchByStrike) => {
      if (query.underlaying === '') {
        return empty();
      }

      return this.service
        .optionsByStrikes(query)
        .map((response: OptionSearchByStrikeResponse) => {
          // console.log(response.Data);
          return new searchActions.LoadByStrikeSuccess(response.Data);
        })
        .catch((error: HttpErrorResponse) => {
          this.logger.SaveErrorItem(error);
          this.handleHttpError(error);
          return of(new searchActions.LoadByStrikeFail(Utils.formatError(error)));
        });
    });

   

  private handleHttpError(status: HttpErrorResponse): Observable<any> {
    this.notificationsService.error(status.name, status.message, { timeOut: 5000, showProgressBar: true, pauseOnHover: true });
    return Observable.throw(status);
  }

}
