import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/switchMap';
import * as fundamentalDataActions from '@app/store/actions/fundamental-data.actions';
import { getFundamentalData } from '@app/store';
import { Store } from '@ngrx/store';
import * as storeState from '@app/store';
import { FundamentalDataService } from '@app/core/services/fundamental-data-feed.service';
import { FundamentalDataResponse, ResponseStatus } from '@app/models';
import { NotificationsService } from 'angular2-notifications';
import { LoggerService } from '@app/core/services/logger.service';
import { HttpErrorResponse } from '@angular/common/http';
//import { SELECTED_OPTION_SYMBOL } from '@app/store/actions/gp.action';

@Injectable()
export class FundamentalDataEffects {

  constructor(
    private actions$: Actions,
    private store: Store<storeState.State>,
    private quotesFeedService: FundamentalDataService,
    private notificationsService: NotificationsService,
    private logger: LoggerService) { }

  @Effect() getLastQuote$: Observable<Action> = this.actions$
//    .ofType(fundamentalDataActions.LOAD_DATA, SELECTED_OPTION_SYMBOL)
    .ofType(fundamentalDataActions.LOAD_DATA)
    // .do(action => console.log("LastQuotesEffects: getLastQuote$", action))
    .switchMap((action: fundamentalDataActions.LoadData) => {
      return this.quotesFeedService
        .getFundamentalData(action.payload)
        // ToDo: Make following more "Rx"
        .map((response: FundamentalDataResponse) => {
          // console.log("LastQuotesEffects: getLastQuote$", response);
          if (response.ResponseStatus) {
            this.handleDownloadC2Error(response);
            return new fundamentalDataActions.LoadDataFailure(response.ResponseStatus);
          }

          if (response.ok == "0") {
            this.handleIQFeedError(response.Symbol, response.error.message);
            return new fundamentalDataActions.LoadDataFailure(response.error);
          }

          if (response.quotefeedstatus == 0) {
            this.handleIQFeedError_2(response);
            return new fundamentalDataActions.LoadDataFailure(response.error);
          }

          return new fundamentalDataActions.LoadDataSuccess(response);
        })
        .catch((error: HttpErrorResponse) => {
          this.logger.SaveErrorItem(error);
          this.handleHttpError(error);
          return of(new fundamentalDataActions.LoadDataFailure(error));
        })
    });

  private handleIQFeedError(symbol: string, error: string): void {
    this.notificationsService.error(symbol + " Error", error, { timeOut: 5000, showProgressBar: true, pauseOnHover: true });
    this.logger.SaveErrorItem(symbol + " " + error);
  }

  private handleIQFeedError_2(data: FundamentalDataResponse): void {
    this.notificationsService.error(data.Symbol + " Error", data.quotefeedstatus, { timeOut: 5000, showProgressBar: true, pauseOnHover: true });
    this.logger.SaveErrorItem(data.Symbol + " " + data.quotefeedstatus);
  }

  private handleDownloadC2Error(response: FundamentalDataResponse): void {
    let title: string = response.Symbol + " Error";
    let message: string = response.error ? response.error.message : "Unknown error";
    this.notificationsService.error(title, message, { timeOut: 5000, showProgressBar: true, pauseOnHover: true });
    this.logger.SaveErrorItem(title + " " + message);
  }

  private handleHttpError(status: HttpErrorResponse): Observable<any> {
    this.notificationsService.error(status.name, status.message, { timeOut: 5000, showProgressBar: true, pauseOnHover: true });
    return Observable.throw(status);
  }

}
