import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/toArray';
import 'rxjs/add/operator/delay';
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';
// import { Database } from '@ngrx/db';
import { Observable } from 'rxjs/Observable';
import { defer } from 'rxjs/observable/defer';
import { of } from 'rxjs/observable/of';

import * as fromEntity from '@app/store/actions/positions/position';
import * as fromCollection from '@app/store/actions/positions/positions.collection';
import * as fromPositions from '../../reducers';
import { Position, RequestTradesResponse, SendSignal, C2Action, ISendSignalResponse } from '@app/models';
import { HttpErrorResponse } from '@angular/common/http';
import { C2Error } from '@app/models/types';
import { Utils } from '@app/utils';
import { Store } from '@ngrx/store';
import { SignalsService } from '@libs/signals-service/';
import { NotificationsService } from 'angular2-notifications';
import { LoggerService } from '@app/core/services/logger.service';


@Injectable()
export class PositionsCollectionEffects {

  constructor(private actions$: Actions,
    private signalsService: SignalsService,
    private store: Store<fromPositions.State>,
    private notificationsService: NotificationsService,
    private logger: LoggerService) { }

  /**
   * This effect does not yield any actions back to the store. Set
   * `dispatch` to false to hint to @ngrx/effects that it should
   * ignore any elements of this effect stream.
   *
   * The `defer` observable accepts an observable factory function
   * that is called when the observable is subscribed to.
   * Wrapping the database open call in `defer` makes
   * effect easier to test.
   */
  /*
  @Effect({ dispatch: false })
  openDB$: Observable<any> = defer(() => {
    return this.db.open('books_app');
  });
    @Effect()
  loadCollection$: Observable<Action> = this.actions$
    .ofType(collection.LOAD)
    .switchMap(() =>
      this.db
        .query('books')
        .toArray()
        .map((books: Position[]) => new collection.LoadSuccess(books))
        .catch(error => of(new collection.LoadFail(error)))
    );
  */

  @Effect() downloadPositions$: Observable<Action> = this.actions$
    .ofType(fromCollection.LOAD)
    // .do(action => console.log("Effect: downloadPositions", action))
    .do(action => this.store.dispatch(new fromEntity.Clear()))
    .switchMap((action: fromCollection.Load) => {
      return this.signalsService.getPositions(action.strategyId)
        // ToDo: Make following more "Rx"
        .map((response: RequestTradesResponse) => {
          // console.log("Effect: downloadPositions", response);
          if (response.ok === '1') {
            const openOnly = response.response.filter((trade: Position) => trade.open_or_closed === "open");
            return new fromCollection.LoadSuccess(this.formatForEntities(response.strategyId, openOnly));
          } else {
            this.handleDownloadC2Error(response);
            return new fromCollection.DownloadC2Error(response.status);
          }
        })
        .catch((error: HttpErrorResponse) => {
          this.logger.SaveErrorItem(error);
          this.handleHttpError(error);
          return of(new fromCollection.LoadFail(Utils.formatError(error)));
        })
    });


  private _closePosition: Position;

  @Effect() closePositions$: Observable<Action> = this.actions$
    .ofType(fromCollection.REMOVE)
    //.do(action => console.log("Effect: closePositions$", action))
    .do((action: fromCollection.Remove) => this._closePosition = action.payload)
    .do((action: fromCollection.Remove) => this.logger.logPositionClose(action.payload))
    .switchMap((action: fromCollection.Remove) => {
      return this.signalsService
        .sendSignal(this.createClosingSignal(action.payload))
        .do((response: ISendSignalResponse) => this.logger.logSignalResponse(response.signalid, response))
        // ToDo: Make following more "Rx"
        .map((response: ISendSignalResponse) => {
          // console.log("Effect: downloadPositions", response);
          if (response.ok === '1') {
            // ToDo: fromCollection.RemoveSuccess is not OK. Closed position does not mean removing it from list, but SEND A CLOSING SIGNAL!
            // So Position stays in the liost and a new signal is added to the signals list.
            // Only in the case when markets are Open, everything is faster. The closing signal (@ market) is done and position removed.
            // But it can take 2-3 seconds. How to solve that?
            this.handleC2SignalResponse(response);
            return new fromCollection.RemoveSuccess(this._closePosition);
          } else {
            this.handleDownloadC2Error(response);
            return new fromCollection.RemoveFail(this._closePosition);
          }
        })
        // ToDo: Do it better
        // Re-read positions after 2-3 seconds. Perhaps
        .delay(2000)
        .do(action => this.store.dispatch(new fromCollection.Load(this._closePosition.strategyId)))
        .catch((error: HttpErrorResponse) => {
          this.logger.SaveErrorItem(error);
          this.handleHttpError(error);
          return of(new fromCollection.RemoveFail(this._closePosition));
        })
    });

  private createClosingSignal(position: Position): SendSignal {

    let signal: SendSignal = new SendSignal();
    signal.action = position.long_or_short === "long" ? C2Action.STC : C2Action.BTC;
    signal.market = "1";
    signal.quant = position.quant_opened - position.quant_closed;
    signal.symbol = position.symbol;
    signal.strategyId = position.strategyId;
    signal.typeofsymbol = position.instrument;
    return signal;
  }


  /**
   * I think this should be done in reducer when handling LoadSuccess
   * @param c2Positions
   */
  formatForEntities(strategyId: number, positions: Position[]): Position[] {
    return positions.map((p: Position) => {
      p.id = p.trade_id.toString();
      p.strategyId = strategyId;

      // Convert wrong C2 types. Numbers arrive as strings.
      p.closeVWAP_timestamp = +p.closeVWAP_timestamp;
      p.strike = +p.strike;
      p.openVWAP_timestamp = +p.openVWAP_timestamp;
      p.closing_price_VWAP = +p.closing_price_VWAP;
      p.quant_closed = +p.quant_closed;
      p.trade_id = +p.trade_id;
      p.opening_price_VWAP = +p.opening_price_VWAP;
      p.quant_opened = +p.quant_opened;
      p.closedWhen = +p.closedWhen;
      p.ptValue = +p.ptValue ;
      p.PL = +p.PL;
      p.closedWhenUnixTimeStamp = +p.closedWhenUnixTimeStamp;
      return p;
    });
  }

  private handleC2SignalResponse(response: ISendSignalResponse): void {
    let title: string = response.error ? response.error.title : (response.status ? response.status.title : "Signal received");
    let message: string = response.error ? response.error.message : (response.status ? response.status.message : `Signal ${response.signalid} accepted for processing.`);
    this.notificationsService.success(title, message, { timeOut: 3000, showProgressBar: true, pauseOnHover: true });
  }

  private handleDownloadC2Error(response: ISendSignalResponse | RequestTradesResponse): void {
    let title: string = response.error ? response.error.title : (response.status ? response.status.title : "Collective2 API error");
    let message: string = response.error ? response.error.message : (response.status ? response.status.message : "Unknown error");
    this.notificationsService.error(title, message, { timeOut: 5000, showProgressBar: true, pauseOnHover: true });
  }

  private handleHttpError(status: HttpErrorResponse): Observable<any> {
    this.notificationsService.error(status.name, status.message, { timeOut: 5000, showProgressBar: true, pauseOnHover: true });
    return Observable.throw(status);
  }

  /*
    @Effect()
    addPositionToCollection$: Observable<Action> = this.actions$
      .ofType(collection.ADD_POSITION)
      .map((action: collection.AddPosition) => action.payload)
      .mergeMap(book =>
        this.db
          .insert('books', [book])
          .map(() => new collection.AddBookSuccess(book))
          .catch(() => of(new collection.AddBookFail(book)))
      );
  
    @Effect()
    removeBookFromCollection$: Observable<Action> = this.actions$
      .ofType(collection.REMOVE_BOOK)
      .map((action: collection.RemoveBook) => action.payload)
      .mergeMap(book =>
        this.db
          .executeWrite('books', 'delete', [book.id])
          .map(() => new collection.RemoveBookSuccess(book))
          .catch(() => of(new collection.RemoveBookFail(book)))
      );
  */
}
