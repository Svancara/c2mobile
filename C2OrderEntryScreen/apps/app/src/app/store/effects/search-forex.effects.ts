import 'rxjs/add/operator/map';
import { of } from 'rxjs/observable/of';
import { Injectable, InjectionToken, Optional, Inject } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
//import { async } from 'rxjs/scheduler/async';
import { Store } from '@ngrx/store';

import { NotificationsService } from 'angular2-notifications';
import { LoggerService } from '@app/core/services/logger.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Utils } from '@app/utils';

import * as actions from '@app/store/actions/search-forex.actions';
import * as reducer from '@app/store/reducers/search-forex.reducer';
import { ForexSymbol, ForexPairsResponse } from '@app/models/LastQuoteService.dtos';
import { ForexSymbolsService } from '@app/core/services/forex-symbols.service';

@Injectable()
export class ForexSymbolsEffects {

  constructor(
    private actions$: Actions,
    private store: Store<reducer.State>,
    private service: ForexSymbolsService,
    private notificationsService: NotificationsService,
    private logger: LoggerService) { }


  @Effect()
  forexSymbols$: Observable<Action> = this.actions$
    .ofType(actions.LOAD)
    .do(action => this.store.dispatch(new actions.Clear()))
    // .map((action: actions.Load) => action.payload)
    .switchMap(() => {
      return this.service
        .getForexPairs()
        .map((response: ForexPairsResponse) => {
          return new actions.LoadSuccess(response.Data);
        })
        .catch((error: HttpErrorResponse) => {
          this.logger.SaveErrorItem(error);
          this.handleHttpError(error);
          return of(new actions.LoadFail(Utils.formatError(error)));
        });
    });

   

  private handleHttpError(status: HttpErrorResponse): Observable<any> {
    this.notificationsService.error(status.name, status.message, { timeOut: 5000, showProgressBar: true, pauseOnHover: true });
    return Observable.throw(status);
  }

}
