import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/toArray';
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';
// import { Database } from '@ngrx/db';
import { Observable } from 'rxjs/Observable';
import { defer } from 'rxjs/observable/defer';
import { of } from 'rxjs/observable/of';

// import * as fromCollection from '../actions/user-strategies.collection.actions';
import { IStrategy } from '@app/models';
import { UserStrategyService } from '@app/core';
import { HttpErrorResponse } from '@angular/common/http';
import { C2Error } from '../../models/types';
import { Utils } from '../../utils';

@Injectable()
export class UserStrategyEffects {

  constructor(private actions$: Actions, private service: UserStrategyService) { }

  /**
   * This effect does not yield any actions back to the store. Set
   * `dispatch` to false to hint to @ngrx/effects that it should
   * ignore any elements of this effect stream.
   *
   * The `defer` observable accepts an observable factory function
   * that is called when the observable is subscribed to.
   * Wrapping the database open call in `defer` makes
   * effect easier to test.
   */
  /*
  @Effect({ dispatch: false })
  openDB$: Observable<any> = defer(() => {
    return this.db.open('books_app');
  });
    @Effect()
  loadCollection$: Observable<Action> = this.actions$
    .ofType(collection.LOAD)
    .switchMap(() =>
      this.db
        .query('books')
        .toArray()
        .map((books: Position[]) => new collection.LoadSuccess(books))
        .catch(error => of(new collection.LoadFail(error)))
    );
  */
  /*
  @Effect() downloadUserStrategy: Observable<Action> = this.actions$
    .ofType(fromCollection.LOAD)
    // .do(action => console.log("Effect: downloadUserStrategies", action))
    .switchMap((action: fromCollection.Load) => this.service.getStrategies())
    .map((response: UserStrategies) => {
      // console.log("Effect: downloadUserStrategies", response);
      if (response.ok === '1') {
        return new fromCollection.LoadSuccess(this.formatForEntities(response.response));
      } else {
        return new fromCollection.DownloadC2Error(response.status);
      }
    })
    .catch((status: HttpErrorResponse) => {
      //this.store$.dispatch(new fromCollection.DownloadFail(status));
      //return Observable.of(this.formatError(status));
      console.log(status);
      return of(new fromCollection.LoadFail(Utils.formatError(status)));
    });
  */

  
  /*
    @Effect()
    addPositionToCollection$: Observable<Action> = this.actions$
      .ofType(collection.ADD_POSITION)
      .map((action: collection.AddPosition) => action.payload)
      .mergeMap(book =>
        this.db
          .insert('books', [book])
          .map(() => new collection.AddBookSuccess(book))
          .catch(() => of(new collection.AddBookFail(book)))
      );
  
    @Effect()
    removeBookFromCollection$: Observable<Action> = this.actions$
      .ofType(collection.REMOVE_BOOK)
      .map((action: collection.RemoveBook) => action.payload)
      .mergeMap(book =>
        this.db
          .executeWrite('books', 'delete', [book.id])
          .map(() => new collection.RemoveBookSuccess(book))
          .catch(() => of(new collection.RemoveBookFail(book)))
      );
  */
}
