import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/toArray';
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';
// import { Database } from '@ngrx/db';
import { Observable } from 'rxjs/Observable';
import { defer } from 'rxjs/observable/defer';
import { of } from 'rxjs/observable/of';
import { Store } from '@ngrx/store';

import * as fromStore from '@app/store';
import * as fromUserStore from '@app/store/reducers/auth';
import * as fromCollection from '@app/store/actions/user-strategies.collection.actions';
import * as fromEntity from '@app/store/actions/user-strategy.actions';

import { IStrategy, UserStrategies } from '@app/models';
import { HttpErrorResponse } from '@angular/common/http';
import { C2Error } from '../../models/types';
import { Utils } from '../../utils';
// import { UserStrategiesApiClient } from '@app/user-strategies-list/user-strategies-ApiClient.service';
import { SignalsService } from '@libs/signals-service/';

import { NotificationsService } from 'angular2-notifications';
import { LoggerService } from '@app/core/services/logger.service';

@Injectable()
export class UserStragiesEffects {

  constructor(private actions$: Actions,
    private appState$: Store<fromStore.State>,
    //private service: UserStrategiesApiClient
    private service: SignalsService,
    private notificationsService: NotificationsService,
    private logger: LoggerService
  ) { }

  /**
   * This effect does not yield any actions back to the store. Set
   * `dispatch` to false to hint to @ngrx/effects that it should
   * ignore any elements of this effect stream.
   *
   * The `defer` observable accepts an observable factory function
   * that is called when the observable is subscribed to.
   * Wrapping the database open call in `defer` makes
   * effect easier to test.
   */
  /*
  @Effect({ dispatch: false })
  openDB$: Observable<any> = defer(() => {
    return this.db.open('books_app');
  });
    @Effect()
  loadCollection$: Observable<Action> = this.actions$
    .ofType(collection.LOAD)
    .switchMap(() =>
      this.db
        .query('books')
        .toArray()
        .map((books: Position[]) => new collection.LoadSuccess(books))
        .catch(error => of(new collection.LoadFail(error)))
    );
  */

  @Effect() downloadUserStrategies: Observable<Action> = this.actions$
    .ofType(fromCollection.LOAD)
    // .do(action => console.log("Effect: downloadUserStrategies", action))
    .do(action => this.appState$.dispatch(new fromEntity.Clear()))
    .switchMap((action: fromCollection.Load) => {
      return this.service
        .getStrategies()
        .map((response: UserStrategies) => {
          // console.log("Effect: downloadUserStrategies", response);
          if (response.ok === '1') {
            if (response.ok && response.response.length == 0) {
              this.handle_NoStrategiesFound(response);
            }
            return new fromCollection.LoadSuccess(this.formatForEntities(response.response));
          } else {
            this.handleDownloadC2Error(response);
            return new fromCollection.DownloadC2Error(response.status);
          }
        })
        .catch((error: HttpErrorResponse) => {
          this.handleHttpError(error);
          return of(new fromCollection.LoadFail(Utils.formatError(error)));
        })
    });

  /**
  * For developing urposes
  */
  @Effect() addStrategy: Observable<Action> = this.actions$
    .ofType(fromCollection.ADD)
    // .do(action => console.log("Effect: downloadUserStrategies", action))
    // .do(action => this.appState$.dispatch(new fromEntity.Clear()))
    .do((action: fromCollection.Add) => {
      console.log("Effect: addStrategy", action.payload);
//      return new fromCollection.LoadSuccess(this.formatForEntities([action.payload]));
    }).catch(error => {
      console.log('*** ERROR in UserStragiesEffects.addStrategy', error);
      return of(new fromCollection.AddFail(error));
    });

/**
 * I think this should be done in reducer when handling LoadSuccess
 */
formatForEntities(positions: IStrategy[]): IStrategy[] {
  return positions.map((p: IStrategy) => { p.id = p.system_id.toString(); return p; });
}

  private handleDownloadC2Error(response: UserStrategies): void {
  let title: string = response.error ? response.error.title : (response.status ? response.status.title : "Collective2 API error");
  let message: string = response.error ? response.error.message : (response.status ? response.status.message : "Unknown error");
  this.notificationsService.error(title, message, { timeOut: 5000, showProgressBar: true, pauseOnHover: true });
}

  private handle_NoStrategiesFound(response: UserStrategies): void {
  this.notificationsService.warn('No strategies found', '(Wrong Api key?)', { timeOut: 5000, showProgressBar: true, pauseOnHover: true });
}

  private handleHttpError(status: HttpErrorResponse): Observable < any > {
  this.notificationsService.error(status.name, status.message, { timeOut: 5000, showProgressBar: true, pauseOnHover: true });
  return Observable.throw(status);
}

}
