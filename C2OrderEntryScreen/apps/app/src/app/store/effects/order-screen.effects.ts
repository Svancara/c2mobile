import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/toArray';
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';
// import { Database } from '@ngrx/db';
import { Observable } from 'rxjs/Observable';
import { defer } from 'rxjs/observable/defer';
import { of } from 'rxjs/observable/of';

import * as entryScreenActions from '../actions/order-screen-actions';
import * as reducers from '../../reducers';
import { OrderScreen, OrderScreenSymbol, SendSignalPayload, ISendSignalResponse } from '@app/models';
import { HttpErrorResponse } from '@angular/common/http';
import { C2Error } from '@app/models/types';
import { Utils } from '@app/utils';
import { Store } from '@ngrx/store';
import { SignalsService } from '@libs/signals-service/';

import { NotificationsService } from 'angular2-notifications';
import { LoggerService } from '@app/core/services/logger.service';

@Injectable()
export class OrderScreenEffects {

  constructor(private actions$: Actions,
    private service: SignalsService,
    private store: Store<reducers.State>,
    private notificationsService: NotificationsService,
    private logger: LoggerService) { }

  /**
   * This effect does not yield any actions back to the store. Set
   * `dispatch` to false to hint to @ngrx/effects that it should
   * ignore any elements of this effect stream.
   *
   * The `defer` observable accepts an observable factory function
   * that is called when the observable is subscribed to.
   * Wrapping the database open call in `defer` makes
   * effect easier to test.
   */
  /*
  @Effect({ dispatch: false })
  openDB$: Observable<any> = defer(() => {
    return this.db.open('books_app');
  });
    @Effect()
  loadCollection$: Observable<Action> = this.actions$
    .ofType(collection.LOAD)
    .switchMap(() =>
      this.db
        .query('books')
        .toArray()
        .map((books: Position[]) => new collection.LoadSuccess(books))
        .catch(error => of(new collection.LoadFail(error)))
    );
  */

  @Effect({ dispatch: false }) sendSignal$: Observable<Action> = this.actions$
    .ofType(entryScreenActions.SEND)
    // .do(action => console.log("Effect: sendSignal", action))
    .do((action: entryScreenActions.Send) => this.logger.logSignal(action.payload))
    .switchMap((action: entryScreenActions.Send) => {
      return this.service.sendSignal(action.payload)
        .do((response: ISendSignalResponse) => this.logger.logSignalResponse(response.signalid, response))
        // ToDo: Make following more "Rx"
        .map((response: ISendSignalResponse) => {
          //console.log("Effect: sendSignal", response);
          if (response.ok === '1') {
            this.handleC2SignalResponse(response);
            return new entryScreenActions.SendSuccess(response);
          } else {
            this.handleC2Error(response);
            return new entryScreenActions.SendC2Error(response);
          }
        })
        .catch((error) => {
          this.logger.SaveErrorItem(error)
          this.handleHttpError(error);
          return of(new entryScreenActions.SendFail(error));
        })
    });

  private handleC2SignalResponse(response: ISendSignalResponse): void {
    let title: string = response.error ? response.error.title : (response.status ? response.status.title : "Signal received");
    let message: string = response.error ? response.error.message : (response.status ? response.status.message : `Signal ${response.signalid} accepted for processing.`);
    this.notificationsService.success(title, message, { timeOut: 3000, showProgressBar: true, pauseOnHover: true });
  }

  private handleC2Error(response: ISendSignalResponse): void {
    let title: string = response.error ? response.error.title : (response.status ? response.status.title : "Collective2 API error");
    let message: string = response.error ? response.error.message : (response.status ? response.status.message : "Unknown error");
    this.notificationsService.error(title, message, { timeOut: 5000, showProgressBar: true, pauseOnHover: true });
  }

  private handleHttpError(status: HttpErrorResponse): Observable<any> {
    this.notificationsService.error(status.name, status.message, { timeOut: 5000, showProgressBar: true, pauseOnHover: true });
    return Observable.throw(status);
  }

}
