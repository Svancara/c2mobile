import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/takeUntil';
// import 'rxjs/add/operator/debounce';

import { Injectable, InjectionToken, Optional, Inject } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Scheduler } from 'rxjs/Scheduler';
import { async } from 'rxjs/scheduler/async';
import { empty } from 'rxjs/observable/empty';
import { of } from 'rxjs/observable/of';
import { Store } from '@ngrx/store';

import { SearchService } from '@app/core/services/symbol-search.service';
import * as symbolSearch from '@app/store/actions/symbol-search/symbol-search';
import { SymbolSearch, SymbolSearchResponse, SymbolSearchTypeAheadSupport } from '@app/models/LastQuoteService.dtos';

//import * as fromEntity from '@app/store/actions/symbol-search/book';
import * as fromCollection from '@app/store/actions/symbol-search/collection';
import * as fromSearchSymbol from '@app/store/reducers/symbol-search';
import { NotificationsService } from 'angular2-notifications';
import { LoggerService } from '@app/core/services/logger.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Utils } from '@app/utils';

@Injectable()
export class SymbolSearchEffects {

  constructor(
    private actions$: Actions,
    private store: Store<fromSearchSymbol.State>,
    private googleBooks: SearchService,
    private notificationsService: NotificationsService,
    private logger: LoggerService) { }



  /**
  * Autocomplete prepared but not used for now
  */
  @Effect()
  autoComplete$: Observable<Action> = this.actions$
    .ofType<symbolSearch.Search>(symbolSearch.SEARCH)
    .do(action => this.store.dispatch(new symbolSearch.Clear()))
    .debounceTime(300, async)
    .map((action: symbolSearch.Search) => action.payload)
    .switchMap((query: SymbolSearch) => {
      if (query.query === '') {
        return empty();
      }

      const nextSearch$ = this.actions$.ofType(symbolSearch.SEARCH).skip(1);

      return this.googleBooks
        .symbolSearch(query)
        .takeUntil(nextSearch$)
        .map((response: SymbolSearchResponse) => new symbolSearch.SearchComplete(response.Data))
        .catch(err => of(new symbolSearch.SearchError(err)));
    });


  /**
  *  Search in names
  */
  @Effect()
  downloadSearchResults$: Observable<Action> = this.actions$
    .ofType(fromCollection.LOAD)
    .do(action => this.store.dispatch(new symbolSearch.Clear()))
    .map((action: fromCollection.Load) => action.payload)
    .switchMap((query: SymbolSearch) => {
      if (query.query === '') {
        return empty();
      }

      return this.googleBooks
        .symbolSearch(query)
        .map((response: SymbolSearchResponse) => new fromCollection.LoadSuccess(response.Data))
        .catch((error: HttpErrorResponse) => {
          this.logger.SaveErrorItem(error);
          this.handleHttpError(error);
          return of(new fromCollection.LoadFail(Utils.formatError(error)));
        });
    });


  private handleHttpError(status: HttpErrorResponse): Observable<any> {
    this.notificationsService.error(status.name, status.message, { timeOut: 5000, showProgressBar: true, pauseOnHover: true });
    return Observable.throw(status);
  }

}
