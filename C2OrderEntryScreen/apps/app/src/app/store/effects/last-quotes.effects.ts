import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/switchMap';
import * as lastQuoteActions from '@app/store/actions/last-quote.actions';
import { getLastQuote } from '@app/store';
import { Store } from '@ngrx/store';
import * as storeState from '@app/store';
import { LastQuotesFeedService } from '@app/core/services/last-quotes-feed.service';
import { GetLastQuoteResponse, ResponseStatus, LastQuoteData } from '@app/models';
import { NotificationsService } from 'angular2-notifications';
import { LoggerService } from '@app/core/services/logger.service';
import { HttpErrorResponse } from '@angular/common/http';
//import { SELECTED_OPTION_SYMBOL } from '@app/store/actions/gp.action';

@Injectable()
export class LastQuotesEffects {

  constructor(
    private actions$: Actions,
    private store: Store<storeState.State>,
    private quotesFeedService: LastQuotesFeedService,
    private notificationsService: NotificationsService,
    private logger: LoggerService) { }

  @Effect() getLastQuote$: Observable<Action> = this.actions$
//    .ofType(lastQuoteActions.LOAD_DATA, SELECTED_OPTION_SYMBOL)
    .ofType(lastQuoteActions.LOAD_DATA)
    // .do(action => console.log("LastQuotesEffects: getLastQuote$", action))
    .switchMap((action: lastQuoteActions.LoadData) => {
      return this.quotesFeedService
        .getLastQuote(action.payload)
        // ToDo: Make following more "Rx"
        .map((response: GetLastQuoteResponse) => {
          // console.log("LastQuotesEffects: getLastQuote$", response);
          if (response.ResponseStatus) {
            this.handleDownloadC2Error(response);
            return new lastQuoteActions.LoadDataFailure(response.ResponseStatus);
          }

          if (response.lastQuoteData.ok == "0") {
            this.handleIQFeedError(response);
            return new lastQuoteActions.LoadDataFailure(response.lastQuoteData.error);
          }

          if (response.lastQuoteData.quotefeedstatus == 0) {
            this.handleIQFeedError_2(response.lastQuoteData);
            return new lastQuoteActions.LoadDataFailure(response.lastQuoteData.error);
          }

          return new lastQuoteActions.LoadDataSuccess(response.lastQuoteData);

        })
        .catch((error: HttpErrorResponse) => {
          this.logger.SaveErrorItem(error);
          this.handleHttpError(error);
          return of(new lastQuoteActions.LoadDataFailure(error));
        })
    });

  private handleIQFeedError(response: GetLastQuoteResponse): void {
    this.notificationsService.error(response.lastQuoteData.symbol + " Error", response.lastQuoteData.error, { timeOut: 5000, showProgressBar: true, pauseOnHover: true });
    this.logger.SaveErrorItem(response.lastQuoteData.symbol + " " + response.lastQuoteData.error);
  }

  private handleIQFeedError_2(data: LastQuoteData): void {
    this.notificationsService.error(data.symbol + " Error", data.quotefeedstatus, { timeOut: 5000, showProgressBar: true, pauseOnHover: true });
    this.logger.SaveErrorItem(data.symbol + " " + data.quotefeedstatus);
  }

  private handleDownloadC2Error(response: GetLastQuoteResponse): void {
    let title: string = response.lastQuoteData.symbol + " Error " + (response.ResponseStatus ? response.ResponseStatus.ErrorCode : `Uknown Http error`);
    let message: string = response.ResponseStatus.Message ? response.ResponseStatus.Message : "";
    this.notificationsService.error(title, message, { timeOut: 5000, showProgressBar: true, pauseOnHover: true });
    this.logger.SaveErrorItem(response);
  }

  private handleHttpError(status: HttpErrorResponse): Observable<any> {
    this.notificationsService.error(status.name, status.message, { timeOut: 5000, showProgressBar: true, pauseOnHover: true });
    return Observable.throw(status);
  }

}
