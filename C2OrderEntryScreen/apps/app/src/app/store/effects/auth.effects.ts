import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/exhaustMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import { of } from 'rxjs/observable/of';

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Effect, Actions } from '@ngrx/effects';

import { AuthService } from '@app/core/services/auth.service';
import * as Auth from '@app/store/actions/auth.action';
import { ILoginResponse, CURRENT_USER } from '@app/models';
import { NotificationsService } from 'angular2-notifications';
import { HttpErrorResponse } from '@angular/common/http';
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable()
export class AuthEffects {
  @Effect()
  login$ = this.actions$
    .ofType(Auth.LOGIN)
    .map((action: Auth.Login) => action.payload)
    .exhaustMap(auth =>
      this.authService
        .login(auth)
        .map((response: ILoginResponse) => {
          if (response.ok === "1") {
            return new Auth.LoginSuccess(response.success, this.authService.redirectUrl);
          } else {
            this.notificationsService.error(response.error.title, response.error.message, { timeOut: 5000, showProgressBar: true, pauseOnHover: true });
            return new Auth.LoginFailure(response.error);
          }
        })
        .catch((error: HttpErrorResponse) => {
          this.notificationsService.error(error.name, error.message, { timeOut: 5000, showProgressBar: true, pauseOnHover: true });
          return of(new Auth.LoginFailure({ title: error.name, message: error.message, errors: error.statusText }));
        }
        )
    );

  @Effect({ dispatch: false })
  loginSuccess$ = this.actions$
    .ofType(Auth.LOGIN_SUCCESS)
    .do((action: Auth.LoginSuccess) => {
      this.localStorage.set(CURRENT_USER, action.payload);
      if (action.redirectUrl) {
        this.router.navigate([action.redirectUrl]);
        this.authService.redirectUrl = undefined;
      } else {
        this.router.navigate(['/']);
      }
    });

  @Effect({ dispatch: false })
  logout$ = this.actions$
    .ofType( Auth.LOGOUT)
    .do(authed => {
      this.localStorage.remove(CURRENT_USER);
      this.router.navigate(['/']);
    });

  @Effect({ dispatch: false })
  loginRedirect$ = this.actions$
    .ofType(Auth.LOGIN_REDIRECT)
    .do((action: Auth.LoginRedirect) => {
      this.authService.redirectUrl = action.redirectUrl;
      this.router.navigate(['/login',]);
    });

  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private router: Router,
    private notificationsService: NotificationsService,
    private localStorage: LocalStorageService
  ) { }
}
