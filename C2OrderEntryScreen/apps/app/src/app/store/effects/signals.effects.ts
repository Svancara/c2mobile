import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/toArray';
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';
// import { Database } from '@ngrx/db';
import { Observable } from 'rxjs/Observable';
import { defer } from 'rxjs/observable/defer';
import { of } from 'rxjs/observable/of';

import * as fromEntity from '@app/store/actions/signals/signal.actions';
import * as fromCollection from '@app/store/actions/signals/signals.collection.actions';
import * as fromSignals from '@app/store/reducers/signals';

import { Signal, SignalsWorking, CancelSignalResponse } from '@app/models';
import { SignalsService } from '@libs/signals-service/';
import { HttpErrorResponse } from '@angular/common/http';
import { C2Error } from '../../models/types';
import { Utils } from '../../utils';
import { Store } from '@ngrx/store';
import { Load as loadSignalsAction } from '@app/store/actions/signals/signals.collection.actions';
import { NotificationsService } from 'angular2-notifications';
import { LoggerService } from '@app/core/services/logger.service';

@Injectable()
export class SignalsCollectionEffects {

  constructor(private actions$: Actions,
    private service: SignalsService,
    private store: Store<fromSignals.State>,
    private notificationsService: NotificationsService,
    private logger: LoggerService) { }

  /**
   * This effect does not yield any actions back to the store. Set
   * `dispatch` to false to hint to @ngrx/effects that it should
   * ignore any elements of this effect stream.
   *
   * The `defer` observable accepts an observable factory function
   * that is called when the observable is subscribed to.
   * Wrapping the database open call in `defer` makes
   * effect easier to test.
   */
  /*
  @Effect({ dispatch: false })
  openDB$: Observable<any> = defer(() => {
    return this.db.open('books_app');
  });
    @Effect()
  loadCollection$: Observable<Action> = this.actions$
    .ofType(collection.LOAD)
    .switchMap(() =>
      this.db
        .query('books')
        .toArray()
        .map((books: Position[]) => new collection.LoadSuccess(books))
        .catch(error => of(new collection.LoadFail(error)))
    );
  */

  @Effect() downloadSignals: Observable<Action> = this.actions$
    .ofType(fromCollection.LOAD)
    // .do(action => console.log("Effect: downloadSignals", action))
    .do(action => this.store.dispatch(new fromEntity.Clear()))
    .switchMap((action: fromCollection.Load) => {
      return this.service
        .retrieveSignalsWorking(action.strategyId)
        .map((response: SignalsWorking) => {
          // console.log("Effect: downloadSignals", response);
          if (response.ok === '1') {
            return new fromCollection.LoadSuccess(this.formatForEntities(response.strategyId, response.response));
          } else {
            this.handleDownloadC2Error(response);
            return new fromCollection.DownloadC2Error(response.status);
          }
        })
        .catch((error: HttpErrorResponse) => {
          this.logger.SaveErrorItem(error);
          this.handleHttpError(error);
          return of(new fromCollection.LoadFail(Utils.formatError(error)));
        })
    });


  private _signal: Signal;

  @Effect() cancelSignal: Observable<any> = this.actions$
    .ofType(fromCollection.REMOVE)
    //.do(action => console.log("Effect: cancelSignal", action))
    .do((action: fromCollection.Remove) => this._signal = action.payload)
    .do((action: fromCollection.Remove) => this.logger.logSignalCancel(action.payload))
    //.do(action => this.store.dispatch(new fromEntity.Clear()))
    .switchMap((action: fromCollection.Remove) => {
      return this.service
        .cancelSignal(action.payload)
        .do((response: CancelSignalResponse) => this.logger.logSignalCancelResponse(response.signalid, response))
        .map((response: CancelSignalResponse) => {
          // console.log("Effect: downloadSignals", response);
          if (response.ok === '1') {
            this.handleC2SignalResponse(response);
            return new fromCollection.RemoveSuccess(this._signal);
          } else {
            this.handleDownloadC2Error(response);
            return new fromCollection.DownloadC2Error(response.status);
          }
        })
        //.filter(response => response instanceof fromCollection.RemoveSuccess )
        // reload signals
        .do(action => this.store.dispatch(new loadSignalsAction(this._signal.strategyId)))
        .catch((error: HttpErrorResponse) => {
          this.logger.SaveErrorItem(error);
          this.handleHttpError(error);
          return of(new fromCollection.RemoveFail(this._signal));
        })
      //    .do((response: CancelSignalResponse) => {
      //console.log("cancelSignal AA", response);
      //if (response.ok === '0') {
      //  return new fromCollection.DownloadC2Error(response.status);
      //}
      //    })
      //    .do((response: CancelSignalResponse) => {
      //console.log("cancelSignal AB", response);
      //if (response.ok === '1') {
      //  this.store.dispatch(new fromCollection.Load(response.response.systemid))
      //  return new fromCollection.RemoveSuccess(response.response.systemid, response.signalid);
      //}
      //      return null;
      //    })
      //.map((response: CancelSignalResponse) => {
      //  console.log("Effect: cancelSignal", response);
      //  if (response.ok === '1') {
      //  } else {
      //    return new fromCollection.DownloadC2Error(response.status);
      //  }
      //})

      //    .filter((response: CancelSignalResponse) => response.ok === '1', null, null)
      // {
      //  this.store.dispatch(new fromCollection.Load(response.response.systemid));))
      //  return new fromCollection.RemoveSuccess(response.response.systemid);

      //})

      // .switchMap((action: fromCollection.Remove) => this.store.dispatch(new fromCollection.Load(response.response.systemid));))
    })
  ;


  /**
   * I think this should be done in reducer when handling LoadSuccess
   */
  formatForEntities(strategyId: number, signals: Signal[]): Signal[] {
    return signals.map((p: Signal) => {
      p.strategyId = strategyId;
      p.id = p.signal_id.toString();

      // Convert wrong C2 types. Numbers arrive as strings.
      p.isLimitOrder = +p.isLimitOrder;
      p.strike = +p.strike;
      p.isMarketOrder = +p.isMarketOrder;
      p.quant = +p.quant;
      p.isStopOrder = +p.isStopOrder;
      p.posted_time_unix = +p.posted_time_unix;
      p.signal_id = +p.signal_id;
      return p;
    });
  }

  private handleHttpError(status: HttpErrorResponse): Observable<any> {
    this.notificationsService.error(status.name, status.message, { timeOut: 5000, showProgressBar: true, pauseOnHover: true });
    return Observable.throw(status);
  }

  private handleC2SignalResponse(response: SignalsWorking | CancelSignalResponse): void {
    let title: string = response.error ? response.error.title : (response.status ? response.status.title : "Signal received");
    let message: string = response.error ? response.error.message : (response.status ? response.status.message : `Signal accepted for processing.`);
    this.notificationsService.success(title, message, { timeOut: 3000, showProgressBar: true, pauseOnHover: true });
  }

  private handleDownloadC2Error(response: SignalsWorking | CancelSignalResponse): void {
    let title: string = response.error ? response.error.title : (response.status ? response.status.title : "Collective2 API error");
    let message: string = response.error ? response.error.message : (response.status ? response.status.message : "Unknown error");
    this.notificationsService.error(title, message, { timeOut: 5000, showProgressBar: true, pauseOnHover: true });
  }
}
