import { Action } from '@ngrx/store';

import { SymbolSearch, SymbolSearchResponse, SymbolSearchTypeAheadSupport } from '@app/models/LastQuoteService.dtos';

export const SEARCH = '[SymbolSearch] Search';
export const SEARCH_COMPLETE = '[SymbolSearch] Search Complete';
export const SEARCH_ERROR = '[SymbolSearch] Search Error';
export const LOAD = '[SymbolSearch] Load';
export const SELECT = '[SymbolSearch] Select';
export const CLEAR = '[SymbolSearch] Clear';

/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful
 * type checking in reducer functions.
 *
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */
export class Search implements Action {
  readonly type = SEARCH;

  constructor(public payload: SymbolSearch) { }
}

export class SearchComplete implements Action {
  readonly type = SEARCH_COMPLETE;

  constructor(public payload: SymbolSearchTypeAheadSupport[]) { }
}

export class SearchError implements Action {
  readonly type = SEARCH_ERROR;

  constructor(public payload: string) { }
}

export class Load implements Action {
  readonly type = LOAD;

  constructor(public payload: SymbolSearchTypeAheadSupport) { }
}

export class Select implements Action {
  readonly type = SELECT;

  constructor(public payload: SymbolSearchTypeAheadSupport) { }
}

export class Clear implements Action {
  readonly type = CLEAR;
}

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type Actions = Search | SearchComplete | SearchError | Load | Select | Clear;
