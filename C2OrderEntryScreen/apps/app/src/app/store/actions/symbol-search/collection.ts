import { Action } from '@ngrx/store';
import { SymbolSearch, SymbolSearchTypeAheadSupport} from '@app/models/LastQuoteService.dtos';
import { C2Error } from '@app/models/types';

export const ADD = '[SymbolSearch Collection] Add Symbol';
export const ADD_SUCCESS = '[SymbolSearch Collection] Add Symbol Success';
export const ADD_FAIL = '[SymbolSearch Collection] Add Symbol Fail';
export const REMOVE = '[SymbolSearch Collection] Remove Symbol';
export const REMOVE_SUCCESS = '[SymbolSearch Collection] Remove Symbol Success';
export const REMOVE_FAIL = '[SymbolSearch Collection] Remove Symbol Fail';
export const LOAD = '[SymbolSearch Collection] Load';
export const LOAD_SUCCESS = '[SymbolSearch Collection] Load Success';
export const LOAD_FAIL = '[SymbolSearch Collection] Load Fail';
export const DOWNLOAD_C2_ERROR = '[SymbolSearch Collection] C2 Error ';

/**
 * Add Book to Collection Actions
 */
export class Add implements Action {
  readonly type = ADD;

  constructor(public payload: SymbolSearchTypeAheadSupport) { }
}

export class AddSuccess implements Action {
  readonly type = ADD_SUCCESS;

  constructor(public payload: SymbolSearchTypeAheadSupport) { }
}

export class AddFail implements Action {
  readonly type = ADD_FAIL;

  constructor(public payload: SymbolSearchTypeAheadSupport) { }
}

/**
 * Remove Book from Collection Actions
 */
export class Remove implements Action {
  readonly type = REMOVE;

  constructor(public payload: SymbolSearchTypeAheadSupport) { }
}

export class RemoveSuccess implements Action {
  readonly type = REMOVE_SUCCESS;

  constructor(public payload: SymbolSearchTypeAheadSupport) { }
}

export class RemoveFail implements Action {
  readonly type = REMOVE_FAIL;

  constructor(public payload: SymbolSearchTypeAheadSupport) { }
}

/**
 * Load Collection Actions
 */
export class Load implements Action {
  readonly type = LOAD;
  constructor(public payload: SymbolSearch) { }
}

export class LoadSuccess implements Action {
  readonly type = LOAD_SUCCESS;

  constructor(public payload: SymbolSearchTypeAheadSupport[]) { }
}

export class LoadFail implements Action {
  readonly type = LOAD_FAIL;

  constructor(public payload: any) { }
}

export class DownloadC2Error implements Action {
  readonly type = DOWNLOAD_C2_ERROR;

  constructor(public payload: C2Error) { }
}

export type Actions =
  | Add
  | AddSuccess
  | AddFail
  | Remove
  | RemoveSuccess
  | RemoveFail
  | Load
  | LoadSuccess
  | LoadFail
  | DownloadC2Error
  ;
