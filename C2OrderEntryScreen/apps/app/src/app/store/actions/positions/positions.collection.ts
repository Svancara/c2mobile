import { Action } from '@ngrx/store';
import { C2Error, Position } from '@app/models';

export const ADD = '[Position Collection] Add';
export const ADD_SUCCESS = '[Position Collection] Add Success';
export const ADD_FAIL = '[Position Collection] Add Fail';
export const REMOVE = '[Position Collection] Remove';
export const REMOVE_SUCCESS = '[Position Collection] Remove Success';
export const REMOVE_FAIL = '[Position Collection] Remove Fail';
export const LOAD = '[Position Collection] Load';
export const LOAD_SUCCESS = '[Position Collection] Load Success';
export const LOAD_FAIL = '[Position Collection] Load Fail';
export const DOWNLOAD_C2_ERROR = '[Position Collection] C2 Error ';

/**
 * Add Position to Collection Actions
 */
export class Add implements Action {
  readonly type = ADD;

  constructor(public payload: Position) { }
}

export class AddSuccess implements Action {
  readonly type = ADD_SUCCESS;

  constructor(public payload: Position) { }
}

export class AddFail implements Action {
  readonly type = ADD_FAIL;

  constructor(public payload: Position) { }
}

/**
 * Remove Position from Collection Actions
 */
export class Remove implements Action {
  readonly type = REMOVE;

  constructor(public payload: Position) { }
}

export class RemoveSuccess implements Action {
  readonly type = REMOVE_SUCCESS;

  constructor(public payload: Position) { }
}

export class RemoveFail implements Action {
  readonly type = REMOVE_FAIL;

  constructor(public payload: Position) { }
}

/**
 * Load Collection Actions
 */
export class Load implements Action {
  readonly type = LOAD;
  constructor(public strategyId: number) { }
}

export class LoadSuccess implements Action {
  readonly type = LOAD_SUCCESS;

  constructor(public payload: Position[]) { }
}

export class LoadFail implements Action {
  readonly type = LOAD_FAIL;

  constructor(public payload: any) { }
}

export class DownloadC2Error implements Action {
  readonly type = DOWNLOAD_C2_ERROR;

  constructor(public payload: C2Error) { }
}

export type Actions =
  | Add
  | AddSuccess
  | AddFail
  | Remove
  | RemoveSuccess
  | RemoveFail
  | Load
  | LoadSuccess
  | LoadFail
  | DownloadC2Error;
