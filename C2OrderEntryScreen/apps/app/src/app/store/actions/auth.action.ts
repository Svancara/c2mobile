import { Action } from '@ngrx/store';
import { IAuthenticate } from '@app/models/user';
import { ILoginOK, ILoginError } from '@app/models';

export const LOGIN = '[Auth] Login';
export const LOGOUT = '[Auth] Logout';
export const LOGIN_SUCCESS = '[Auth] Login Success';
export const LOGIN_FAILURE = '[Auth] Login Failure';
export const LOGIN_REDIRECT = '[Auth] Login Redirect';
export const ADD_USER = '[Auth] Add user';
// export const REMOVE_USER = '[Auth] Remove user');


export class Login implements Action {
  readonly type = LOGIN;

  constructor(public payload: IAuthenticate) { }
}

export class LoginSuccess implements Action {
  readonly type = LOGIN_SUCCESS;

  constructor(public payload: ILoginOK, public redirectUrl: string) { }
}

export class LoginFailure implements Action {
  readonly type = LOGIN_FAILURE;

  constructor(public payload: ILoginError) { }
}

export class LoginRedirect implements Action {
  readonly type = LOGIN_REDIRECT;
  constructor(public redirectUrl: string) { }
}

export class Logout implements Action {
  readonly type = LOGOUT;
}

/**
 * User Actions
 */
export class AddUserAction implements Action {
  type = ADD_USER;

  constructor(public payload: ILoginOK) { }
}

//export class RemoveUserAction implements Action {
//  type = REMOVE_USER;

//  constructor(public payload: any) { }
//}


export type Actions =
  | Login
  | LoginSuccess
  | LoginFailure
  | LoginRedirect
  | Logout
  | AddUserAction
  //| RemoveUserAction
  ;
