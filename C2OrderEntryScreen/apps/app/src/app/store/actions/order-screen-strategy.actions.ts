import { Action } from '@ngrx/store';
import { Strategy } from '@app/models';


export const ORDER_SCREEN_ADD_STRATEGY = '[OrderScreenStrategy] Add';
export const ORDER_SCREEN_ADD_STRATEGY_SUCCESS = '[OrderScreenStrategy] Add Success';
export const ORDER_SCREEN_ADD_STRATEGY_FAILURE = '[OrderScreenStrategy] Add Failure';

export class AddOrderScreenStrategy implements Action {
  readonly type = ORDER_SCREEN_ADD_STRATEGY;

  constructor(public payload: Strategy) { }
}

export class AddOrderScreenStrategySuccess implements Action {
  readonly type = ORDER_SCREEN_ADD_STRATEGY_SUCCESS;

  constructor(public payload: any) { }
}

export class AddOrderScreenStrategyFailure implements Action {
  readonly type = ORDER_SCREEN_ADD_STRATEGY_FAILURE;
  constructor(public payload: any) { }
}


export type OrderScreenStrategyAction = AddOrderScreenStrategy | AddOrderScreenStrategySuccess | AddOrderScreenStrategyFailure; 
