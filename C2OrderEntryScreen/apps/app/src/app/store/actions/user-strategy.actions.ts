import { Action } from '@ngrx/store';
import { IStrategy as Entity } from '@app/models';

export const SEARCH = '[Strategy] Search';
export const SEARCH_COMPLETE = '[Strategy] Search Complete';
export const SEARCH_ERROR = '[Strategy] Search Error';
export const LOAD = '[Strategy] Load';
export const SELECT = '[Strategy] Select';
export const CLEAR = '[Strategy] Clear';

/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful
 * type checking in reducer functions.
 *
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */
export class Search implements Action {
  readonly type = SEARCH;

  constructor(public payload: string) { }
}

export class SearchComplete implements Action {
  readonly type = SEARCH_COMPLETE;

  constructor(public payload: Entity[]) { }
}

export class SearchError implements Action {
  readonly type = SEARCH_ERROR;

  constructor(public payload: string) { }
}

export class Load implements Action {
  readonly type = LOAD;

  constructor(public payload: Entity) { }
}

export class Select implements Action {
  readonly type = SELECT;

  constructor(public payload: string) { }
}

export class Clear implements Action {
  readonly type = CLEAR;
}

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type Actions = Search | SearchComplete | SearchError | Load | Select | Clear;
