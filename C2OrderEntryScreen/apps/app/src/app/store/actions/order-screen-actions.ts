import { Action } from '@ngrx/store';
import { OrderScreen, OrderScreenSymbol, SendSignalResponse, ISendSignal } from '@app/models/orderScreen';


export const SEARCH = '[OrderScreen] Search';
export const SEARCH_COMPLETE = '[OrderScreen] Search Complete';
export const SEARCH_ERROR = '[OrderScreen] Search Error';
export const SYMBOL_SELECTED = '[OrderScreen] Symbol selected';
export const SEND = '[OrderScreen] Send';
export const SEND_SUCCESS = '[OrderScreen] Send success';
export const SEND_FAIL = '[OrderScreen] Send fail';
export const SEND_C2_ERROR = '[OrderScreen] Send C2 Error response';

/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful
 * type checking in reducer functions.
 *
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */
export class Search implements Action {
  readonly type = SEARCH;

  constructor(public payload: string) { }
}

export class SearchComplete implements Action {
  readonly type = SEARCH_COMPLETE;

  constructor(public payload: OrderScreenSymbol[]) { }
}

export class SearchError implements Action {
  readonly type = SEARCH_ERROR;

  constructor(public payload: string) { }
}

export class SymbolSelected implements Action {
  readonly type = SYMBOL_SELECTED;

  constructor(public payload: OrderScreenSymbol) { }
}

export class Send implements Action {
  readonly type = SEND;

  constructor(public payload: ISendSignal) { }
}

export class SendSuccess implements Action {
  readonly type = SEND_SUCCESS;

  constructor(public payload: SendSignalResponse) { }
}

export class SendFail implements Action {
  readonly type = SEND_FAIL;

  constructor(public payload: any) { }
}
export class SendC2Error implements Action {
  readonly type = SEND_C2_ERROR;

  constructor(public payload: any) { }
}



/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type Actions =
  Search
  | SearchComplete
  | SearchError
  | SymbolSelected
  | Send
  | SendSuccess
  | SendFail
  | SendC2Error
  ;
