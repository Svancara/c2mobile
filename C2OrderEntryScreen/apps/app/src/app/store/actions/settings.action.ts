import { Action } from '@ngrx/store';

export const SET_LANGUAGE = '[Settings] SetLanguage';
export const SET_CULTURE = '[Settings] SetCulture';

/**
 * Settings Actions
 */
export class SetLanguageAction implements Action {
  readonly type = SET_LANGUAGE;

  constructor(public payload: string) { }
}

export class SetCultureAction implements Action {
  readonly type = SET_CULTURE;

  constructor(public payload: string) { }
}

export type Actions
  = SetLanguageAction
  | SetCultureAction;
