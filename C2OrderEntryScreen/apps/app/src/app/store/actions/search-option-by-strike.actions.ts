import { Action } from '@ngrx/store';
import { OptionSearchByStrike, OptionSymbolByStrike } from '@app/models/LastQuoteService.dtos';
import { C2Error } from '@app/models/types';

export const LOAD_BY_STRIKE = '[Option By Strike] Load ';
export const LOAD_BY_STRIKE_SUCCESS = '[Option By Strike] Load Success';
export const LOAD_BY_STRIKE_FAIL = '[Option By Strike] Load Fail';
export const CLEAR = '[Option By Strike] C2 Clear ';
export const DOWNLOAD_C2_ERROR = '[Option By Strike] C2 Error ';


export class LoadByStrike implements Action {
  readonly type = LOAD_BY_STRIKE;
  constructor(public payload: OptionSearchByStrike) { }
}

export class LoadByStrikeSuccess implements Action {
  readonly type = LOAD_BY_STRIKE_SUCCESS;
  constructor(public payload: OptionSymbolByStrike[]) { }
}

export class LoadByStrikeFail implements Action {
  readonly type = LOAD_BY_STRIKE_FAIL;
  constructor(public payload: any) { }
}

export class DownloadC2Error implements Action {
  readonly type = DOWNLOAD_C2_ERROR;
  constructor(public payload: C2Error) { }
}

export class Clear implements Action {
  readonly type = CLEAR;
}

export type Actions =
  | LoadByStrike
  | LoadByStrikeSuccess
  | LoadByStrikeFail
  | DownloadC2Error
  | Clear
  ;
