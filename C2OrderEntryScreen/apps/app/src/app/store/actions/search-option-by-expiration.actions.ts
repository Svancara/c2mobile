import { Action } from '@ngrx/store';
import { OptionSearchByExpiration, OptionSymbolByExpiration } from '@app/models/LastQuoteService.dtos';
import { C2Error } from '@app/models/types';

export const LOAD_BY_EXPIRATION = '[Option By Expiration] Load ';
export const LOAD_BY_EXPIRATION_SUCCESS = '[Option By Expiration] Load Success';
export const LOAD_BY_EXPIRATION_FAIL = '[Option By Expiration] Load Fail';
export const CLEAR = '[Option By Expiration] C2 Clear ';
export const DOWNLOAD_C2_ERROR = '[Option By Expiration] C2 Error ';


export class LoadByExpiration implements Action {
  readonly type = LOAD_BY_EXPIRATION;
  constructor(public payload: OptionSearchByExpiration) { }
}

export class LoadByExpirationSuccess implements Action {
  readonly type = LOAD_BY_EXPIRATION_SUCCESS;
  constructor(public payload: OptionSymbolByExpiration[]) { }
}

export class LoadByExpirationFail implements Action {
  readonly type = LOAD_BY_EXPIRATION_FAIL;
  constructor(public payload: any) { }
}

export class DownloadC2Error implements Action {
  readonly type = DOWNLOAD_C2_ERROR;
  constructor(public payload: C2Error) { }
}

export class Clear implements Action {
  readonly type = CLEAR;
}

export type Actions =
  | LoadByExpiration
  | LoadByExpirationSuccess
  | LoadByExpirationFail
  | DownloadC2Error
  | Clear
  ;
