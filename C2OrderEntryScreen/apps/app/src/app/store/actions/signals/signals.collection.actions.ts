import { Action } from '@ngrx/store';
import { Signal } from '@app/models/retrieveSignalsWorking';
import { C2Error } from '@app/models/types';

export const ADD = '[Signal Collection] Add';
export const ADD_SUCCESS = '[Signal Collection] Add Success';
export const ADD_FAIL = '[Signal Collection] Add Fail';
export const REMOVE = '[Signal Collection] Remove';
export const REMOVE_SUCCESS = '[Signal Collection] Remove Success';
export const REMOVE_FAIL = '[Signal Collection] Remove Fail';
export const LOAD = '[Signal Collection] Load';
export const LOAD_SUCCESS = '[Signal Collection] Load Success';
export const LOAD_FAIL = '[Signal Collection] Load Fail';
export const DOWNLOAD_C2_ERROR = '[Signal Collection] C2 Error ';

/**
 * Add Signal to Collection Actions
 */
export class Add implements Action {
  readonly type = ADD;

  constructor(public payload: Signal) { }
}

export class AddSuccess implements Action {
  readonly type = ADD_SUCCESS;

  constructor(public payload: Signal) { }
}

export class AddFail implements Action {
  readonly type = ADD_FAIL;

  constructor(public payload: Signal) { }
}

/**
 * Remove Signal from Collection Actions
 */
export class Remove implements Action {
  readonly type = REMOVE;

  constructor(public payload: Signal) { }
}

export class RemoveSuccess implements Action {
  readonly type = REMOVE_SUCCESS;

  constructor(public payload: Signal) { }
}

export class RemoveFail implements Action {
  readonly type = REMOVE_FAIL;

  constructor(public payload: Signal) { }
}

/**
 * Load Collection Actions
 */
export class Load implements Action {
  readonly type = LOAD;
  constructor(public strategyId: number) { }
}

export class LoadSuccess implements Action {
  readonly type = LOAD_SUCCESS;

  constructor(public payload: Signal[]) { }
}

export class LoadFail implements Action {
  readonly type = LOAD_FAIL;

  constructor(public payload: any) { }
}

export class DownloadC2Error implements Action {
  readonly type = DOWNLOAD_C2_ERROR;

  constructor(public payload: C2Error) { }
}

//export class Clear implements Action {
//  readonly type = CLEAR;
//}

export type Actions =
  | Add
  | AddSuccess
  | AddFail
  | Remove
  | RemoveSuccess
  | RemoveFail
  | Load
  | LoadSuccess
  | LoadFail
  | DownloadC2Error
  ;
