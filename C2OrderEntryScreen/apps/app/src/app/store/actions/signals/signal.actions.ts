import { Action } from '@ngrx/store';
import { Signal } from '@app/models';



export const SEARCH = '[Signal] Search';
export const SEARCH_COMPLETE = '[Signal] Search Complete';
export const SEARCH_ERROR = '[Signal] Search Error';
export const LOAD = '[Signal] Load';
export const SELECT = '[Signal] Select';
export const CLEAR = '[Signal] Clear';

/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful
 * type checking in reducer functions.
 *
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */
export class Search implements Action {
  readonly type = SEARCH;

  constructor(public payload: string) { }
}

export class SearchComplete implements Action {
  readonly type = SEARCH_COMPLETE;

  constructor(public payload: Signal[]) { }
}

export class SearchError implements Action {
  readonly type = SEARCH_ERROR;

  constructor(public payload: string) { }
}

export class Load implements Action {
  readonly type = LOAD;

  constructor(public payload: Signal) { }
}

export class Select implements Action {
  readonly type = SELECT;

  constructor(public payload: string) { }
}

export class Clear implements Action {
  readonly type = CLEAR;
}

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type Actions = Search | SearchComplete | SearchError | Load | Select | Clear;
