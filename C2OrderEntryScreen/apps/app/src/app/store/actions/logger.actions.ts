import { Action } from '@ngrx/store';
import { ISendSignal, ISendSignalResponse, CancelSignalResponse, Position, Signal } from '@app/models';
// import { ILogState, ILogItem } from '@app/store/reducers/logger.interfaces';

// import { type } from '../../utility';

export interface ILogItem {
  timestamp: number; // Date;
  type: string;
  info: any;
}

//export const ActionTypes = {
//  SAVE_SIGNAL: type('[Log] Save Signal Sent'),
//  SAVE_SIGNAL_RESPONSE: type('[Log] Save Signal Response'),
//  SAVE_SIGNAL_CANCELLATION: type('[Log] Save Signal Cancel'),
//  SAVE_SIGNAL_CANCELLATION_RESPONSE: type('[Log] Save Signal Cancel Response'),
//  SAVE_ERROR_ITEM: type('[Log] Save Error Item'),
//  READ_LOG: type('[Log] ReadLog'),
//  READ_LOG_DONE: type('[Log] ReadLog Done'),
//  CLEAR_LOG: type('[Log] ClearLog'),
//  CLEAR_LOG_DONE: type('[Log] ClearLog Done')
//}

export const SAVE_SIGNAL = '[Log] Save Signal Sent';
export const SAVE_SIGNAL_RESPONSE = '[Log] Save Signal Response';
export const SAVE_SIGNAL_CANCELLATION = '[Log] Save Signal Cancel';
export const SAVE_SIGNAL_CANCELLATION_RESPONSE = '[Log] Save Signal Cancel Response';
export const SAVE_ERROR_ITEM = '[Log] Save Error Item';
export const READ_LOG = '[Log] ReadLog';
export const READ_LOG_DONE = '[Log] ReadLog Done';
export const CLEAR_LOG = '[Log] ClearLog';
export const CLEAR_LOG_DONE = '[Log] ClearLog Done';
//export const LOGGER_DONE = '[Log] Logger done';
export const CLOSE_POSITION = '[Log] Close position';
//export const SAVE_GENERAL_ERROR = '[Log] General error';
// export const CLOSE_POSITION_RESPONSE = '[Log] Close position response';



export class SaveSignal implements Action {
  readonly type = SAVE_SIGNAL;

  constructor(public payload: ISendSignal) { }
}

export class SaveSignalResponse implements Action {
  readonly type = SAVE_SIGNAL_RESPONSE;

  constructor(public signalId: number, public payload: ISendSignalResponse) { }
}

export class ClosePosition implements Action {
  readonly type = CLOSE_POSITION;

  constructor(public payload: Position) { }
}

//export class ClosePositionResponse implements Action {
//  readonly type = CLOSE_POSITION_RESPONSE;

//  constructor(public payload: ISendSignalResponse) { }
//}


export class SaveSignalCancellation implements Action {
  readonly type = SAVE_SIGNAL_CANCELLATION;

  constructor(public payload: Signal) { }
}

export class SaveSignalCancellationResponse implements Action {
  readonly type = SAVE_SIGNAL_CANCELLATION_RESPONSE;

  constructor(public signalId: number, public payload: CancelSignalResponse) { }
}

export class SaveErrorItem implements Action {
  readonly type = SAVE_ERROR_ITEM;

  constructor(public payload: ILogItem) { }
}

//export class SaveGeneralError implements Action {
//  readonly type = SAVE_GENERAL_ERROR;

//  constructor(public payload: any) { }
//}

export class ReadLog implements Action {
  readonly type = READ_LOG;
  constructor(public payload?: any) { }
}

export class ReadLogDone implements Action {
  readonly type = READ_LOG_DONE;

  constructor(public payload?: any) { }
}

export class ClearLog implements Action {
  readonly type = CLEAR_LOG;
  constructor(public payload?: any) { }
}

export class ClearLogDone implements Action {
  readonly type = CLEAR_LOG_DONE;
  constructor(public payload?: any) { }
}

//export class LoggerDone implements Action {
//  readonly type = LOGGER_DONE;

//  constructor(public payload: any) { }
//}

export type LoggerAction =
  | SaveSignal
  | SaveSignalResponse
  | SaveSignalCancellation
  | SaveSignalCancellationResponse
  | SaveErrorItem
  | ReadLog
  | ReadLogDone
  | ClearLog
  | ClearLogDone
  //| LoggerDone
  | ClosePosition
  //| SaveGeneralError
  ;
