import { Action } from '@ngrx/store';
import { IStrategy } from '@app/models';
import { C2Error } from '../../models/types';

export const ADD = '[Strategy Collection] Add';
export const ADD_SUCCESS = '[Strategy Collection] Add Success';
export const ADD_FAIL = '[Strategy Collection] Add Fail';
export const REMOVE = '[Strategy Collection] Remove';
export const REMOVE_SUCCESS = '[Strategy Collection] Remove Success';
export const REMOVE_FAIL = '[Strategy Collection] Remove Fail';
export const LOAD = '[Strategy Collection] Load';
export const LOAD_SUCCESS = '[Strategy Collection] Load Success';
export const LOAD_FAIL = '[Strategy Collection] Load Fail';
export const DOWNLOAD_C2_ERROR = '[Strategy Collection] C2 Error ';

/**
 * Add Strategy to Collection Actions
 */
export class Add implements Action {
  readonly type = ADD;

  constructor(public payload: IStrategy) { }
}

export class AddSuccess implements Action {
  readonly type = ADD_SUCCESS;

  constructor(public payload: IStrategy) { }
}

export class AddFail implements Action {
  readonly type = ADD_FAIL;

  constructor(public payload: IStrategy) { }
}

/**
 * Remove Strategy from Collection Actions
 */
export class Remove implements Action {
  readonly type = REMOVE;

  constructor(public payload: IStrategy) { }
}

export class RemoveSuccess implements Action {
  readonly type = REMOVE_SUCCESS;

  constructor(public payload: IStrategy) { }
}

export class RemoveFail implements Action {
  readonly type = REMOVE_FAIL;

  constructor(public payload: IStrategy) { }
}

/**
 * Load Collection Actions
 */
export class Load implements Action {
  readonly type = LOAD;
  //constructor(public payload: number) { }
}

export class LoadSuccess implements Action {
  readonly type = LOAD_SUCCESS;

  constructor(public payload: IStrategy[]) { }
}

export class LoadFail implements Action {
  readonly type = LOAD_FAIL;

  constructor(public payload: any) { }
}

export class DownloadC2Error implements Action {
  readonly type = DOWNLOAD_C2_ERROR;

  constructor(public payload: C2Error) { }
}


export type Actions =
  | Add
  | AddSuccess
  | AddFail
  | Remove
  | RemoveSuccess
  | RemoveFail
  | Load
  | LoadSuccess
  | LoadFail
  | DownloadC2Error;
