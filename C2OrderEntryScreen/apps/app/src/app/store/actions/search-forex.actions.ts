import { Action } from '@ngrx/store';
import { ForexSymbol, ForexPairs } from '@app/models/LastQuoteService.dtos';
import { C2Error } from '@app/models/types';
import { GetLastQuote } from '@app/models';

export const LOAD = '[Load forex] Load ';
export const LOAD_SUCCESS = '[Load forex] Load Success';
export const LOAD_FAIL = '[Load forex] Load Fail';
export const SELECT = '[Load forex] Select';
export const CLEAR = '[Load forex] Clear ';
export const DOWNLOAD_C2_ERROR = '[Load forex] C2 Error ';


export class Load implements Action {
  readonly type = LOAD;
  // I need payload.returnToRoute
  constructor(public payload: ForexPairs) { }
}

export class LoadSuccess implements Action {
  readonly type = LOAD_SUCCESS;
  constructor(public payload: ForexSymbol[]) { }
}

export class LoadFail implements Action {
  readonly type = LOAD_FAIL;
  constructor(public payload: any) { }
}

export class DownloadC2Error implements Action {
  readonly type = DOWNLOAD_C2_ERROR;
  constructor(public payload: C2Error) { }
}

export class Clear implements Action {
  readonly type = CLEAR;
}

export class Select implements Action {
  readonly type = SELECT;
  constructor(public payload: GetLastQuote) { }
}

export type Actions =
  | Load
  | LoadSuccess
  | LoadFail
  | Select
  | DownloadC2Error
  | Clear
  ;
