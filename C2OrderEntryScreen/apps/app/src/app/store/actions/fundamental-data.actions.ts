import { Action } from '@ngrx/store';
import { FundamentalDataResponse, ResponseStatus, GetLastQuote, C2Error } from '@app/models';
import { HttpErrorResponse } from '@angular/common/http';

export const LOAD_DATA = '[FundamentalData] Load';
export const LOAD_DATA_SUCCESS = '[FundamentalData] Load Success';
export const LOAD_DATA_FAILURE = '[FundamentalData] Load Failure';

export class LoadData implements Action {
  readonly type = LOAD_DATA;

  constructor(public payload: GetLastQuote // FundamentalData, but LastQuote is easier
  ) { }
}

export class LoadDataSuccess implements Action {
  readonly type = LOAD_DATA_SUCCESS;

  constructor(public payload: FundamentalDataResponse) { }
}

export class LoadDataFailure implements Action {
  readonly type = LOAD_DATA_FAILURE;

  constructor(public payload: ResponseStatus | HttpErrorResponse | C2Error| string) { }
}


export type FundamentalDataAction = LoadData | LoadDataSuccess | LoadDataFailure; 
