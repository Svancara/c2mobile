import { Action } from '@ngrx/store';
import { GetLastQuote, ResponseStatus, LastQuoteData, C2Error } from '@app/models';
import { HttpErrorResponse } from '@angular/common/http';

export const LOAD_DATA = '[LastQuote] Load';
export const LOAD_DATA_SUCCESS = '[LastQuote] Load Success';
export const LOAD_DATA_FAILURE = '[LastQuote] Load Failure';

export class LoadData implements Action {
  readonly type = LOAD_DATA;

  constructor(public payload: GetLastQuote) { }
}

export class LoadDataSuccess implements Action {
  readonly type = LOAD_DATA_SUCCESS;

  constructor(public payload: LastQuoteData) { }
}

export class LoadDataFailure implements Action {
  readonly type = LOAD_DATA_FAILURE;

  constructor(public payload: ResponseStatus | HttpErrorResponse | C2Error | string) { }
}


export type LastQuoteAction = LoadData | LoadDataSuccess | LoadDataFailure; 
