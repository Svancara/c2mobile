import { Action } from '@ngrx/store';
import { InternaSearchSymbolRequest, GetLastQuote } from '@app/models';

export const SET_SEARCH_SYMBOL_PARTIAL_QUERY = '[GP] Search symbol part query';
export const SELECTED_OPTION_SYMBOL = '[GP] Selected option symbol';

/**
 * Search symbol - a query text to be transported
 */
export class SetSearchSymbolPartialQuery implements Action {
  readonly type = SET_SEARCH_SYMBOL_PARTIAL_QUERY;

  constructor(public payload: InternaSearchSymbolRequest) { }
}

export class SelectedOptionSymbol implements Action {
  readonly type = SELECTED_OPTION_SYMBOL;

  constructor(public payload: GetLastQuote) { }
}

export type Actions = SetSearchSymbolPartialQuery | SelectedOptionSymbol;
