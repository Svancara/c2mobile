import * as collection from '@app/store/actions/symbol-search/collection';

export interface State {
  loaded: boolean;
  loading: boolean;
  failed: boolean;
  ids: number[];
}

const initialState: State = {
  loaded: false,
  loading: false,
  failed: false,
  ids: [],
};

export function reducer(
  state = initialState,
  action: collection.Actions
): State {
  switch (action.type) {
    case collection.LOAD: {
      return {
        ...state,
        loading: true,
      };
    }

    case collection.LOAD_SUCCESS: {
      return {
        loaded: true,
        loading: false,
        failed: false,
        ids: action.payload.map(book => book.Guid),
      };
    }

    case collection.ADD_SUCCESS:
    case collection.REMOVE_FAIL: {
      if (state.ids.indexOf(action.payload.Guid) > -1) {
        return state;
      }

      return {
        ...state,
        ids: [...state.ids, action.payload.Guid],
      };
    }

    case collection.REMOVE_SUCCESS:
    case collection.ADD_FAIL: {
      return {
        ...state,
        ids: state.ids.filter(id => id !== action.payload.Guid),
      };
    }

    case collection.DOWNLOAD_C2_ERROR:
    case collection.LOAD_FAIL: {
      return {
        ...state,
        loaded: false,
        loading: false,
        failed: true,
        ids: []
      };
    }      

    default: {
      return state;
    }
  }
}

export const getLoaded = (state: State) => state.loaded;
export const getLoading = (state: State) => state.loading;
export const getFailed = (state: State) => state.failed;
export const getIds = (state: State) => state.ids;
