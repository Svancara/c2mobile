import * as settings from '@app/store/actions/settings.action';

export interface State {
  selectedLanguage:   string;
  selectedCulture:    string;
  availableLanguages: Array<any>
};

const INITIAL_STATE: State = {
  selectedLanguage: '',
  selectedCulture:  '',
  availableLanguages: [
    {code: 'hr', name: 'HR', culture: 'hr-HR'},
    {code: 'en', name: 'EN', culture: 'en-EN'}
  ]
};

export const getSelectedLanguage = (state: State) => state.selectedLanguage;
export const getSelectedCulture = (state: State) => state.selectedCulture;
export const getAvailableLanguages = (state: State) => state.availableLanguages;

export function reducer(state = INITIAL_STATE, action: settings.Actions): State {
  switch (action.type) {
    case settings.SET_LANGUAGE: {
      // return Object.assign({}, state, { selectedLanguage: action.payload });
      return {
        ...state, selectedLanguage : action.payload
      };
    }

    case settings.SET_CULTURE: {
//      return Object.assign({}, state, { selectedCulture: action.payload });
      return {
        ...state, selectedCulture: action.payload
      };
    }

    default: {
      return state;
    }
  }
}

