import * as auth from '@app/store/actions/auth.action';
import { LoginOK } from '@app/models';

export interface State {
  loggedIn: boolean;
  user: LoginOK;
}

export const initialState: State = {
  loggedIn: false,
  user: null,
};

export function reducer(state = initialState, action: auth.Actions): State {
  switch (action.type) {
    case auth.LOGIN_SUCCESS: {
      return {
        ...state,
        loggedIn: true,
        user: (action as auth.LoginSuccess).payload,
      };
    }

    case auth.ADD_USER: {
      let usr = (action as auth.AddUserAction).payload;
      //let _loggedIn: boolean = (usr !== null) && (usr !== undefined) && !!usr.APIkey;
      let _loggedIn: boolean = !!usr && !!usr.APIkey;
      return { ...state, user: (action as auth.AddUserAction).payload, loggedIn: _loggedIn };
    }

    case auth.LOGIN:
    case auth.LOGIN_FAILURE:
    case auth.LOGIN_REDIRECT:
    case auth.LOGOUT: {
      return initialState;
    }

    default: {
      return state;
    }
  }
}

export const getLoggedIn = (state: State) => state.loggedIn;
export const getUser = (state: State) => state.user;
export const getApiKey = (state: State) => state.user.APIkey;
