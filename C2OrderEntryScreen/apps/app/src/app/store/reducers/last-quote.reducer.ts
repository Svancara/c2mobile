import * as actions from '@app/store/actions/last-quote.actions';
import { LastQuoteData } from '@app/models';
// import * as Immutable from 'immutable';
// import { Map } from 'immutable';

export interface State {
//  lastQuoteData: Map<string, LastQuoteData>;
  lastQuoteData: LastQuoteData;
};

const INITIAL_STATE: State = {
//  lastQuoteData: Map<string, LastQuoteData>()
  lastQuoteData: new LastQuoteData()
};

export function reducer(state = INITIAL_STATE, action: actions.LastQuoteAction): State {
  switch (action.type) {
    case actions.LOAD_DATA_SUCCESS: {
      return {
//        ...state, lastQuoteData: state.lastQuoteData.set(action.payload.symbol, action.payload)
        ...state, lastQuoteData: action.payload
      };
    }

    default: {
      return state;
    }
  }
}

export const getLastQuotesMap = (state: State) => state.lastQuoteData;
//export const getLastQuote = (symbol: string, state: State) => state.lastQuoteData.get(symbol);
export const getLastQuote = (symbol: string, state: State) => state.lastQuoteData;
