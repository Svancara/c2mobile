/*
  General purposes reducer
*/

import * as fromRoot from '@app/reducers';
import * as gp from '@app/store/actions/gp.action';
import { InternaSearchSymbolRequest, GetLastQuote, InstrumentString } from '@app/models';
import { SymbolSearchTypeAheadSupport } from '@app/models/LastQuoteService.dtos';


export interface State {
  searchSymbolPartialQuery: InternaSearchSymbolRequest;
  selectedOptionSymbol: GetLastQuote;
};

const INITIAL_STATE: State = {
  searchSymbolPartialQuery: { query: '', returnToRoute: ["/"], instrument: InstrumentString.stock },
  selectedOptionSymbol: undefined
};

export function reducer(state = INITIAL_STATE, action: gp.Actions): State {
  switch (action.type) {
    case gp.SET_SEARCH_SYMBOL_PARTIAL_QUERY: {
      return {
        ...state, searchSymbolPartialQuery: action.payload
      };
    }
      
    case gp.SELECTED_OPTION_SYMBOL: {
      return {
        ...state, selectedOptionSymbol: action.payload
      };
    }

    default: {
      return state;
    }
  }
}

export const getSearchSymbolPartialQuery = (state: State) => state.searchSymbolPartialQuery;
export const getSelectedOption = (state: State) => state.selectedOptionSymbol;

