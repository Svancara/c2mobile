import { createSelector, createFeatureSelector } from '@ngrx/store';
import * as actions from '@app/store/actions/search-option-by-expiration.actions';
import * as fromParent from '../index';
import { OptionSymbolByExpiration } from '@app/models/LastQuoteService.dtos';

export interface OptionSearchByExpirationState {
  collection: OptionSymbolByExpiration[];
  loading:boolean;
}

export interface State extends fromParent.State {
  'optionsByExpiration': OptionSearchByExpirationState;
}

const INITIAL_STATE: OptionSearchByExpirationState = {
  collection: [],
  loading:false
};

export function reducer(state = INITIAL_STATE, action: actions.Actions): OptionSearchByExpirationState {
  switch (action.type) {

    case actions.LOAD_BY_EXPIRATION: {
      return {
        ...state, loading: true
      };
    }

    case actions.LOAD_BY_EXPIRATION_SUCCESS: {
      return {
        ...state, collection: action.payload, loading: false
      };
    }

  case actions.CLEAR:
  case actions.DOWNLOAD_C2_ERROR:
  case actions.LOAD_BY_EXPIRATION_FAIL:
    return INITIAL_STATE;

    default: {
      return state;
    }
  }
}


/**
 * The createFeatureSelector function selects a piece of state from the root of the state object.
 * This is used for selecting feature states that are loaded eagerly or lazily.
*/
export const getOptionsByExpState = createFeatureSelector<OptionSearchByExpirationState>('optionsByExpiration');

/**
 * Every reducer module exports selector functions, however child reducers
 * have no knowledge of the overall state tree. To make them usable, we
 * need to make new selectors that wrap them.
 *
 * The createSelector function creates very efficient selectors that are memoized and
 * only recompute when arguments change. The created selectors can also be composed
 * together to select different pieces of state.
 */
export const getOptionsByExpirations = createSelector(
  getOptionsByExpState,
  state => state.collection
);

export const getOptionsByExpirationsLoading = createSelector(
  getOptionsByExpState,
  state => state.loading
);
