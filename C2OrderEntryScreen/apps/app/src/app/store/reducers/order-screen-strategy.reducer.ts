import * as actions from '@app/store/actions/order-screen-strategy.actions';
import { Strategy } from '@app/models';


export interface State {
  orderScreenStrategy : Strategy;
};

const INITIAL_STATE: State = {
  orderScreenStrategy: new Strategy()
};

export function reducer(state = INITIAL_STATE, action: actions.OrderScreenStrategyAction): State {
  switch (action.type) {
    case actions.ORDER_SCREEN_ADD_STRATEGY: {
      return {
        ...state, orderScreenStrategy: action.payload
      };
    }

    default: {
      return state;
    }
  }
}

export const getOrderScreenStrategy = (state: State): Strategy => state.orderScreenStrategy;

