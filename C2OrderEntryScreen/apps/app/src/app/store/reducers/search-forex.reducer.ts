import { createSelector, createFeatureSelector } from '@ngrx/store';
import * as actions from '@app/store/actions/search-forex.actions';
import * as fromParent from '../index';
import { ForexSymbol } from '@app/models/LastQuoteService.dtos';
import { GetLastQuote } from '@app/models';

export interface ForexSymbolsState {
  collection: ForexSymbol[];
  selectedSymbol: GetLastQuote;
  loading: boolean;
}

export interface State extends fromParent.State {
  'forexSymbols': ForexSymbolsState;
}

const INITIAL_STATE: ForexSymbolsState = {
  collection: [],
  selectedSymbol: undefined,
  loading: false
};

export function reducer(state = INITIAL_STATE, action: actions.Actions): ForexSymbolsState {
  switch (action.type) {

    case actions.LOAD: {
      return {
        ...state, loading: true
      };
    }

    case actions.LOAD_SUCCESS: {
      return {
        ...state, collection: action.payload, loading: false
      };
    }

    case actions.SELECT: {
      return {
        ...state, selectedSymbol: action.payload
      };
    }

    case actions.DOWNLOAD_C2_ERROR:
    case actions.LOAD_FAIL:
      return {
        ...state, collection: [], selectedSymbol: undefined, loading: false
      };

    case actions.CLEAR:
      return {
        ...state, collection: [], selectedSymbol: undefined
      };

    default: {
      return state;
    }
  }
}


/**
 * The createFeatureSelector function selects a piece of state from the root of the state object.
 * This is used for selecting feature states that are loaded eagerly or lazily.
*/
export const getForexSymbolsState = createFeatureSelector<ForexSymbolsState>('forexSymbols');

/**
 * Every reducer module exports selector functions, however child reducers
 * have no knowledge of the overall state tree. To make them usable, we
 * need to make new selectors that wrap them.
 *
 * The createSelector function creates very efficient selectors that are memoized and
 * only recompute when arguments change. The created selectors can also be composed
 * together to select different pieces of state.
 */
export const getForexSymbols = createSelector(
  getForexSymbolsState,
  state => state.collection
);

export const getForexSymbolsLoading = createSelector(
  getForexSymbolsState,
  state => state.loading
);

export const getSelectedForexSymbol = createSelector(
  getForexSymbolsState,
  state => state.selectedSymbol
);
