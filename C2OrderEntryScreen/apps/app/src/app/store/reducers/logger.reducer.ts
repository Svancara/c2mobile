import * as logger from '@app/store/actions/logger.actions';


export interface State {
  //  items: (ILogItem | ILogSignal | ILogSignalSentResponse)[] //  Array<ILogSignal | ILogSignalSentResponse >
  items: logger.ILogItem[] //  Array<ILogSignal | ILogSignalSentResponse >
};


export const INITIAL_LOG_STATE: State = {
  items: []
};


export function reducer(state = INITIAL_LOG_STATE, action: logger.LoggerAction): State {
  switch (action.type) {

    case logger.SAVE_SIGNAL:
    case logger.SAVE_SIGNAL_RESPONSE:
    case logger.SAVE_SIGNAL_CANCELLATION:
    case logger.SAVE_SIGNAL_CANCELLATION_RESPONSE:
    case logger.SAVE_SIGNAL_CANCELLATION:
    case logger.SAVE_SIGNAL_CANCELLATION_RESPONSE:
    case logger.CLOSE_POSITION:
    case logger.SAVE_ERROR_ITEM: {
      // This is correct, but it means that I will have the same log in the logger memory and the same in local storage
      // return { ...state, items: [...state.items, { timestamp: Date.now(), type: action.type, info: action.payload }] }
      // So lets return just a log item formated like this:
      return { items: [{ timestamp: Date.now(), type: action.type, info: action.payload }] }; // Handled in effects
    };

    //case logger.LOGGER_DONE: {
    //  console.log("Logger reducer - LOGGER_DONE", state);
    //  return state;
    //};

    case logger.READ_LOG:
    // case logger.ActionTypes.READ_LOG_DONE:
    case logger.CLEAR_LOG:
      //case logger.ActionTypes.CLEAR_LOG_DONE:
      {
        return state;
      };


    default: {
      return state;
    }
  }
}

export const getLogState = (state: State) => state;
export const getLogItems = (state: State) => state.items;
