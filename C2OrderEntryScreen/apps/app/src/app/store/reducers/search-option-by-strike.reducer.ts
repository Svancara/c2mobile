import { createSelector, createFeatureSelector } from '@ngrx/store';
import * as actions from '@app/store/actions/search-option-by-strike.actions';
import * as fromParent from '../index';
import { OptionSymbolByStrike } from '@app/models/LastQuoteService.dtos';

export interface OptionSearchByStrikeState {
  collection: OptionSymbolByStrike[];
  loading:boolean;
}

export interface State extends fromParent.State {
  'optionsByStrike': OptionSearchByStrikeState;
}

const INITIAL_STATE: OptionSearchByStrikeState = {
  collection: [],
  loading:false
};

export function reducer(state = INITIAL_STATE, action: actions.Actions): OptionSearchByStrikeState {
  switch (action.type) {

    case actions.LOAD_BY_STRIKE:{
      return {
        ...state, loading: true
      };
    }

    case actions.LOAD_BY_STRIKE_SUCCESS: {
      return {
        ...state, collection: action.payload, loading: false
      };
    }

  case actions.CLEAR:
  case actions.DOWNLOAD_C2_ERROR:
  case actions.LOAD_BY_STRIKE_FAIL:
    return INITIAL_STATE;

    default: {
      return state;
    }
  }
}


/**
 * The createFeatureSelector function selects a piece of state from the root of the state object.
 * This is used for selecting feature states that are loaded eagerly or lazily.
*/
export const getOptionsByStrikeExpState = createFeatureSelector<OptionSearchByStrikeState>('optionsByStrike');

/**
 * Every reducer module exports selector functions, however child reducers
 * have no knowledge of the overall state tree. To make them usable, we
 * need to make new selectors that wrap them.
 *
 * The createSelector function creates very efficient selectors that are memoized and
 * only recompute when arguments change. The created selectors can also be composed
 * together to select different pieces of state.
 */
export const getOptionsByStrikes = createSelector(
  getOptionsByStrikeExpState,
  state => state.collection
);

export const getOptionsByStrikesLoading = createSelector(
  getOptionsByStrikeExpState,
  state => state.loading
);
