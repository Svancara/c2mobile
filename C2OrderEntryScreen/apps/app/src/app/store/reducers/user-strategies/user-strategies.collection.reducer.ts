import * as collection from '@app/store/actions/user-strategies.collection.actions';

export interface State {
  loaded: boolean;
  loading: boolean;
  failed: boolean;
  ids: string[];
}

const initialState: State = {
  loaded: false,
  loading: false,
  failed: false,
  ids: [],
};

export function reducer(
  state = initialState,
  action: collection.Actions
): State {
  switch (action.type) {
    case collection.LOAD: {
      return {
        ...state,
        loading: true,
      };
    }

    case collection.LOAD_SUCCESS: {
      return {
        loaded: true,
        loading: false,
        failed:false,
        ids: action.payload.map(entity => entity.id),
      };
    }

    case collection.ADD_SUCCESS:
    case collection.REMOVE_FAIL: {
      if (state.ids.indexOf(action.payload.id) > -1) {
        return state;
      }

      return {
        ...state,
        ids: [...state.ids, action.payload.id],
      };
    }

    case collection.REMOVE_SUCCESS:
    case collection.ADD_FAIL: {
      return {
        ...state,
        ids: state.ids.filter(id => id !== action.payload.id),
      };
    }

    case collection.DOWNLOAD_C2_ERROR:
    case collection.LOAD_FAIL: {
      return {
        ...state,
        loaded: false,
        loading: false,
        failed: true,
        ids: []
      };
    }      

    default: {
      return state;
    }
  }
}

export const getLoaded = (state: State) => state.loaded;
export const getLoading = (state: State) => state.loading;
export const getFailed = (state: State) => state.failed;
export const getIds = (state: State) => state.ids;
