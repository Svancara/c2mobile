import * as actions from '@app/store/actions/fundamental-data.actions';
import { FundamentalDataResponse } from '@app/models';
// import * as Immutable from 'immutable';
// import { Map } from 'immutable';

export interface State {
//  fundamentalData: Map<string, FundamentalDataResponse>;
  fundamentalData: FundamentalDataResponse;
};

const INITIAL_STATE: State = {
//  fundamentalData: Map<string, FundamentalDataResponse>()
  fundamentalData: new FundamentalDataResponse()
};

export function reducer(state = INITIAL_STATE, action: actions.FundamentalDataAction): State {
  switch (action.type) {
    case actions.LOAD_DATA_SUCCESS: {
      return {
//        ...state, fundamentalData: state.fundamentalData.set(action.payload.Symbol, action.payload)
        ...state, fundamentalData: action.payload
      };
    }

    default: {
      return state;
    }
  }
}

export const getFundamentalDataMap = (state: State) => state.fundamentalData;
//export const getFundamentalData = (symbol: string, state: State) => state.fundamentalData.get(symbol);
export const getFundamentalData = (symbol: string, state: State) => state.fundamentalData;
