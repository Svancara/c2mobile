import * as orderScreenActions from '@app/store/actions/order-screen-actions';
import { OrderScreen, OrderScreenSymbol } from '@app/models';

export interface State {
  searchSymbol: string;
  orderScreenSymbol: OrderScreenSymbol;
  orderScreen: OrderScreen
}

const initialState: State = {
  searchSymbol: "",
  orderScreenSymbol: undefined,
  orderScreen: undefined,
};

export function reducer(state = initialState, action: orderScreenActions.Actions): State {
  switch (action.type) {
    case orderScreenActions.SEARCH:
      return {
        ...state, searchSymbol: action.payload
      };

    case orderScreenActions.SYMBOL_SELECTED:
      return {
        ...state, orderScreenSymbol: action.payload
      };

    case orderScreenActions.SEND:
      return {
        ...state
      };

    default:
      return state;
  }
}

export const getOrderScreen = (state: State) => state.orderScreen;
export const getOrderScreenSymbol = (state: State) => state.orderScreenSymbol;

