/**
 * More info: https://egghead.io/lessons/javascript-redux-implementing-combinereducers-from-scratch
 */

import {
  ActionReducerMap,
  createSelector,
  createFeatureSelector,
  ActionReducer,
  MetaReducer,
} from '@ngrx/store';

import { environment } from '@env/environment';
import { RouterStateUrl } from '../shared/ngrx-utils';
import * as fromRouter from '@ngrx/router-store';
// import { User } from '@app/models/user';


/**
 * storeFreeze prevents state from being mutated. When mutation occurs, an
 * exception will be thrown. This is useful during development mode to
 * ensure that none of the reducers accidentally mutates the state.
 */
import { storeFreeze } from 'ngrx-store-freeze';

/**
 * Every reducer module's default export is the reducer function itself. In
 * addition, each module should export a type or interface that describes
 * the state of the reducer plus any selector functions. The `* as`
 * notation packages up all of the exports into a single object.
 */

import * as fromSettings from '@app/store/reducers/settings.reducer';
import * as fromUserStrategies from '@app/store/reducers/user-strategies';
import * as fromPositions from '@app/store/reducers/positions';
import * as fromSignals from '@app/store/reducers/signals';
import * as fromAuth from '@app/store/reducers/auth';
import * as fromLogger from '@app/store/reducers/logger.reducer';
import * as fromLastQuotes from '@app/store/reducers/last-quote.reducer';
import * as fromFundamentalData from '@app/store/reducers/fundamental-data.reducer';
import * as fromGp from '@app/store/reducers/gp.reducer';
import * as fromOrderScreenStrategy from '@app/store/reducers/order-screen-strategy.reducer';
// import * as fromSymbolSearch from '@app/store/reducers/symbol-search';


/**
 * We treat each reducer like a table in a database. This means
 * our top level state interface is just a map of keys to inner state types.
 */
export interface State {
  routerReducer: fromRouter.RouterReducerState<RouterStateUrl>;
  user: fromAuth.State;
  userStrategies: fromUserStrategies.State;
  positions: fromPositions.State;
  signals: fromSignals.State;
  settings: fromSettings.State;
  log: fromLogger.State;
  lastQuoteData: fromLastQuotes.State;
  fundamentalData: fromFundamentalData.State;
  gp: fromGp.State;
  orderScreenStrategy: fromOrderScreenStrategy.State;
  // symbolSearch: fromSymbolSearch.State;
}


/**
 * Our state is composed of a map of action reducer functions.
 * These reducer functions are called with each dispatched action
 * and the current or initial state and return a new immutable state.
 */
export const reducers = {
  user: fromAuth.reducers,
  userStrategies: fromUserStrategies.reducers,
  positions: fromPositions.reducers,
  signals: fromSignals.reducers,
  settings: fromSettings.reducer,
  log: fromLogger.reducer,
  lastQuoteData: fromLastQuotes.reducer,
  fundamentalData: fromFundamentalData.reducer,
  gp: fromGp.reducer,
  orderScreenStrategy: fromOrderScreenStrategy.reducer
  // symbolSearch: fromSymbolSearch.reducers
};

/**
 * Every reducer module exports selector functions, however child reducers
 * have no knowledge of the overall state tree. To make them useable, we
 * need to make new selectors that wrap them.
 */

/**
 * Settings store functions
 */
export const getSettingsState = (state: State) => state.settings;
export const getSelectedLanguage = createSelector(getSettingsState, fromSettings.getSelectedLanguage);
export const getSelectedCulture = createSelector(getSettingsState, fromSettings.getSelectedCulture);
export const getAvailableLanguages = createSelector(getSettingsState, fromSettings.getAvailableLanguages);

/**
 * Auth store functions
 */
export const getAuthState = (state: State) => state.user;
// export const getAuthLoaded = createSelector(getAuthState, fromAuth.getLoaded);
// export const getAuthLoading = createSelector(getAuthState, fromAuth.getLoading);
// export const getAuthFailed = createSelector(getAuthState, fromAuth.getFailed);
export const getLoggedUser = createSelector(getAuthState, fromAuth.getUser);
export const getApiKey = createSelector(getAuthState, fromAuth.getApiKey);


/**
 * userStrategies store functions
 */
export const getStrategiesState = (state: State) => state.userStrategies.userStrategies;
export const getStrategiesEntitiesState = createSelector(getStrategiesState, fromUserStrategies.getStrategiesEntitiesState);
export const getSelectedStrategyId = createSelector(getStrategiesState, fromUserStrategies.getSelectedStrategyId);
export const getSelectedStrategy = createSelector(getStrategiesState, fromUserStrategies.getSelectedStrategy);
export const getStrategiesLoading = createSelector(getStrategiesState, fromUserStrategies.getCollectionLoading);
export const getStrategiesLoaded = createSelector(getStrategiesState, fromUserStrategies.getCollectionLoaded);

/**
 * Positions store functions
 */
export const getPositionsState = (state: State) => state.positions;
export const getAllPositions = createSelector(getPositionsState, fromPositions.getAllPositions);
export const getPositionsLoaded = createSelector(getPositionsState, fromPositions.getCollectionLoaded);
export const getPositionsLoading = createSelector(getPositionsState, fromPositions.getCollectionLoading);
export const getSelectedPosition = createSelector(getPositionsState, fromPositions.getSelectedPosition);


/**
 * Signals store functions
 */
export const getSignalsState = (state: State) => state.signals;
export const getSignalsLoaded = createSelector(getSignalsState, fromSignals.getCollectionLoaded);
export const getSignalsLoading = createSelector(getSignalsState, fromSignals.getCollectionLoading);
export const getSelectedSignal = createSelector(getSignalsState, fromSignals.getSelectedBook);

/**
 * Logger store functions
 */
export const getLogState = (state: State) => state.log;
export const getLogItems = createSelector(getLogState, fromLogger.getLogItems);

/**
 * Last quotes functions
 */
export const getLastQuoteState = (state: State) => state.lastQuoteData;
export const getLastQuote = createSelector(getLastQuoteState, fromLastQuotes.getLastQuotesMap);

/**
 * Last quotes functions
 */
export const getFundamentalDataState = (state: State) => state.fundamentalData;
export const getFundamentalData = createSelector(getFundamentalDataState, fromFundamentalData.getFundamentalDataMap);

/**
 * General purposes functions
 */
export const getGpState = (state: State) => state.gp;
export const getSearchSymbolPartialQuery = createSelector(getGpState, fromGp.getSearchSymbolPartialQuery);
export const getSelectedOption = createSelector(getGpState, fromGp.getSelectedOption);

/**
 * Order Entry Strategy 
 */
export const getOrderScreenStrategyState = (state: State) => state.orderScreenStrategy;
export const getOrderScreenStrategy = createSelector(getOrderScreenStrategyState, fromOrderScreenStrategy.getOrderScreenStrategy);

/**
 * Symbol search functions
 */
//export const getSymbolSearchState = (state: State) => state.symbolSearch;
//export const getSymbolSearchIds = createSelector(getSymbolSearchState, fromSymbolSearch.getIds);
//export const getSymbolSearchTextQuery = createSelector(getSymbolSearchState, fromSymbolSearch.getTextQuery);
//export const getSymbolSearchQuery = createSelector(getSymbolSearchState, fromSymbolSearch.getQuery);
//export const getSymbolSearchLoading = createSelector(getSymbolSearchState, fromSymbolSearch.getLoading);
//export const getSymbolSearchError = createSelector(getSymbolSearchState, fromSymbolSearch.getError);

