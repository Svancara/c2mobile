import { Routes } from '@angular/router';
// import { AuthGuard } from './auth/services/auth-guard.service';
import { NotFoundPageComponent } from './core/containers/not-found-page';
import { HomeRoute, UserStrategyRoute } from './globals';
import { AuthGuard } from '@app/core/services/auth-guard.service';

export const routes: Routes = [
  { path: '', redirectTo: '/' + HomeRoute, pathMatch: 'full' },
//  { path: 'home', loadChildren: 'app/home-page/home-page.module#HomePageModule' },
  { path: HomeRoute, loadChildren: 'app/home-page/home-page.module#HomePageModule' },
  
  { path: 'positions', loadChildren: 'app/positions/positions.module#PositionsModule', canActivate: [AuthGuard] },
//  { path: 'watchlist', loadChildren: 'app/watchlist/watchlist.module#WatchlistModule' },
  { path: 'watchlist', component: NotFoundPageComponent, canActivate: [AuthGuard]},
  { path: 'user-strategies-list', loadChildren: 'app/user-strategies-list/user-strategies-list.module#UserStrategiesListModule', canActivate: [AuthGuard]},
  { path: UserStrategyRoute, loadChildren: 'app/user-strategy/user-strategy.module#UserStrategyModule', canActivate: [AuthGuard]},
  { path: 'signals', loadChildren: 'app/signals/signals.module#SignalsModule', canActivate: [AuthGuard]},
  { path: 'quotes', loadChildren: 'app/quotes/quotes.module#QuotesModule', canActivate: [AuthGuard]},
  //{ path: 'trades', component: NotFoundPageComponent },
  { path: 'closedtrades', component: NotFoundPageComponent, canActivate: [AuthGuard]},
  
  { path: 'orderentryscreen', loadChildren: 'app/order-screen/order-screen.module#OrderScreenModule', canActivate: [AuthGuard]},
  {path: 'c2account', component: NotFoundPageComponent, canActivate: [AuthGuard] },
  { path: 'disclaimer', component: NotFoundPageComponent },
  { path: 'login', loadChildren: 'app/auth/auth.module#AuthModule' },
  { path: 'searchsymbol', loadChildren: 'app/symbol-search/symbol-search.module#SymbolSearchModule', canActivate: [AuthGuard] },
  { path: '**', component: NotFoundPageComponent },
 
];
