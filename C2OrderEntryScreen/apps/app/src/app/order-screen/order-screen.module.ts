import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { PrimeNgModule } from '../primeng.module';
import { ComponentsModule } from './components/components.module';
import { reducer } from '@app/store/reducers/order-screen.reducer';

import { OrderScreenEffects } from '@app/store/effects/order-screen.effects';
import { OrderScreenContainerComponent } from './containers/order-screen-container/order-screen-container.component';
import { OrderScreenSandbox } from '@libs/order-screen/order-screen.sandbox';

import { QuotesFeedsModule } from '@app/quotes-feeds/quotes-feeds.module';
import { SymbolSearchModule } from '@app/symbol-search/symbol-search.module';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    PrimeNgModule,
    QuotesFeedsModule,
    SymbolSearchModule,
    RouterModule.forChild([
      { path: 'orderentryscreen', component: OrderScreenContainerComponent},
      {
        path: ':id',
        component: OrderScreenContainerComponent,
      },
      { path: '', component: OrderScreenContainerComponent},

    ]),

    /**
     * StoreModule.forFeature is used for composing state
     * from feature modules. These modules can be loaded
     * eagerly or lazily and will be dynamically added to
     * the existing state.
     */
    StoreModule.forFeature('order-screen', reducer),

    /**
     * Effects.forFeature is used to register effects
     * from feature modules. Effects can be loaded
     * eagerly or lazily and will be started immediately.
     *
     * All Effects will only be instantiated once regardless of
     * whether they are registered once or multiple times.
     */
    EffectsModule.forFeature([
      // PositionsEffects,
      OrderScreenEffects
    ]),


  ],
  declarations: [
    //OrderScreenRouteComponent,
    OrderScreenContainerComponent,
  ],
  providers: [OrderScreenSandbox]
})
export class OrderScreenModule{ }
