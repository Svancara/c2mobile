import { Component} from '@angular/core';
import { ISendSignal , InternaSearchSymbolRequest, GetLastQuote } from '@app/models';
import { OrderScreenSandbox } from '@libs/order-screen/order-screen.sandbox';

@Component({
  selector: 'app-order-screen-container',
  templateUrl: './order-screen-container.component.html',
  styleUrls: ['./order-screen-container.component.css']
})
export class OrderScreenContainerComponent {


  constructor(public sandbox$: OrderScreenSandbox) { }

  symbolSearch(query: InternaSearchSymbolRequest) {
    this.sandbox$.symbolSearch(query,true);
  }

  symbolSelected(request: GetLastQuote) {
    this.sandbox$.getLastQuote(request);
  }

  sendSignalEvent(signal: ISendSignal) {
    // Do something with the notification (evt) sent by the child!
    // this.store.dispatch(new screenActions.Send(signal));
    this.sandbox$.sendSignal(signal);
  }

}
