import 'hammerjs';
import { async, ComponentFixtureAutoDetect, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';

import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { RouterModule } from '@angular/router';

//import { PrimeNgModule } from '@app/primeng.module';
import { SharedModule } from '@app/shared/shared.module';

import { OrderScreenHeaderComponent } from '@app/order-screen/components/header/header.component';
import { AppheaderContainer } from '@app/core/containers/appheader/appheader.container';
import { AppheaderComponent } from '@app/core/components/appheader/appheader.component';
import { InstrumentSelectionComponent } from '@app/shared/components/instrument-selection/instrument-selection.component';

import { OrderScreenComponent } from './order-screen.component';
import { C2Action, ISendSignal } from '@app/models';

import { OrderType, ValidationResult } from '@libs/order-screen/model';



describe('OrderScreenComponent ', () => {

  let component: OrderScreenComponent;
  let fixture: ComponentFixture<OrderScreenComponent>;
  let de: DebugElement;
  let el: HTMLElement;

  // async beforeEach
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppheaderComponent,
        AppheaderContainer,
        OrderScreenComponent,
        OrderScreenHeaderComponent], 
      imports: [
        NoopAnimationsModule,
        SharedModule,
   //     StoreModule.forRoot([reducers]),
        RouterModule.forRoot([]),
        ],
      providers: [
        { provide: ComponentFixtureAutoDetect, useValue: true },
        { provide: APP_BASE_HREF, useValue: '/' }
      ]
    })
      .compileComponents();  // compile template and css
  }));

  // synchronous beforeEach
  beforeEach(() => {
    fixture = TestBed.createComponent(OrderScreenComponent);

    component = fixture.componentInstance; // test instance

  });

  describe('Entry screen', () => {

    it('expects a valid Market BTO signal', () => {

      let validationResult: ValidationResult;

      // Template variables
      component.strategyId = 1;
      
      component.strategyName = "My strategyName";
      component.symbol = "MSFT";

      validationResult = component.createSignal(C2Action.BTO);

      expect(validationResult.valid).toBeFalsy("this signal should not be valid");
      expect(validationResult.message).toEqual({ severity: 'error', summary: 'Quantity is empty', detail: '' });

      component.quantity = 100;
      validationResult = component.createSignal(C2Action.BTO);

      console.log(validationResult.message);
      
      expect(validationResult.valid).toBeTruthy("this signal shoud be valid");
      expect(validationResult.message).toBeUndefined();

      const signal: ISendSignal = validationResult.validSignal;

      expect(signal.strategyId).toEqual(1);
      expect(signal.action).toEqual(C2Action.BTO);
      expect(signal.quant).toEqual(100);
      expect(signal.symbol).toEqual("MSFT");
      expect(signal.typeofsymbol).toEqual("stock");
      expect(signal.limit).toBeUndefined();
      expect(signal.stop).toBeUndefined();
      expect(signal.duration).toEqual("GTC"); // Market orders are always GTC
      expect(signal.market).toEqual("1");
      expect(signal.stoploss).toBeUndefined();
      expect(signal.profittarget).toBeUndefined();
      expect(signal._parkUntilSecs_).toBeUndefined();
      expect(signal.parkUntilYYYYMMDDHHMM).toBeUndefined();
      
    });
    
    it('expects a valid Limit BTO signal', () => {

      let validationResult: ValidationResult;

      // Template variables
      component.strategyId = 1;
      component.strategyName = "My strategyName";
      component.symbol = "MSFT";
      component.quantity = 100;
      component.selectedOrderType = OrderType.Limit;

      validationResult = component.createSignal(C2Action.BTO);

//      console.log("...............", validationResult.message);

      expect(validationResult.valid).toBeFalsy("this signal should not be valid");
      expect(validationResult.message).toEqual({ severity: 'error', summary: 'A limit price is not provided for the limit order', detail: 'Limit price must be a positive number' });
             

      component.limitPrice = 12.34;
      validationResult = component.createSignal(C2Action.BTO);


      expect(validationResult.valid).toBeTruthy("this signal shoud be valid");
      expect(validationResult.message).toBeUndefined();

      const signal: ISendSignal = validationResult.validSignal;

      expect(signal.strategyId).toEqual(1);
      expect(signal.action).toEqual(C2Action.BTO);
      expect(signal.quant).toEqual(100);
      expect(signal.symbol).toEqual("MSFT");
      expect(signal.typeofsymbol).toEqual("stock");
      expect(signal.limit).toEqual(12.34);
      expect(signal.stop).toBeUndefined();
      expect(signal.duration).toEqual("DAY"); // A default value is DAY
      expect(signal.market).toEqual("0");
      expect(signal.stoploss).toBeUndefined();
      expect(signal.profittarget).toBeUndefined();
      expect(signal._parkUntilSecs_).toBeUndefined();
      expect(signal.parkUntilYYYYMMDDHHMM).toBeUndefined();
    });
    
    it('expects a valid Stop BTO signal with stop price and profit target', () => {

    /*
           /----- Profit target
          /--- Stop 
    Buy -<-------------------------------
          \--- Limit
           \---- Stop loss
    */

      let validationResult: ValidationResult;
      
      // Template variables
      component.strategyId = 1;
      component.strategyName = "My strategyName";
      component.symbol = "MSFT";
      component.quantity = 100;
      component.selectedOrderType = OrderType.Stop;
      component.stopPrice = 23.45;
      component.stopLoss = 20;
      component.profitTarget = 20;
      component.limitPrice = 12.34;

      validationResult = component.createSignal(C2Action.BTO);

      expect(validationResult.valid).toBeFalsy("this signal should not be valid");
      expect(validationResult.message).toEqual({ severity: 'warn', summary: 'Wrong profit target price for the stop order', detail: 'The profit target price 20 is lesser than the stop price 23.45' });

      component.profitTarget = 200.1;

      validationResult = component.createSignal(C2Action.BTO);

      //expect(validationResult.valid).toBeFalsy("this signal should be suspicios");
      //expect(validationResult.message).toEqual({ severity: 'warn', summary: 'Wrong profit target price for the stop order', detail: 'The profit target price 20 is lesser than the stop price 23.45' });
      //console.log("...............", validationResult.message);

      expect(validationResult.valid).toBeTruthy("this signal shoud be valid");
      expect(validationResult.message).toBeUndefined();

      const signal: ISendSignal = validationResult.validSignal;

      expect(signal.strategyId).toEqual(1);
      expect(signal.action).toEqual(C2Action.BTO);
      expect(signal.quant).toEqual(100);
      expect(signal.symbol).toEqual("MSFT");
      expect(signal.typeofsymbol).toEqual("stock");
      expect(signal.limit).toBeUndefined();
      expect(signal.stop).toEqual(23.45);
      expect(signal.duration).toEqual("DAY"); // A default value is DAY
      expect(signal.market).toEqual("0");
      expect(signal.stoploss).toEqual(20.0);
      expect(signal.profittarget).toEqual(200.1);
      expect(signal._parkUntilSecs_).toBeUndefined();
      expect(signal.parkUntilYYYYMMDDHHMM).toBeUndefined();
      
    });

  });
});

