import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Instrument, OrderScreenSymbol, OrderScreen, TIF, InstrumentString, ISendSignal, C2Action, InternaSearchSymbolRequest, GetLastQuote, LastQuoteData, FundamentalDataResponse } from '@app/models';
import { SelectItem } from 'primeng/components/common/selectitem';
//import { Observable } from 'rxjs/Observable';
import { Message } from 'primeng/components/common/message';
import { MessageService } from 'primeng/components/common/messageservice';
import { SendSignal } from '@app/models/sendSignal';
//import { OrderScreenSandbox } from '@app/order-screen/order-screen.sandbox';
import { SymbolSearchTypeAheadSupport } from '@app/models/LastQuoteService.dtos';
import { Utils } from '@app/utils';
import { SignalFactory} from '@libs/order-screen/utils/signal-factory';
import { OrderType, ValidationResult } from '@libs/order-screen/model';
import { Router } from '@angular/router';

enum ScreenType { SimpleStock, AdvancedStock }

@Component({
  selector: 'app-order-screen',
  templateUrl: './order-screen.component.html',
  styleUrls: ['./order-screen.component.css'],
//  changeDetection: ChangeDetectionStrategy.OnPush, instruments selection does not work with this
})
export class OrderScreenComponent // implements OnInit, OnDestroy
{

  private _lastQuote: LastQuoteData;

  @Input()
  set lastQuote(value: LastQuoteData) {
    if (value) {
      this._lastQuote = value;
      this.symbol = value.symbol;
      this.selectedInstrumentReceiver(value.typeofsymbol as InstrumentString);
    }
  }
  get lastQuote(): LastQuoteData {
    return this._lastQuote
  }

  @Input() fundamentalData: FundamentalDataResponse;

  @Input() strategyId: number;
  @Input() strategyName: string;
  @Output() notifyParent: EventEmitter<ISendSignal> = new EventEmitter();
  @Output() symbolSearchEmmiter: EventEmitter<InternaSearchSymbolRequest> = new EventEmitter<InternaSearchSymbolRequest>();
  @Output() symbolSelectedEmmiter: EventEmitter<GetLastQuote> = new EventEmitter<GetLastQuote>();

  public lock: boolean = true;

  public symbol: string = "";
  public market: string = "US";

  public screenType: ScreenType = ScreenType.SimpleStock;

  public orderType: SelectItem[] = [
    { label: 'Market', value: OrderType.Market },
    { label: 'Limit', value: OrderType.Limit },
    { label: 'Stop', value: OrderType.Stop },
  ];


  public limitPriceDisabled = true;
  public stopPriceDisabled = true;

  public limitPrice: number;
  public parkuntil: Date;
  public parkuntilChecked: boolean;
  public profitTarget: number;
  public quantity: number;
  public stopLoss: number;
  public stopPrice: number;
  public tif: boolean = true;

  public msgs: Message[] = [];
  public showMessages = false;
  
  // --------------- Private -----------------
  
  private _selectedOrderType: OrderType = OrderType.Market;

  public get selectedOrderType(): OrderType {
    return this._selectedOrderType;
  }
  public set selectedOrderType(value: OrderType) {
    this._selectedOrderType = value;
    switch (value) {
      case OrderType.Market:
        this.limitPriceDisabled = true;
        this.stopPriceDisabled = true;
        break;
      case OrderType.Limit:
        this.limitPriceDisabled = false;
        this.stopPriceDisabled = true;
        break;
      case OrderType.Stop:
        this.limitPriceDisabled = true;
        this.stopPriceDisabled = false;
        break;
      default:
        this.limitPriceDisabled = true;
        this.stopPriceDisabled = true;
    }
  }

  constructor(private router: Router) { }

  public send(action: C2Action) {

    this.msgs = [];
    this.showMessages = false;

    let validationResult = this.createSignal(action)

    if (validationResult.valid) {
      this.notifyParent.emit(validationResult.validSignal);
    } else {
      this.msgs.push(validationResult.message);
    }
    this.showMessages = true;
    this.lock = true;
  }

  /**
   * Creates a signal form order screen variables, checks it and returns it if valid.
   * If not valid,  eturns a message.
   * @param action
   */
  createSignal(action: C2Action): ValidationResult {
    let signalFactory: SignalFactory = new SignalFactory();

    signalFactory._parkUntilSecs_ = this.parkuntil;
    signalFactory.action = action;
    signalFactory.limitPrice = this.limitPrice;
    signalFactory.orderType = this.selectedOrderType;
    signalFactory.parkuntil = this.parkuntil;
    signalFactory.parkuntilChecked = this.parkuntilChecked;
    signalFactory.profitTarget = this.profitTarget;
    signalFactory.quant = this.quantity;
    signalFactory.stopPrice = this.stopPrice;
    signalFactory.stopLoss = this.stopLoss;
    signalFactory.strategyId = this.strategyId;
    signalFactory.symbol = this.symbol;
    signalFactory.tif = this.tif;
    signalFactory.typeofsymbol = this.selectedInstrument;

    return signalFactory.validate();
  }


  buy() { this.send(C2Action.BTO); }
  sell() { this.send(C2Action.STC); }
  short() { this.send(C2Action.STO); }
  cover() { this.send(C2Action.BTC); }
  

  //private getInstrumentType(): InstrumentString {
  //  switch (this.selectedInstrument) {
  //    case 0: return InstrumentString.stock;
  //    case 1: return InstrumentString.option;
  //    case 2: return InstrumentString.future;
  //    case 3: return InstrumentString.forex;
  //    case 4: return InstrumentString.mutual;
  //  }
  //}

  public selectedInstrument: InstrumentString = InstrumentString.stock;
  selectedInstrumentReceiver(instrument: InstrumentString) {
    this.selectedInstrument = instrument;
  }

  
  searchsymbol() {
    const request: InternaSearchSymbolRequest = {
      query: this.symbol,
      returnToRoute: ["/orderentryscreen", this.strategyId],
      // instrument: this.getInstrumentType()
      instrument: this.selectedInstrument
      
    };
    this.symbolSearchEmmiter.emit(request);
  }

  settings() {
    alert("OrderScreenComponent says: Order Entry settings clicked");
  }


  createRequest(): GetLastQuote {
    let result: GetLastQuote = new GetLastQuote();
    result.market = this.market;
    result.typeofsymbol = Utils.prepInstrument(this.selectedInstrument);
    result.symbol = Utils.prepSymbol(this.symbol); // this.searchedSymbol$.Symbol;
    return result;
  }

  sendRequest() {
    if (this.symbol.trim()) {
      this.symbolSelectedEmmiter.emit(this.createRequest());
    }
  }

  swipeRight() {
    this.router.navigate(['/signals', this.strategyId]);
  }
  swipeLeft() {
    this.router.navigate(['/positions', this.strategyId]);
  }

  
  bidClicked() {
  //  this.limitPrice = this._lastQuote.bid;
  }
  lastTradeClicked(value) {
    // alert("LastBid" + value);
  }
  askClicked(value) {
   // this.stopPrice = this._lastQuote.ask;
  }


  public smallLastQuotePanel = true;
  public collapsed = true;
  toggleLastQuotePanel() {
    this.collapsed = !this.collapsed;
    this.smallLastQuotePanel = !this.smallLastQuotePanel;
  }
  
}

/*
   
    //  Entered = case (Signal#signal.order) #order.ts_entered_line of
    //  [] -> [];
    //_ -> io_lib:format("&entered=~s", [edoc_lib:escape_uri((Signal#signal.order) #order.ts_entered_line)])
    //end,
    //StampParam = case Stamp of
    //[] -> []; % Stamp is mandatory, but it makes problems in unit tests.So I can disable it here.
    //  _ -> io_lib:format("&stamp=~s", [Stamp])
    //end,


Delay = case (Signal#signal.delay > 0) of
true -> io_lib:format("&delay=~w", [Signal#signal.delay]);
false -> []
end,

  Cond = case (Signal#signal.conditionalupon = /= 0) of
true -> io_lib:format("&conditionalupon=~w", [Signal#signal.conditionalupon]);
false -> []
end,

  OCA_ID = case (Signal#signal.ocaId = /= 0) of
true -> io_lib:format("&ocaid=~w", [Signal#signal.ocaId]);
false -> []
end,
    
    case Signal#signal.xreplace of
true -> XReplace = io_lib: format("&xreplace=~B", [Signal#signal.signalId]),
  Signal_ID = [];
false -> XReplace = [],
  Signal_ID = case (Signal#signal.signalId = /= 0) of
true -> io_lib:format("&signalid=~w", [Signal#signal.signalId]);
false -> []
end
end,


  Commentary = case (Signal#signal.commentary = /= "") of
true -> io_lib:format("&commentary=~s", [edoc_lib:escape_uri(Signal#signal.commentary)]);
false -> []
end,

  UserSignalName = case ((Signal#signal.order)#order.user_signal_id = /= "") of
true -> io_lib:format("&signame=~s", [edoc_lib:escape_uri((Signal#signal.order) #order.user_signal_id)]);
false -> []
end,

  KillSameDirection = case (Signal#signal.killsamedirection =:= 1) of
true -> "&killsamedirection=1";
false -> []
end,

  TradeStationFlag = case ((Signal#signal.order)#order.softwareId) of
1 -> ?TS_SIGNAL_MARKER;  % "&ts=1";
2 -> ?MT4_SIGNAL_MARKER; % "&ts=0&sourcecode=MT";
_ -> []
end,

  SoftwareVersion = case ((Signal#signal.order)#order.softwareVersion) of
  [] -> [];
Version -> ?SOFTWARE_VERSION++ Version
end,

  SubmitTime = case ((Signal#signal.order)#order.submitTime = /= "") of
true -> io_lib:format("&submittime=~s", [(Signal#signal.order)#order.submitTime]);
false -> []
end,

  CancelsAtRelative = case (Signal#signal.order)#order.cancelsatrelative > 0 of
true -> io_lib:format("&cancelsatrelative=~B", [(Signal#signal.order)#order.cancelsatrelative]);
false -> []
end,

  FillsOnly = case ((Signal#signal.order)#order.fillsOnly =:= 1) of
true -> "&fillsonly=1";
false -> []
end,
    
    %% Update 2012- 08 - 12: for filled matrix orders "at market" add "tradeprice" param
  %% Update 2017- 02 - 17: for all filled matrix orders "at market" add "tradeprice" param.Well - it does not work on the C2 side.But I have a clear conscience.
    TradePrice = case ((Signal#signal.order)#order.from_matrix =:= 1) and(Signal#signal.filled =:= 1) of % and((Signal#signal.order) #order.price_type =:= ?Price_market)
true -> io_lib:format("&tradeprice=~f", [(Signal#signal.order)#order.filledPrice]);
false -> []
end,

  ForceNoOca = case Signal#signal.forcenooca of
1 -> "&forcenooca=1";
_ -> []
end,

  C2Pro = case ((Signal#signal.order)#order.c2pro) of
1 -> "&c2pro=1";
_ -> []
end,

  lists:flatten(Mandatory++ Instrument ++ Limit ++ Stop ++ Delay ++ Cond ++ OCA_ID ++ Signal_ID ++ ParkUntil ++ Commentary 
                 ++ UserSignalName ++ KillSameDirection ++ TradeStationFlag ++ SubmitTime ++ CancelsAtRelative 
                 ++ FillsOnly ++ TradePrice ++ SoftwareVersion ++ StopLossPrice ++ ProfitTargetPrice ++ XReplace ++ ForceNoOca
                 ++ MatrixParam ++ C2Pro).
*/
