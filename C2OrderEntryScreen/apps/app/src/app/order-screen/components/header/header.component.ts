import { Component, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-order-entry-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderScreenHeaderComponent {

  @Input() strategyId: number;
  @Input() strategyName: string = "";
  @Output() settingsEvent = new EventEmitter();

  settings() {
    this.settingsEvent.emit();
  }


}
