import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrimeNgModule } from '@app/primeng.module';

import { SharedModule } from '@app/shared/shared.module';
import { CoreModule } from '@app/core';

import { OrderScreenComponent } from './order-screen/order-screen.component';
import { OrderScreenHeaderComponent } from './header/header.component';
         
@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    PrimeNgModule,
    SharedModule,
  ],
  declarations: [OrderScreenComponent, OrderScreenHeaderComponent],
  exports: [OrderScreenComponent]
})
export class ComponentsModule { }
