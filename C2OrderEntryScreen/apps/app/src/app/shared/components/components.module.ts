import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InstrumentSelectionComponent } from './instrument-selection/instrument-selection.component';
import { PrimeNgModule } from '@app/primeng.module';

@NgModule({
  imports: [
    CommonModule,
    PrimeNgModule
  ],
  declarations: [InstrumentSelectionComponent],
  exports: [InstrumentSelectionComponent]
})
export class SharedComponentsModule { }
