import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { InstrumentString } from '@app/models';

interface instrumentOption { label: string, value: InstrumentString }

@Component({
  selector: 'app-instrument-selection',
  templateUrl: './instrument-selection.component.html',
  styleUrls: ['./instrument-selection.component.css'],
  // changeDetection: ChangeDetectionStrategy.OnPush, this does not work. Instrument selection buttons are not initialy set 
})
export class InstrumentSelectionComponent {


  private _currentInstrument: InstrumentString = InstrumentString.stock;
  @Input()
  set currentInstrument(value: InstrumentString) {
    if (value) {
      this._currentInstrument = value;
    }
  }
  get currentInstrument(): InstrumentString {
    return this._currentInstrument;
  }

  @Input() fullNames = false;

  @Output() selectedInstrumentEmitter: EventEmitter<InstrumentString> = new EventEmitter<InstrumentString>();

  public instrumentsShort: instrumentOption[] = [
    { label: 'Stck', value: InstrumentString.stock },
    { label: 'Fut', value: InstrumentString.future },
    { label: 'Frx', value: InstrumentString.forex },
    { label: 'Opt', value: InstrumentString.option },
    { label: 'Mut', value: InstrumentString.mutual },
  ];

  public instrumentsLong: instrumentOption[] = [
    { label: 'Stock', value: InstrumentString.stock },
    { label: 'Future', value: InstrumentString.future },
    { label: 'Forex', value: InstrumentString.forex },
    { label: 'Option', value: InstrumentString.option },
    { label: 'Mutual', value: InstrumentString.mutual },
  ];


  instrumentClick(instrument) {
    this.selectedInstrumentEmitter.emit(instrument.option.value);
  }
}
