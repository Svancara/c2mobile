import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import * as fromStore from '@app/store';
import * as fromUserStore from '@app/store/reducers/auth';
import * as authActions from '@app/store/actions/auth.action';
import { LoginOK, CURRENT_USER, ILoginOK, Strategy } from '@app/models';
import { localeDateString } from '../utility';
import { LocalStorageService } from 'angular-2-local-storage';


import * as fromOrderScreenStrategy from '@app/store/actions/order-screen-strategy.actions';
import { environment } from '@env/environment';

export abstract class Sandbox {

  public loggedUser$: Observable<LoginOK>;// = this.appState$.select(store.getLoggedUser);
  public culture$: Observable<string>;// = this.appState$.select(store.getSelectedCulture);
  public culture: string;

  constructor(protected store$: Store<any>, protected localStorage?: LocalStorageService) {
    this.loggedUser$ = this.store$.select(fromUserStore.getUser);
    this.culture$ = this.store$.select(fromStore.getSelectedCulture);
  }

  /**
   * Pulls user from local storage and saves it to the store
   */
  public loadUser(): void {
    //let user = JSON.parse(localStorage.getItem(CURRENT_USER));
    let user: ILoginOK;
    if (!environment.production) {
      // ======================== For development ==============================
      user = { personid: environment.C2OrderEntryScreenGlobals.c2UserId, APIkey: environment.C2OrderEntryScreenGlobals.c2Apikey, site_id: 0, site_name: "" };

      let devStrategy: Strategy = new Strategy();
      devStrategy.system_id = environment.C2OrderEntryScreenGlobals.testingStrategyId;
      devStrategy.systemName = "[==TEST==]";
      this.store$.dispatch(new fromOrderScreenStrategy.AddOrderScreenStrategy(new Strategy(devStrategy)));

    } else {
      user = this.localStorage.get(CURRENT_USER);
    }
    this.store$.dispatch(new authActions.AddUserAction(new LoginOK(user)));
  }

  /**
   * Formats date string based on selected culture
   * 
   * @param value
   */
  public formatDate(value: string) {
    return localeDateString(value, this.culture);
  }
}
