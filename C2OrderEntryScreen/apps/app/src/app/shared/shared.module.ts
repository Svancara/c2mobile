import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrimeNgModule } from '@app/primeng.module';

import { TruncatePipe } from './pipes/truncate.pipe';
import { ElipsisTruncatePipe } from './pipes/elipis-truncate.pipe';

import { SharedComponentsModule } from './components/components.module';
import { SharedContainersModule } from './containers/containers.module';


const COMPONENTS = [
  ElipsisTruncatePipe,
  TruncatePipe,
];

@NgModule({
  imports: [
    CommonModule,
    PrimeNgModule,
    SharedComponentsModule,
    SharedContainersModule,
  ],
  declarations: [
    ElipsisTruncatePipe,
    TruncatePipe,
  ],
  exports: [
    ElipsisTruncatePipe,
    PrimeNgModule,
    SharedComponentsModule,
    SharedContainersModule,
    TruncatePipe,
  ],
  providers: []
})
export class SharedModule { }
