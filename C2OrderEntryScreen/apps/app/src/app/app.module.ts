import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/exhaustMap';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/take';
import 'rxjs/add/observable/throw';
import { empty } from 'rxjs/observable/empty';
import { of } from 'rxjs/observable/of';


import { NgModule, APP_INITIALIZER } from '@angular/core';
import {
  HttpModule,
  RequestOptions,
  XHRBackend,
  Http
} from '@angular/http';

//import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { StoreRouterConnectingModule, RouterStateSerializer, } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { CoreModule } from '@app/core';
import { AuthModule } from '@app/auth/auth.module';

import { routes } from './routes';
// import { reducers, metaReducers } from './reducers';
import * as State from '@app/store';
import { CustomRouterStateSerializer } from '@app/shared/ngrx-utils';

import { environment } from '../environments/environment';
import { AppComponent } from './app.component';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateService } from '@ngx-translate/core';

import { SimpleNotificationsModule } from 'angular2-notifications';
import { RootAuthModule } from '@app/auth/auth.module';

import { UtilityModule } from '@app/utility';
import { ConfigService } from './app-config.service';
import { HttpServiceModule } from '@app/shared/http';
import { LoggerModule } from '@app/logger/logger.module';


// Guards
import { AuthGuard } from '@app/core/services/auth-guard.service';

// AoT requires an exported function for factories
/**
 * Calling functions or calling new is not supported in metadata when using AoT.
 * The work-around is to introduce an exported function.
 *
 * The reason for this limitation is that the AoT compiler needs to generate the code that calls the factory
 * and there is no way to import a lambda from a module, you can only import an exported symbol.
 */
export function configServiceFactory(config: ConfigService) {
  return () => config.load()
}

//export function HttpLoaderFactory(http: HttpClient) {
//  return new TranslateHttpLoader(http);
//}
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}



@NgModule({
  declarations: [AppComponent],
  imports: [
    //    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule, ReactiveFormsModule,

    HttpClientModule,
    HttpModule,
    LoggerModule,
    // App custom dependencies
    HttpServiceModule.forRoot(),
    UtilityModule.forRoot(),
    RootAuthModule,
    RouterModule.forRoot(routes, {
      // useHash: true
      // enableTracing: true
    }),

    //MaterialModule,
    //C2MobileModule
    //C2DesktopModule,
    //UserStrategiesModule,

    /**
   * StoreModule.forRoot is imported once in the root module, accepting a reducer
   * function or object map of reducer functions. If passed an object of
   * reducers, combineReducers will be run creating your application
   * meta-reducer. This returns all providers for an @ngrx/store
   * based application.
   */
    //StoreModule.forRoot(reducers, { metaReducers }),
    //StoreModule.provideStore(State),
    StoreModule.forRoot(State.reducers),


    /**
     * @ngrx/router-store keeps router state up-to-date in the store.
     */
    StoreRouterConnectingModule,

    /**
     * Store devtools instrument the store retaining past versions of state
     * and recalculating new states. This enables powerful time-travel
     * debugging.
     *
     * To use the debugger, install the Redux Devtools extension for either
     * Chrome or Firefox
     *
     * See: https://github.com/zalmoxisus/redux-devtools-extension
     */
    !environment.production ? StoreDevtoolsModule.instrument({ maxAge: 25 }) : [],

    /**
     * EffectsModule.forRoot() is imported once in the root module and
     * sets up the effects class to be initialized immediately when the
     * application starts.
     *
     * See: https://github.com/ngrx/platform/blob/master/docs/effects/api.md#forroot
     */
    EffectsModule.forRoot([]),

    /**
     * `provideDB` sets up @ngrx/db with the provided schema and makes the Database
     * service available.
     */
    // DBModule.provideDB(schema),

    CoreModule.forRoot(),

    AuthModule.forRoot(),

    //TranslateModule.forRoot({
    //  loader: {
    //    provide: TranslateLoader,
    //    useFactory: HttpLoaderFactory,
    //    deps: [HttpClient]
    //  }
    //}),

    //TranslateModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),


    SimpleNotificationsModule.forRoot(),
  ],
  // declarations: [ AppComponent,],
  providers: [
    AuthGuard,
    /**
     * The `RouterStateSnapshot` provided by the `Router` is a large complex structure.
     * A custom RouterStateSerializer is used to parse the `RouterStateSnapshot` provided
     * by `@ngrx/router-store` to include only the desired pieces of the snapshot.
     */
    { provide: RouterStateSerializer, useClass: CustomRouterStateSerializer },
    ConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: configServiceFactory,
      deps: [ConfigService],
      multi: true
    }
  ],
  exports: [AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
