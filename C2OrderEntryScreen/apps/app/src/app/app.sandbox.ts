import { Injectable } from '@angular/core';
import { Sandbox } from '@app/shared/sandbox/base.sandbox';
import { Store } from '@ngrx/store';
import * as store from '@app/store';
import * as settingsActions from '@app/store/actions/settings.action';
import { TranslateService } from '@ngx-translate/core';
import { ConfigService } from '@app/app-config.service';
import { LocalStorageService } from 'angular-2-local-storage';
import { CURRENT_USER, ILoginOK, Layout, ILayout } from '@app/models';
import { LoginSuccess } from '@app/store/actions/auth.action';
import { changeTheme } from '@app/utility/changeTheme';
import { LAYOUT } from '@app/globals';

@Injectable()
export class AppSandbox extends Sandbox {

  constructor(
    public store$: Store<store.State>,
    private translate: TranslateService,
    private configService: ConfigService,
    protected localStorage: LocalStorageService
  ) {
    super(store$);
  }

  /**
   * Sets up default language for the application. Uses browser default language.
   */
  public setupLanguage(): void {
    let localization: any = this.configService.get('localization');
    let languages: Array<string> = localization.languages.map(lang => lang.code);
    let browserLang: string = this.translate.getBrowserLang();

    this.translate.addLangs(languages);
    this.translate.setDefaultLang(localization.defaultLanguage);

    let selectedLang = browserLang.match(/en|hr/) ? browserLang : localization.defaultLanguage;
    let selectedCulture = localization.languages.filter(lang => lang.code === selectedLang)[0].culture;

    this.translate.use(selectedLang);
    this.store$.dispatch(new settingsActions.SetLanguageAction(selectedLang));
    this.store$.dispatch(new settingsActions.SetCultureAction(selectedCulture));

    let layout: ILayout = this.localStorage.get(LAYOUT);
    if (layout) {
      changeTheme(undefined, layout.theme, undefined);
    }

    this.loadUser();
  }

  /**

   * Returns global notification options
   */
  public getNotificationOptions(): any {
    return this.configService.get('notifications').options;
  }
}
