import { Store } from '@ngrx/store';
import { environment } from '../environments/environment';
import { Environment, C2OrderEntryScreenGlobals } from './models/types';
import { HttpErrorResponse } from '@angular/common/http';
import { ErrorAction } from './core/errormessages/errorAction';

import { State as AuthState } from '@app/store/reducers/auth/auth';
import * as authReducers from '@app/store/reducers/auth';


// declare var gC2OrderEntryScreenGlobals: C2OrderEntryScreenGlobals;

export class Utils {

  public static getApiKey(store: Store<AuthState>): string {
    let apikey: string = '';
    store.select(authReducers.getApiKey).first().subscribe(apikeyState => apikey = apikeyState);
    return apikey;
  }

  public static formatError(response: HttpErrorResponse): ErrorAction {
    return new ErrorAction({ severity: 'error', summary: response.message, detail: response.statusText })
  }

  public static prepMarket(market: string): string {
    return market ? market.toUpperCase() : "US";
  }

  public static prepInstrument(instrument: string): string {
    return instrument ? instrument.toLowerCase() : "stock";
  }

  public static prepSymbol(symbol: string): string | undefined {
    return symbol ? symbol.trim().toUpperCase() : undefined;
  }

}
