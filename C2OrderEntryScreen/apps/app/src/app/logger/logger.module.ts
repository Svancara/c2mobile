import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';

import { LoggerEffects } from '@app/store/effects/logger.effects';
import { LoggerService} from '@app/core/services/logger.service';


@NgModule({
  imports: [
    CommonModule,
    /**
     * Effects.forFeature is used to register effects
     * from feature modules. Effects can be loaded
     * eagerly or lazily and will be started immediately.
     *
     * All Effects will only be instantiated once regardless of
     * whether they are registered once or multiple times.
     */
    EffectsModule.forFeature([LoggerEffects]),

  ],
  declarations: [],
  providers: [LoggerService]
})
export class LoggerModule { }
