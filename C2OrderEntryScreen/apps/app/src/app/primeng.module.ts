import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

/*
import { AutoCompleteModule } from 'primeng/primeng';
import { ButtonModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
// import { ListboxModule } from 'primeng/primeng';
import { MenuModule } from 'primeng/primeng';
// import { SidebarModule } from 'primeng/primeng';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { ToolbarModule } from 'primeng/primeng';
*/

//import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { ButtonModule } from 'primeng/components/button/button';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { DialogModule } from 'primeng/components/dialog/dialog';
import { DropdownModule } from 'primeng/components/dropdown/dropdown';
// import { GrowlModule } from 'primeng/components/growl/growl';
import { InputMaskModule } from 'primeng/components/inputmask/inputmask';
import { InputTextModule } from 'primeng/components/inputtext/inputtext';
import { InputSwitchModule } from 'primeng/components/inputswitch/inputswitch';
import { MenuModule } from 'primeng/components/menu/menu';
import { MessageModule } from 'primeng/components/message/message';
import { MessagesModule } from 'primeng/components/messages/messages';
import { MessageService } from 'primeng/components/common/messageservice';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { SelectButtonModule} from 'primeng/components/selectbutton/selectbutton';
import { SharedModule } from 'primeng/components/common/shared';
//import { SpinnerModule } from 'primeng/components/spinner/spinner';
import { PasswordModule } from 'primeng/components/password/password';
import { PanelModule } from 'primeng/components/panel/panel';
import { TabViewModule } from 'primeng/components/tabview/tabview';
// import { TieredMenuModule } from 'primeng/primeng';
import { ToggleButtonModule } from 'primeng/components/togglebutton/togglebutton';
import { ToolbarModule } from 'primeng/components/toolbar/toolbar';

const primeNgModules = [
  FormsModule,
  //BrowserAnimationsModule,
  //AutoCompleteModule,
  ButtonModule,
  CalendarModule,
  DataTableModule,
  DialogModule,
  DropdownModule,
  // GrowlModule,
  InputMaskModule,
  InputSwitchModule,
  InputTextModule,
  //ListboxModule,
  MenuModule,
  MessageModule,
  MessagesModule,
  // SidebarModule,
  PanelModule,
  PasswordModule,
  SelectButtonModule,
  SharedModule,
  //SpinnerModule,
  TabViewModule,
  // TieredMenuModule,
  ToggleButtonModule,
  ToolbarModule,
];

@NgModule({
  imports: [ primeNgModules  ],
  exports: [primeNgModules],
  providers: [MessageService]
})
export class PrimeNgModule { }
