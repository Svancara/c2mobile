import { LocalStorageService } from 'angular-2-local-storage';
import { LAYOUT } from '@app/globals';
import { Layout, ILayout } from '@app/models';

export const changeTheme = (event: Event, theme: string, localStorage: LocalStorageService) => {

  if (!theme) {
    return;
  }

  let themeLink: HTMLLinkElement = <HTMLLinkElement>document.getElementById('theme-css');
  themeLink.href = "assets/themes/" + theme + "/theme.css";

  let htmlTag: HTMLHtmlElement = document.getElementsByTagName('html')[0];
  if (theme === 'darkness') {
    htmlTag.style.backgroundColor = "black";
    htmlTag.style.color = "white";
  } else {
    htmlTag.style.backgroundColor = "white";
    htmlTag.style.color = "black";
  }

  if (localStorage) {
    let storedLayout: ILayout = localStorage.get(LAYOUT);
    let layout: Layout = new Layout();
    layout.theme = theme;
    localStorage.set(LAYOUT, layout);
  }

}

