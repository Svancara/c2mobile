//export enum OrderType { BTO, BTC, STO, STC, SSHORT };
export enum C2Action { BTO = "BTO", STC = "STC", STO = "STO", BTC = "BTC", SSHORT = "SSHORT" }
export enum Instrument { stock, forex, future, option, mutual, index, forward, unknown };
export enum InstrumentString {
  stock = "stock",
  forex = "forex",
  future = "future",
  option = "option",
  mutual  = "mutual",
  index = "index",
  forward = "forward",
  unknown = "unknown",
};

/**
 * Server will determine an istrument
 */
export const UnknownInstrument = "unknown";

/**
 * Logged in user - localstorage key
 */
export const CURRENT_USER = "currentUser";


export type LongOrShort = "long" | "short";
export type OK = "0" | "1";
export type PutCall = "P" | "C";
export type TIF = "GTC" | "DAY";

export interface C2OrderEntryScreenGlobals {
  c2UserId: number;
  c2SessionId: string;
  c2Language: string;
  c2Apikey: string;
  testingStrategyId: number;
};

export interface Environment {
  production: boolean;
  C2ApiUrl: string;
  C2LastQuoteApiUrl: string;
  C2FundametalDataApiUrl: string;
  C2SymbolSearchApiUrl: string;
  C2OrderEntryScreenGlobals: C2OrderEntryScreenGlobals;
};


export interface C2Error {
  title: string;
  message: string;
  errors?: string[]; // getLastQuote, fundamentalData 
}


export interface InternaSearchSymbolRequest {
  query: string;
  instrument: InstrumentString; 
  returnToRoute: any[];
}

export interface ILayout {
  theme: string;
}

export class Layout implements ILayout{
  public theme: string;
}

/*
{
    "ok": "1",
    "success": {
        "site_id": 1,
        "site_name": "Collective2",
        "personid": "100",
        "APIkey": "thisiskeyhere"
    }
}

{
    "ok": "0",
    "error": {
        "errors": "Either email or password are incorrect",
        "title": "Unable to requestAPIkey",
        "message": "There were problems with your request."
    }
}
*/

export interface ILoginOK {
  site_id: number;
  site_name: string;
  personid: number;
  APIkey: string;
}

export interface ILoginError {
  errors: string; 
  title: string;
  message: string;
}

export interface ILoginResponse {
  ok: OK;
  success?: ILoginOK;
  error?: ILoginError;
}


export class LoginOK implements ILoginOK {
  site_id: number;
  site_name: string;
  personid: number;
  APIkey: string;

  constructor(userData: ILoginOK) {
    this.APIkey = userData ? userData.APIkey : undefined;
    this.personid = userData ? userData.personid : undefined;
    this.site_id = userData ? userData.site_id : undefined;
    this.site_name = userData ? userData.site_name : undefined;
  }
}
