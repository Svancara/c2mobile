// Authentication inputs
export interface IAuthenticate {
  email : string;
  password: string;
  // fakeApikeyField: string;
}

/*
// Login result
export interface IUser {
  name: string;
  apikey: string;
  isLoggedIn: boolean;
}


export class User implements IUser {
  public name: string;
  public apikey: string;
  public isLoggedIn: boolean;
 
  constructor(user: IUser) {
    this.name = user ? user.name : '';
    this.apikey = user ? user.apikey : undefined;
    this.isLoggedIn = user ? user.isLoggedIn : false;
  }

}

*/
