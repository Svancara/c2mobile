/* Options:
Date: 2018-01-11 17:19:35
Version: 5.00
Tip: To override a DTO option, remove "//" prefix before updating
BaseUrl: http://localhost:8042

//GlobalNamespace: 
//MakePropertiesOptional: True
//AddServiceStackTypes: True
//AddResponseStatus: False
//AddImplicitVersion: 
//AddDescriptionAsComments: True
//IncludeTypes: 
//ExcludeTypes: 
//DefaultImports: 
*/


export interface IReturn<T>
{
    createResponse() : T;
}

export interface IReturnVoid
{
    createResponse() : void;
}

// @DataContract
export class ResponseError
{
    // @DataMember(Order=1, EmitDefaultValue=false)
    ErrorCode: string;

    // @DataMember(Order=2, EmitDefaultValue=false)
    FieldName: string;

    // @DataMember(Order=3, EmitDefaultValue=false)
    Message: string;

    // @DataMember(Order=4, EmitDefaultValue=false)
    Meta: { [index:string]: string; };
}

// @DataContract
export class ResponseStatus
{
    // @DataMember(Order=1)
    ErrorCode: string;

    // @DataMember(Order=2)
    Message: string;

    // @DataMember(Order=3)
    StackTrace: string;

    // @DataMember(Order=4)
    Errors: ResponseError[];

    // @DataMember(Order=5)
    Meta: { [index:string]: string; };
}

export class C2Error
{
    title: string;
    message: string;
    errors: string[];
}

export class LastQuoteData
{
    ok: string;
    error: C2Error;
    quotefeedstatus: number;
    quotefeederror: string;
    quote_type: string;
    /**
    * Passed back by C2 what we sent in the request 
    */
    // @ApiMember(Description="Passed back by C2 what we sent in the request ")
    typeofsymbol: string;

    lastTradeDate: string;
    bid: number;
    strike: number;
    marketOpen: string;
    percentChange: number;
    lastSize: number;
    range: number;
    high: number;
    bidSize: number;
    close: number;
    low: number;
    expiration: string;
    openInterest: number;
    totalVolume: number;
    lastTradeTime: string;
    askTime: string;
    symbol: string;
    change: number;
    bidTime: string;
    last: number;
    description: string;
    ask: number;
    open: number;
    askSize: number;
    settle: number;
    exchange: string;
}

export class Component_Signals
{
    symbol: string;
    signalid: number;
    action: string;
    quant: number;
    instrument: string;
    tradeprice: number;
    tradedwhen: string;
}

export class Trade
{
    closeVWAP_timestamp: number;
    strike: number;
    open_or_closed: string;
    expir: string;
    openVWAP_timestamp: number;
    underlying: string;
    closing_price_VWAP: number;
    putcall: string;
    component_signals: Component_Signals[];
    quant_closed: number;
    markToMarket_time: string;
    opening_price_VWAP: number;
    trade_id: number;
    symbol: string;
    quant_opened: number;
    closedWhen: string;
    instrument: string;
    ptValue: number;
    PL: number;
    closedWhenUnixTimeStamp: number;
    openedWhen: string;
    long_or_short: string;
    symbol_description: string;
    systemid: number;
}

export class SymbolSearchTypeAheadSupport
{
    Guid: number;
    Datasource: string;
    Symbol: string;
    Descrip: string;
    ListedMarket: string;
    Frontmonth: string;
    YYYYMM: string;
    Instrument: string;
    Exchange: string;
    /**
    * Just a formal field in order to generate it on the Typecript side
    */
    // @ApiMember(Description="Just a formal field in order to generate it on the Typecript side")
    returnToRoute: string[];
}

export class ForexSymbol
{
    Guid: number;
    Symbol: string;
    Descrip: string;
}

export class OptionSymbol
{
    Guid: number;
    Symbol: string;
    Description: string;
    Expiration: string;
    CallPut: string;
    Strike: number;
}

export class OptionCallPut
{
    Symbol: string;
    Description: string;
    CallPut: string;
}

export class OptionStrike
{
    Strike: number;
    Call: OptionCallPut;
    Put: OptionCallPut;
}

export class OptionSymbolByExpiration
{
    Guid: number;
    Underlying: string;
    Expiration: string;
    Strikes: OptionStrike[];
}

export class OptionExpiry
{
    Expiration: string;
    Call: OptionCallPut;
    Put: OptionCallPut;
}

export class OptionSymbolByStrike
{
    Guid: number;
    Underlying: string;
    Strike: number;
    Expirations: OptionExpiry[];
}

export class HelloResponse
{
    Result: string;
}

export class GetLastQuoteResponse
{
    ResponseStatus: ResponseStatus;
    lastQuoteData: LastQuoteData;
}

export class FundamentalDataResponse
{
    ResponseStatus: ResponseStatus;
    error: C2Error;
    FiftyTwoWeekHighDate: string;
    BalanceSheetDate: string;
    NAICS: string;
    quotefeederror: string;
    CouponRate: number;
    CommonSharesOutstanding: number;
    Symbol: string;
    ExpirationDate: string;
    StrikePrice: number;
    SplitFactor1: string;
    FiscalYearEnd: string;
    DividendAmount: number;
    CurrentLiabilities: number;
    FiveYearGrowthPercentage: number;
    FiftyTwoWeekLow: number;
    HistoricalVolatility: number;
    ExchangeRoot: string;
    Beta: number;
    CalendarYearLow: number;
    YearEndClose: number;
    PayDate: string;
    ShortInterest: number;
    DividendRate: number;
    CurrentYearEPS: number;
    ListedMarket: string;
    PE: number;
    CompanyName: string;
    FormatCode: string;
    SplitFactor2: string;
    Precision: string;
    OptionSymbol: string;
    Leaps: string;
    CalendarYearHighDate: string;
    ok: string;
    quotefeedstatus: number;
    DividendYield: number;
    FiftyTwoWeekHigh: number;
    ExchangeID: string;
    AverageVolume: number;
    CalendarYearLowDate: string;
    LongTermDebt: number;
    PercentHeldByInstitutions: number;
    FiftyTwoWeekLowDate: string;
    SIC: string;
    CalendarYearHigh: number;
    NextYearEPS: number;
    ExdividendDate: string;
    CurrentAssets: number;
    SecurityType: string;
    MaturityDate: string;
    /**
    * Passed back by C2 what we sent in the request 
    */
    // @ApiMember(Description="Passed back by C2 what we sent in the request ")
    typeofsymbol: string;
}

export class ClosedTradesResponse
{
    ResponseStatus: ResponseStatus;
    ok: string;
    TotalRecords: number;
    response: Trade[];
}

export class SymbolSearchResponse
{
    ResponseStatus: ResponseStatus;
    Data: SymbolSearchTypeAheadSupport[];
}

export class ForexPairsResponse
{
    ResponseStatus: ResponseStatus;
    Data: ForexSymbol[];
}

export class OptionSymbolsSearchResponse
{
    ResponseStatus: ResponseStatus;
    Data: OptionSymbol[];
}

export class OptionSearchByExpirationResponse
{
    ResponseStatus: ResponseStatus;
    Data: OptionSymbolByExpiration[];
}

export class OptionSearchByStrikeResponse
{
    ResponseStatus: ResponseStatus;
    Data: OptionSymbolByStrike[];
}

// @Route("/hello/{Name}")
export class Hello implements IReturn<HelloResponse>
{
    Name: string;
    createResponse() { return new HelloResponse(); }
    getTypeName() { return "Hello"; }
}

// @Route("/getLastQuote")
export class GetLastQuote implements IReturn<GetLastQuoteResponse>
{
    apikey: string;
    market: string;
    typeofsymbol: string;
    symbol: string;
    /**
    * Just a formal field in order to generate it on the Typecript side
    */
    // @ApiMember(Description="Just a formal field in order to generate it on the Typecript side")
    returnToRoute: string[];
    createResponse() { return new GetLastQuoteResponse(); }
    getTypeName() { return "GetLastQuote"; }
}

// @Route("/getFundamentalData")
export class GetFundamentalData implements IReturn<FundamentalDataResponse>
{
    apikey: string;
    market: string;
    typeofsymbol: string;
    symbol: string;
    /**
    * Just a formal field in order to generate it on the Typecript side
    */
    // @ApiMember(Description="Just a formal field in order to generate it on the Typecript side")
    returnToRoute: string[];
    createResponse() { return new FundamentalDataResponse(); }
    getTypeName() { return "GetFundamentalData"; }
}

// @Route("/closedtrades", "GET,POST")
export class ClosedTrades implements IReturn<ClosedTradesResponse>
{
    apikey: string;
    systemid: number;
    show_component_signals: string;
    Lazy: boolean;
    First: number;
    Rows: number;
    SortField: string;
    SortOrder: number;
    createResponse() { return new ClosedTradesResponse(); }
    getTypeName() { return "ClosedTrades"; }
}

// @Route("/symbolsearch", "GET,POST")
export class SymbolSearch implements IReturn<SymbolSearchResponse>
{
    apiKey: string;
    market: string;
    instrument: string;
    query: string;
    /**
    * Just a formal field in order to generate it on the Typecript side
    */
    // @ApiMember(Description="Just a formal field in order to generate it on the Typecript side")
    returnToRoute: string[];
    createResponse() { return new SymbolSearchResponse(); }
    getTypeName() { return "SymbolSearch"; }
}

// @Route("/forexpairs", "GET,POST")
export class ForexPairs implements IReturn<ForexPairsResponse>
{
    apikey: string;
    /**
    * Just a formal field in order to generate it on the Typecript side
    */
    // @ApiMember(Description="Just a formal field in order to generate it on the Typecript side")
    returnToRoute: string[];
    createResponse() { return new ForexPairsResponse(); }
    getTypeName() { return "ForexPairs"; }
}

// @Route("/optionssearch", "GET,POST")
export class OptionSymbolsSearch implements IReturn<OptionSymbolsSearchResponse>
{
    apikey: string;
    underlaying: string;
    optionType: string;
    /**
    * Just a formal field in order to generate it on the Typecript side
    */
    // @ApiMember(Description="Just a formal field in order to generate it on the Typecript side")
    returnToRoute: string[];
    createResponse() { return new OptionSymbolsSearchResponse(); }
    getTypeName() { return "OptionSymbolsSearch"; }
}

// @Route("/optionsByExpiration", "GET,POST")
export class OptionSearchByExpiration implements IReturn<OptionSearchByExpirationResponse>
{
    apikey: string;
    underlaying: string;
    optionType: string;
    /**
    * Just a formal field in order to generate it on the Typecript side
    */
    // @ApiMember(Description="Just a formal field in order to generate it on the Typecript side")
    returnToRoute: string[];
    createResponse() { return new OptionSearchByExpirationResponse(); }
    getTypeName() { return "OptionSearchByExpiration"; }
}

// @Route("/optionsByStrike", "GET,POST")
export class OptionSearchByStrike implements IReturn<OptionSearchByStrikeResponse>
{
    apikey: string;
    underlaying: string;
    optionType: string;
    /**
    * Just a formal field in order to generate it on the Typecript side
    */
    // @ApiMember(Description="Just a formal field in order to generate it on the Typecript side")
    returnToRoute: string[];
    createResponse() { return new OptionSearchByStrikeResponse(); }
    getTypeName() { return "OptionSearchByStrike"; }
}
