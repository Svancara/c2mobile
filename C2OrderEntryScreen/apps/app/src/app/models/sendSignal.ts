import { ApiSystemRequest, ISendSignal, C2Action, OK, C2Error, InstrumentString, Signal } from "@app/models";
import { HttpErrorResponse } from "@angular/common/http";


export class SendSignal implements ISendSignal {
  public strategyId: number;
  public action: C2Action;
  public quant: number;
  public symbol: string;
  public typeofsymbol: InstrumentString;
  public limit: number;
  public stop: number;
  public duration: string;
  public market: OK;
  public stoploss: number;
  public profittarget: number;
  public _parkUntilSecs_: number;
  public parkUntilYYYYMMDDHHMM: string;
}

export interface ISendSignalResponse {
  ok: OK;
  status?: C2Error;
  error?: C2Error;
  response: SendSignal;
  signalid: number;
  elapsed_time: number;

}


export interface ICancelSignal extends ApiSystemRequest {
  signalid: number;
}

export interface CanceledSignal {
  market: OK;
  strike: number;
  typeofsymbol: InstrumentString;
  signalid: number;
  expir: null;
  personid: number;
  underlying: null;
  putcall: null;
  day: OK;
  gtc: OK;
  quant: number;
  guid: number;
  expiredwhen: "0",
  tradeprice: number;
  killedwhen: number;
  systemid: number;
  symbol: string;
  islimit: number;
  postedwhen: number;
  stop: number;
  action: C2Action,
  signalexpiresat: number;
  tradedwhen: number;
}

export class CancelSignalResponse {
  ok: OK;
  status?: C2Error;
  error?: C2Error;
  response: CanceledSignal;
  signalid: number;
  elapsed_time: number;
}


export function CancelSignalHttpException(status: HttpErrorResponse, signal: Signal) {
  this.status = status;
  this.signal = signal;
}
