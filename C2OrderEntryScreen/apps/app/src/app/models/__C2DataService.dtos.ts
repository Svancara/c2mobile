/* Options:
Date: 2017-12-17 06:49:23
Version: 4.514
Tip: To override a DTO option, remove "//" prefix before updating
BaseUrl: http://54.86.43.216:8040

//GlobalNamespace: 
//MakePropertiesOptional: True
//AddServiceStackTypes: True
//AddResponseStatus: False
//AddImplicitVersion: 
//AddDescriptionAsComments: True
//IncludeTypes: 
//ExcludeTypes: 
//DefaultImports: 
*/


export interface IReturnVoid
{
    createResponse() : void;
}

export interface IReturn<T>
{
    createResponse() : T;
}

export type Instrument = "Unknown" | "Stock" | "Forex" | "Future" | "Option" | "Mutual" | "Index" | "Forward";

// @DataContract
export class ResponseError
{
    // @DataMember(Order=1, EmitDefaultValue=false)
    ErrorCode: string;

    // @DataMember(Order=2, EmitDefaultValue=false)
    FieldName: string;

    // @DataMember(Order=3, EmitDefaultValue=false)
    Message: string;

    // @DataMember(Order=4, EmitDefaultValue=false)
    Meta: { [index:string]: string; };
}

// @DataContract
export class ResponseStatus
{
    // @DataMember(Order=1)
    ErrorCode: string;

    // @DataMember(Order=2)
    Message: string;

    // @DataMember(Order=3)
    StackTrace: string;

    // @DataMember(Order=4)
    Errors: ResponseError[];

    // @DataMember(Order=5)
    Meta: { [index:string]: string; };
}

export type C2DataSource = "IqFeed" | "CQG";

// @DataContract
export class Symbol
{
    // @DataMember
    Ticker: string;

    // @DataMember
    Instrument: Instrument;

    // @DataMember
    DataSource: C2DataSource;

    // @DataMember
    Market: string;

    // @DataMember
    DatasourceGroup: string;
}

export class OHLC
{
    Open: number;
    High: number;
    Low: number;
    Close: number;
    Volume: number;
}

export class OHLCObject extends OHLC
{
    DateTime: string;
}

export class SplitInfo
{
    Date: string;
    Ratio: number;
}

export class DailyOHLCDataResponse
{
    ResponseStatus: ResponseStatus;
    Symbol: Symbol;
    Data: OHLCObject[];
}

export class MinuteOHLCDataResponse
{
    ResponseStatus: ResponseStatus;
    Symbol: Symbol;
    Splits: SplitInfo[];
    Data: OHLCObject[];
}

export class MinuteOHLCDataCSVResponse
{
    ResponseStatus: ResponseStatus;
    Symbol: Symbol;
    Data: string;
}

export class FundamentalDataResponse
{
    /**
    * Historical Data Service status and error. For example: It can't reach IQFeed datafeed.
    */
    // @ApiMember(Description="Historical Data Service status and error. For example: It can't reach IQFeed datafeed.")
    ResponseStatus: ResponseStatus;

    /**
    * 'ok', 'error'
    */
    // @ApiMember(Description="'ok', 'error'")
    // @ApiMember(Description="Datafeed (IQFeed, CQG, ...) status: 'ok', 'error'")
    status: string;

    /**
    * Datafeed (IQFeed, CQG, ...) error. For example: 'invalid_symbol', 'no_data', 'timeout', 'Unauthorized user ID'.
    */
    // @ApiMember(Description="Datafeed (IQFeed, CQG, ...) error. For example: 'invalid_symbol', 'no_data', 'timeout', 'Unauthorized user ID'.")
    error: string;

    /**
    * Collective2 symbol
    */
    // @ApiMember(Description="Collective2 symbol")
    Symbol: string;

    /**
    * Collective2 exchange
    */
    // @ApiMember(Description="Collective2 exchange")
    ExchangeAbbrev: string;

    /**
    * Collective2 instrument type
    */
    // @ApiMember(Description="Collective2 instrument type")
    InstrumentType: string;

    /**
    * IQFeed: a hexadecimal number
    */
    // @ApiMember(Description="IQFeed: a hexadecimal number")
    ExchangeID: string;

    PE: number;
    AverageVolume: number;
    FiftyTwoWeekHigh: number;
    FiftyTwoWeekLow: number;
    CalendarYearHigh: number;
    CalendarYearLow: number;
    DividendYield: number;
    DividendAmount: number;
    DividendRate: number;
    PayDate: string;
    ExdividendDate: string;
    ShortInterest: number;
    CurrentYearEPS: number;
    NextYearEPS: number;
    FiveYearGrowthPercentage: number;
    FiscalYearEnd: number;
    CompanyName: string;
    OptionSymbol: string;
    PercentHeldByInstitutions: number;
    Beta: number;
    Leaps: string;
    CurrentAssets: number;
    CurrentLiabilities: number;
    BalanceSheetDate: string;
    LongTermDebt: number;
    CommonSharesOutstanding: number;
    SplitFactor1: string;
    SplitFactor2: string;
    FormatCode: string;
    Precision: number;
    SIC: number;
    HistoricalVolatility: number;
    SecurityType: string;
    ListedMarket: string;
    FiftyTwoWeekHighDate: string;
    FiftyTwoWeekLowDate: string;
    CalendarYearHighDate: string;
    CalendarYearLowDate: string;
    YearEndClose: number;
    MaturityDate: string;
    CouponRate: number;
    ExpirationDate: string;
    StrikePrice: number;
    NAICS: number;
    ExchangeRoot: string;
}

// @Route("/history/daiyohlc", "GET,POST")
export class DailyOHLCData implements IReturn<DailyOHLCDataResponse>
{
    // @ApiMember(IsRequired=true)
    Symbol: string;

    /**
    * Default: stock
    */
    // @ApiMember(DataType="Instrument", Description="Default: stock")
    Instrument: Instrument;

    /**
    * Default: US
    */
    // @ApiMember(Description="Default: US")
    Market: string;

    /**
    * Format: yyyyMMdd or yyyy-MM-dd or yyyy-MM-dd hh:mm or yyyy-MM-dd hh:mm:ss
    */
    // @ApiMember(Description="Format: yyyyMMdd or yyyy-MM-dd or yyyy-MM-dd hh:mm or yyyy-MM-dd hh:mm:ss", IsRequired=true)
    DateStart: string;

    /**
    * Format: yyyyMMdd or yyyy-MM-dd or yyyy-MM-dd hh:mm or yyyy-MM-dd hh:mm:ss   Default: NOW
    */
    // @ApiMember(Description="Format: yyyyMMdd or yyyy-MM-dd or yyyy-MM-dd hh:mm or yyyy-MM-dd hh:mm:ss   Default: NOW")
    DateEnd: string;

    /**
    * If DateStart is a weekend or a hole in the data, prepend a previous bar.
    */
    // @ApiMember(DataType="bool", Description="If DateStart is a weekend or a hole in the data, prepend a previous bar.")
    IncludePrevious: boolean;
    createResponse() { return new DailyOHLCDataResponse(); }
    getTypeName() { return "DailyOHLCData"; }
}

// @Route("/history/minuteohlc", "GET,POST")
export class MinuteOHLCData implements IReturn<MinuteOHLCDataResponse>
{
    // @ApiMember(IsRequired=true)
    Symbol: string;

    /**
    * Default: stock
    */
    // @ApiMember(DataType="Instrument", Description="Default: stock")
    Instrument: Instrument;

    /**
    * Default: US
    */
    // @ApiMember(Description="Default: US")
    Market: string;

    /**
    * Format: yyyyMMdd HHmm
    */
    // @ApiMember(Description="Format: yyyyMMdd HHmm", IsRequired=true)
    DateStart: string;

    /**
    * Format: yyyyMMdd HHmm  Default: NOW
    */
    // @ApiMember(Description="Format: yyyyMMdd HHmm  Default: NOW")
    DateEnd: string;

    /**
    * If DateStart is a weekend or a hole in the data, prepend a previous bar. Default: true
    */
    // @ApiMember(DataType="bool", Description="If DateStart is a weekend or a hole in the data, prepend a previous bar. Default: true")
    IncludePrevious: boolean;

    /**
    * Apply splits. Default: true
    */
    // @ApiMember(DataType="bool", Description="Apply splits. Default: true")
    ApplySplits: boolean;
    createResponse() { return new MinuteOHLCDataResponse(); }
    getTypeName() { return "MinuteOHLCData"; }
}

// @Route("/history/minuteohlc/csv", "GET,POST")
export class MinuteOHLCDataCSV implements IReturn<MinuteOHLCDataCSVResponse>
{
    // @ApiMember(IsRequired=true)
    Symbol: string;

    /**
    * Default: stock
    */
    // @ApiMember(DataType="Instrument", Description="Default: stock")
    Instrument: Instrument;

    /**
    * Default: US
    */
    // @ApiMember(Description="Default: US")
    Market: string;

    /**
    * Format: yyyyMMdd HHmm
    */
    // @ApiMember(Description="Format: yyyyMMdd HHmm", IsRequired=true)
    DateStart: string;

    /**
    * Format: yyyyMMdd HHmm  Default: NOW
    */
    // @ApiMember(Description="Format: yyyyMMdd HHmm  Default: NOW")
    DateEnd: string;

    /**
    * If DateStart is a weekend or a hole in the data, prepend a previous bar.
    */
    // @ApiMember(DataType="bool", Description="If DateStart is a weekend or a hole in the data, prepend a previous bar.")
    IncludePrevious: boolean;
    createResponse() { return new MinuteOHLCDataCSVResponse(); }
    getTypeName() { return "MinuteOHLCDataCSV"; }
}

// @Route("/fundamental", "GET,POST")
// @Route("/fundamental/{market}/{instrument}/{symbol}", "GET,POST")
export class FundamentalData implements IReturn<FundamentalDataResponse>
{
    // @ApiMember(IsRequired=true)
    Symbol: string;

    /**
    * Default: stock
    */
    // @ApiMember(DataType="Instrument", Description="Default: stock")
    Instrument: Instrument;

    /**
    * Default: US
    */
    // @ApiMember(Description="Default: US")
    Market: string;
    createResponse() { return new FundamentalDataResponse(); }
    getTypeName() { return "FundamentalData"; }
}
