import { OK, TIF, PutCall, Instrument, C2Action, C2Error} from "@app/models/types";


export interface Signal {
  isLimitOrder: number;
  strike: number;
  status: string;
  underlying: string;
  isMarketOrder: number;
  tif: TIF;
  putcall: PutCall;
  expiration: string;
  quant: number;
  symbol: string;
  name: string;
  instrument: Instrument;
  isStopOrder: number;
  posted_time_unix: number;
  action: C2Action;
  signal_id: number;
  posted_time: string;

  // Let's have it for ngrx Entities
  id: string;
  // A missing part from the Collective2 side
  strategyId: number;
}

export interface SignalsWorking {
  ok: OK;
  status?: C2Error;
  error?: C2Error;
  response: Signal[];
  // A missing part from the Collective2 side
  strategyId: number;
}
