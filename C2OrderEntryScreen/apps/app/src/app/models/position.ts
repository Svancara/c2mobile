import { PutCall, LongOrShort, InstrumentString, OK, C2Error} from "@app/models";

export interface Position {
  closeVWAP_timestamp: number;
  strike: number;
  open_or_closed: string;
  expir: string;
  openVWAP_timestamp: number;
  underlying: string;
  closing_price_VWAP: number;
  putcall: PutCall;
  long_or_short: LongOrShort;
  quant_closed: number;
  markToMarket_time: string;
  trade_id: number;
  symbol: string;
  opening_price_VWAP: number;
  quant_opened: number;
  closedWhen: number;
  instrument: InstrumentString;
  ptValue: number;
  PL: number;
  closedWhenUnixTimeStamp: number;
  openedWhen: string;
  symbol_description: string;
  // Let's have it for ngrx Entities
  id: string;
  // A missing part from the Collective2 side
  strategyId: number;

}

export interface RequestTradesResponse {
  ok: OK;
  status?: C2Error;
  error?: C2Error;
  response: Position[];
  // A missing part from the Collective2 side
  strategyId: number;
}

