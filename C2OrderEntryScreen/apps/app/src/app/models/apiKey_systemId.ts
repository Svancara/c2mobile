export interface ApiKey{
  apikey: string
}

export interface ApiSystemRequest
{
  systemid : number,
  apikey : string
}
