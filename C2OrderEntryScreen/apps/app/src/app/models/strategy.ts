import { OK, C2Error } from "@app/models";

export interface IStrategy {
  longDescription: string,
  system_id: number,
  creatorScreenName: string,
  ownerpersonid: number, // ToDo: string on C2 TEST IT!
  trades_stocks: OK,
  owner_email: string,
  owner_lastname: string,
  test_system: OK,
  shorts_stocks: OK,
  minimum_portfolio_size_required: number,
  freeTrialPeriodDays: number,
  creditSystemTimeUntil_epochSecs: number,
  freeSignalEntryCredits: OK,
  owner_screenname: string,
  owner_firstname: string,
  trades_options: OK,
  shorts_options: OK,
  systemName: string,
  equitycurve_startingcapital: number,
  monthlyFee: number,
  createdWhen: string,
  trades_forex: OK,
  isAlive: OK,
  shortDescription: null,
  trades_futures: OK,
  creditSystemTimeUntil_clock: string,

  id: string; // for ngrx Entities
}

export interface UserStrategies {
  ok: OK,
  status?: C2Error,
  error?: C2Error,
  response: IStrategy[],
}

export class Strategy implements IStrategy {
  public longDescription: string;
  public system_id: number;
  public creatorScreenName: string;
  public ownerpersonid: number;
  public trades_stocks: OK;
  public owner_email: string;
  public owner_lastname: string;
  public test_system: OK;
  public shorts_stocks: OK;
  public minimum_portfolio_size_required: number;
  public freeTrialPeriodDays: number;
  public creditSystemTimeUntil_epochSecs: number;
  public freeSignalEntryCredits: OK;
  public owner_screenname: string;
  public owner_firstname: string;
  public trades_options: OK;
  public shorts_options: OK;
  public systemName: string;
  public equitycurve_startingcapital: number;
  public monthlyFee: number;
  public createdWhen: string;
  public trades_forex: OK;
  public isAlive: OK;
  public shortDescription: null;
  public trades_futures: OK;
  public creditSystemTimeUntil_clock: string;
  public id: string;

  constructor(s?: IStrategy) {
    this.longDescription = s ? s.longDescription : undefined;
    this.system_id = s ? s.system_id : undefined;
    this.creatorScreenName = s ? s.creatorScreenName : undefined;
    this.ownerpersonid = s ? s.ownerpersonid : undefined;
    this.trades_stocks = s ? s.trades_stocks : undefined;
    this.owner_email = s ? s.owner_email : undefined;
    this.owner_lastname = s ? s.owner_lastname : undefined;
    this.test_system = s ? s.test_system : undefined;
    this.shorts_stocks = s ? s.shorts_stocks : undefined;
    this.minimum_portfolio_size_required = s ? s.minimum_portfolio_size_required : undefined;
    this.freeTrialPeriodDays = s ? s.freeTrialPeriodDays : undefined;
    this.creditSystemTimeUntil_epochSecs = s ? s.creditSystemTimeUntil_epochSecs : undefined;
    this.freeSignalEntryCredits = s ? s.freeSignalEntryCredits : undefined;
    this.owner_screenname = s ? s.owner_screenname : undefined;
    this.owner_firstname = s ? s.owner_firstname : undefined;
    this.trades_options = s ? s.trades_options : undefined;
    this.shorts_options = s ? s.shorts_options : undefined;
    this.systemName = s ? s.systemName : undefined;
    this.equitycurve_startingcapital = s ? s.equitycurve_startingcapital : undefined;
    this.monthlyFee = s ? s.monthlyFee : undefined;
    this.createdWhen = s ? s.createdWhen : undefined;
    this.trades_forex = s ? s.trades_forex : undefined;
    this.isAlive = s ? s.isAlive : undefined;
    this.shortDescription = s ? s.shortDescription : undefined;
    this.trades_futures = s ? s.trades_futures : undefined;
    this.creditSystemTimeUntil_clock = s ? s.creditSystemTimeUntil_clock : undefined;
    this.id = s ? s.id : undefined;
  }
}
