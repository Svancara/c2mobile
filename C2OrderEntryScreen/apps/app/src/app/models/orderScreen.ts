import { Instrument, C2Action} from "@app/models";

export interface OrderScreenSymbol {
  ticker: string;
  instrument: Instrument;
  name: string;
  lastPrice: number;
}

export interface OrderScreen {
  quantity: number;
  limitPrice: number;
  stopPrice: number;
}


export interface ISendSignal {
  strategyId: number;
  action: C2Action;
  quant: number;
  symbol: string; // "@ESM5",
  typeofsymbol: string; //"future",
  limit: number;
  stop: number;
  duration: string; // "DAY"
  market: string; //1,
  stoploss: number;
  profittarget: number;
  /**
  * in UTC int his project  Math.floor(Date.now() / 1000);
  */
  _parkUntilSecs_: number; 
  /**
  * A user is asked to enter EST time
  */
  parkUntilYYYYMMDDHHMM: string;

}

export interface SendSignalPayload {
  apikey: string;
  systemid: number;
  signal: ISendSignal;
}


// ToDo: SendSignalResponse
export interface SendSignalResponse {
  //apikey: string;
  //systemid: number;
  //signal: SendSignal;
}

