import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SymbolSearchContainerComponent } from './containers/symbol-search-container.component';
import { AuthGuard } from '@app/core/services/auth-guard.service';

const routes: Routes = [
  { path: 'searchsymbol', component: SymbolSearchContainerComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SymbolSearchRoutingModule { }
