import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Sandbox } from '../shared/sandbox/base.sandbox';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import * as fromStore from '@app/store';
import * as fromSymbolsSearch from '@app/store/reducers/symbol-search';

import { Load as symbolSearchLoad } from '@app/store/actions/symbol-search/collection';
import * as symbolSearchActions from '@app/store/actions/symbol-search/symbol-search';

import * as optionSearchByExpirationActions from '@app/store/actions/search-option-by-expiration.actions';
import * as fromOptionsSearchByExpiration from '@app/store/reducers/search-option-by-expiration.reducer';

import * as optionSearchByStrikeActions from '@app/store/actions/search-option-by-strike.actions';
import * as fromOptionsSearchByStrike from '@app/store/reducers/search-option-by-strike.reducer';

import * as forexActions from '@app/store/actions/search-forex.actions';
import * as fromForex from '@app/store/reducers/search-forex.reducer';

import { Clear } from '@app/store/actions/symbol-search/symbol-search';

// import { LoadByExpiration as optionSearchLoad } from '@app/store/actions/option-search/collection';

// import * as gp from '@app/store/actions/gp.action';

import {
  SymbolSearch,
  SymbolSearchTypeAheadSupport,
  OptionSearchByExpiration,
  OptionSymbolByExpiration,
  OptionSymbolByStrike,
  OptionSearchByStrike,
  ForexSymbol,
  ForexPairs
} from '@app/models/LastQuoteService.dtos';

import { InternaSearchSymbolRequest, GetLastQuote } from '@app/models';
import { SelectedOptionSymbol } from '@app/store/actions/gp.action';


@Injectable()
export class SymbolSearchSandboxService extends Sandbox {

  public getSearchQuery$: Observable<InternaSearchSymbolRequest>;

  public symbolsFound$: Observable<SymbolSearchTypeAheadSupport[]>;
  public optionsByExpirationFound$: Observable<OptionSymbolByExpiration[]>;
  public optionsByStrikeFound$: Observable<OptionSymbolByStrike[]>;
  public forexSymbols$: Observable<ForexSymbol[]>;
  
  public loadingSymbols$: Observable<boolean>;
  public loadingOptionsByExpiration$: Observable<boolean>;
  public loadingOptionsByStrike$: Observable<boolean>;
  public loadingForex$: Observable<boolean>;

  constructor(private store: Store<fromStore.State>, private router: Router) {
    super(store);
    this.getSearchQuery$ = this.store.select(fromStore.getSearchSymbolPartialQuery);
    this.symbolsFound$ = this.store.select(fromSymbolsSearch.getAllSymbols);
    this.optionsByExpirationFound$ = this.store.select(fromOptionsSearchByExpiration.getOptionsByExpirations);
    this.optionsByStrikeFound$ = this.store.select(fromOptionsSearchByStrike.getOptionsByStrikes);
    this.forexSymbols$ = this.store.select(fromForex.getForexSymbols);
    
    this.loadingSymbols$ = this.store.select(fromSymbolsSearch.getCollectionLoading);
    this.loadingOptionsByExpiration$ = this.store.select(fromOptionsSearchByExpiration.getOptionsByExpirationsLoading);
    this.loadingOptionsByStrike$ = this.store.select(fromOptionsSearchByStrike.getOptionsByStrikesLoading);
    this.loadingForex$ = this.store.select(fromForex.getForexSymbolsLoading);
  }

  /**
   * Search for symbol (except options)
   * @param request
   */
  symbolSearch(query: SymbolSearch) {
    this.store.dispatch(new symbolSearchLoad(query));
  }

  /**
   * Search for options
   * @param request
   */
  optionSearch(query: OptionSearchByExpiration) {
    this.store.dispatch(new optionSearchByExpirationActions.LoadByExpiration(query));
    let qstrike: OptionSearchByStrike = new OptionSearchByStrike();
    qstrike.optionType = query.optionType;
    qstrike.returnToRoute = query.returnToRoute;
    qstrike.underlaying = query.underlaying;
    this.store.dispatch(new optionSearchByStrikeActions.LoadByStrike(qstrike));
  }


  /**
   * Load all forex symbols
   */
  loadForexData(request: ForexPairs) {
    this.store.dispatch(new forexActions.Load(request));
  }


  symbolSelected(symbol: SymbolSearchTypeAheadSupport) {
    this.store.dispatch(new symbolSearchActions.Select(symbol));
    this.router.navigate(symbol.returnToRoute);
  }

  optionSelected(symbol: GetLastQuote) {
    this.store.dispatch(new SelectedOptionSymbol(symbol));
    this.router.navigate(symbol.returnToRoute);
  }
  
  forexSymbolSelected(symbol: GetLastQuote) {
    this.store.dispatch(new forexActions.Select(symbol));
    this.router.navigate(symbol.returnToRoute);
  }

  clearResults() {
    // Remove a previous list. It is stale. It can be very very long and it can take time to show it in the browser for nothing.
    this.store.dispatch(new Clear());
  }


}
