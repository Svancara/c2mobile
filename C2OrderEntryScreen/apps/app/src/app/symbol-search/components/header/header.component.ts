import { Component, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-symbol-search-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SymbolSearchHeaderComponent {

  @Input() returnToRoute: string[];
  @Output() settingsEvent = new EventEmitter();

  settings() {
    this.settingsEvent.emit();
  }

}
