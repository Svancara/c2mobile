import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrimeNgModule } from '@app/primeng.module';
//import { SymbolSearchComponent } from './symbol-search.component';

@NgModule({
  imports: [
    CommonModule,
    PrimeNgModule
  ],
  declarations: [],
  exports: [],
})
export class SymbolSearchComponentsModule { }
