import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-forex-search-box',
  templateUrl: './forex-search-box.component.html',
  styleUrls: ['./forex-search-box.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ForexSearchBoxComponent  {

  @Input() query: string;
  @Output() queryTextEmitter: EventEmitter<string> = new EventEmitter<string>();

  
  searchsymbol() {
    this.queryTextEmitter.emit(this.query);
  }

  
}


