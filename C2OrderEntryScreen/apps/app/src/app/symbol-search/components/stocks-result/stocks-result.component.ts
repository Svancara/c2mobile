import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { SymbolSearchTypeAheadSupport } from '@app/models/LastQuoteService.dtos';

@Component({
  selector: 'app-stocks-result',
  templateUrl: './stocks-result.component.html',
  styleUrls: ['./stocks-result.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StocksResultComponent {

  @Input() symbolsFound: SymbolSearchTypeAheadSupport[];
  @Input() loading: false;
  @Output() symbolSelectedEmmiter: EventEmitter<SymbolSearchTypeAheadSupport> = new EventEmitter<SymbolSearchTypeAheadSupport>();


  symbolSelected(symbol: SymbolSearchTypeAheadSupport) {
    this.symbolSelectedEmmiter.emit(symbol);
  }

}
