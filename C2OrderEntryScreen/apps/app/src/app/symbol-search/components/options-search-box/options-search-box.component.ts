import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-options-search-box',
  templateUrl: './options-search-box.component.html',
  styleUrls: ['./options-search-box.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OptionsSearchBoxComponent implements OnInit {

  @Input() underlayingSymbol: string;
  @Input() selectedMarket: string = 'US';
  @Output() queryTextEmitter: EventEmitter<string> = new EventEmitter<string>();

  placeholder = "Stock symbol"; //  'Products' | translate"

  constructor(private translate: TranslateService) {
  }

  ngOnInit() {
//    this.placeholder = this.translate.instant('UnderlayingSymbol');
  }

  searchsymbol() {
    this.queryTextEmitter.emit(this.underlayingSymbol);
  }

}
