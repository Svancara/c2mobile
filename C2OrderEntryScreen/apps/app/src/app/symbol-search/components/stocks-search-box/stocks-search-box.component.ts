import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-stocks-search-box',
  templateUrl: './stocks-search-box.component.html',
  styleUrls: ['./stocks-search-box.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StocksSearchBoxComponent {

  @Input() query: string;
  @Input() selectedMarket: string = 'US';
  @Output() queryTextEmitter: EventEmitter<string> = new EventEmitter<string>();
    
  searchsymbol() {
    this.queryTextEmitter.emit(this.query);
  }


}
