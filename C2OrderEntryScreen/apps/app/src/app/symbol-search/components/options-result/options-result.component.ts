import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { OptionExpiry, OptionCallPut, OptionStrike, SymbolSearchTypeAheadSupport, OptionSymbolByExpiration, OptionSymbolByStrike } from '@app/models/LastQuoteService.dtos';
import { SelectItem } from 'primeng/components/common/selectitem';
import { GetLastQuote } from '@app/models';



@Component({
  selector: 'app-options-result',
  templateUrl: './options-result.component.html',
  styleUrls: ['./options-result.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OptionsResultComponent implements OnInit {

  private _optionsByExpirationFound: OptionSymbolByExpiration[];

  @Input()
  get optionsByExpiration(): OptionSymbolByExpiration[] { return this._optionsByExpirationFound }
  set optionsByExpiration(value: OptionSymbolByExpiration[]) {
    if (value) {
      this._optionsByExpirationFound = value;
      this.formatByExpiration(this._optionsByExpirationFound);
    }
  }

  private _optionsByStrikeFound: OptionSymbolByStrike[];
  
  @Input()
  get optionsByStrike(): OptionSymbolByStrike[] { return this._optionsByStrikeFound }
  set optionsByStrike(value: OptionSymbolByStrike[]) {
    if (value) {
      this._optionsByStrikeFound = value;
      this.formatByStrike(this._optionsByStrikeFound);
    }
  }

  @Input() loadingOptionsByExpiration: false;
  @Input() loadingOptionsByStrike: false;
  
  @Output() symbolSelectedEmmiter: EventEmitter<GetLastQuote> = new EventEmitter<GetLastQuote>();

  
  public expirationsList: SelectItem[] = [];
  public strikesList: SelectItem[] = [];
  public strikesTable: OptionStrike[];
  public expirationsTable: OptionExpiry[] = [];

  public selectedStrike: number = 0;
  public selectedExpiration = '';

  public selectedType: string = 'Both';

  public putCall: SelectItem[] = [
    { label: 'Call', value: 'C' },
    { label: 'Put', value: 'P' },
    { label: 'Both', value: 'Both' }
  ];

  public presentationType: SelectItem[] = [
    { label: 'By expiration', value: 1 },
    { label: 'By strikes', value: 2 },
  ];

  public selectedpresentationType = 1;


  constructor() { }

  ngOnInit() {
  }

  onExpirationChange(event) {
    this.strikesTable = this._optionsByExpirationFound.filter(expiry => expiry.Expiration == event.value)[0].Strikes;
  }

  onStrikeChange(event) {
    this.expirationsTable = this._optionsByStrikeFound.filter(opt => opt.Strike == event.value)[0].Expirations;
  }

  optionSelected(symbol: string) {
    let result: GetLastQuote = new GetLastQuote();
    result.market = 'US';
    result.typeofsymbol = 'option';
    result.symbol = symbol;
    this.symbolSelectedEmmiter.emit(result);
  }


  formatByExpiration(symbols: OptionSymbolByExpiration[]): void {
    this.expirationsList = symbols.map(expiry => {
      // Dropdown does not work with Date, probably because a localized string representation
      //const dt: Date = new Date(expiry.Expiration);
      //return ({ label: dt.toDateString(), value: dt });
      const dt: string = expiry.Expiration.split("T")[0];
      return ({ label: dt, value: expiry.Expiration });
    });

    if (this.expirationsList.length > 0) {
      this.selectedExpiration = this.expirationsList[Math.floor(this.expirationsList.length / 2)].value;
      this.strikesTable = this._optionsByExpirationFound.filter(expiry => expiry.Expiration == this.selectedExpiration)[0].Strikes;
    } else {
      this.strikesTable = [];
    }
  }


  formatByStrike(symbols: OptionSymbolByStrike[]): void {
    this.strikesList = symbols.map(opt => {
      return ({ label: `Strike ${opt.Strike}`, value: opt.Strike });
    });

    if (this.strikesList.length > 0) {
      this.selectedStrike = this.strikesList[Math.floor(this.strikesList.length / 2)].value;
      this.expirationsTable = this._optionsByStrikeFound.filter(opt => opt.Strike == this.selectedStrike)[0].Expirations;
    } else {
      this.expirationsTable = [];
    }
  }
}
