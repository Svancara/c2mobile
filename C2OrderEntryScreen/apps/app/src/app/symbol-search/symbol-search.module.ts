import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { PrimeNgModule  } from '@app/primeng.module';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SharedModule } from '@app/shared';


import { SearchService } from '@app/core/services/symbol-search.service';
import { SymbolSearchEffects } from '@app/store/effects/symbol-search.effects';
import { reducers as symbolSearchReducers } from '@app/store/reducers/symbol-search/';

import { OptionSearchByExpirationsEffects } from '@app/store/effects/search-option-by-expiration.effects';
import { reducer as optionsByExpirationsReducers } from '@app/store/reducers/search-option-by-expiration.reducer';

import { OptionSearchByStrikeEffects } from '@app/store/effects/search-option-by-strike.effects';
import { reducer as optionsByStrikeReducers} from '@app/store/reducers/search-option-by-strike.reducer';

import { ForexSymbolsService } from '@app/core/services/forex-symbols.service';
import { ForexSymbolsEffects } from '@app/store/effects/search-forex.effects';
import { reducer as forexSymbolsReducers } from '@app/store/reducers/search-forex.reducer';

import { SymbolSearchSandboxService } from './symbol-search-sandbox.service';

import { SymbolSearchComponent } from './components/symbol-search.component';
import { SymbolSearchHeaderComponent } from './components/header/header.component';
import { SymbolSearchContainerComponent } from './containers/symbol-search-container.component';
import { SymbolSearchRoutingModule } from './symbol-search-routing.module';

import { StocksSearchBoxComponent } from './components/stocks-search-box/stocks-search-box.component';
import { ForexSearchBoxComponent } from './components/forex-search-box/forex-search-box.component';
import { OptionsSearchBoxComponent } from './components/options-search-box/options-search-box.component';
import { OptionsResultComponent } from './components/options-result/options-result.component';
import { StocksResultComponent } from './components/stocks-result/stocks-result.component';
import { ForexResultComponent } from './components/forex-result/forex-result.component';


@NgModule({
  imports: [
    CommonModule,
    PrimeNgModule,
    SharedModule,
    SymbolSearchRoutingModule,

    /**
    * StoreModule.forFeature is used for composing state
    * from feature modules. These modules can be loaded
    * eagerly or lazily and will be dynamically added to
    * the existing state.
    */
    StoreModule.forFeature('symbolSearch', symbolSearchReducers),
    StoreModule.forFeature('optionsByExpiration', optionsByExpirationsReducers),
    StoreModule.forFeature('optionsByStrike', optionsByStrikeReducers),
    StoreModule.forFeature('forexSymbols', forexSymbolsReducers),
   

    /**
     * Effects.forFeature is used to register effects
     * from feature modules. Effects can be loaded
     * eagerly or lazily and will be started immediately.
     *
     * All Effects will only be instantiated once regardless of
     * whether they are registered once or multiple times.
     */
    EffectsModule.forFeature([
      ForexSymbolsEffects,
      OptionSearchByExpirationsEffects,
      OptionSearchByStrikeEffects,
      SymbolSearchEffects,
    ]),
  ],
  declarations: [
    ForexResultComponent,
    SymbolSearchContainerComponent,
    SymbolSearchComponent,
    SymbolSearchHeaderComponent,
    StocksSearchBoxComponent,
    ForexSearchBoxComponent,
    OptionsSearchBoxComponent,
    OptionsResultComponent,
    StocksResultComponent
  ],
  providers: [
    ForexSymbolsService,
    SearchService,
    SymbolSearchSandboxService],
  exports: [
    SymbolSearchContainerComponent,
    StocksSearchBoxComponent,
    StocksResultComponent
  ]
})
export class SymbolSearchModule { }
