import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { reducer as fundamentalDataReducer } from '@app/store/reducers/fundamental-data.reducer';
import { FundamentalDataEffects } from '@app/store/effects/fundamental-data.effects';
import { FundamentalDataService } from '@app/core/services/fundamental-data-feed.service';

import { reducer as lastQuotesReducer } from '@app/store/reducers/last-quote.reducer';
import { LastQuotesEffects } from '@app/store/effects/last-quotes.effects';
import { LastQuotesFeedService } from '@app/core/services/last-quotes-feed.service';


@NgModule({
  imports: [
    CommonModule,

    /**
   * StoreModule.forFeature is used for composing state
   * from feature modules. These modules can be loaded
   * eagerly or lazily and will be dynamically added to
   * the existing state.
   */
    StoreModule.forFeature('lastQuoteData', lastQuotesReducer),
    StoreModule.forFeature('fundamentalData', fundamentalDataReducer),
    

    /**
     * Effects.forFeature is used to register effects
     * from feature modules. Effects can be loaded
     * eagerly or lazily and will be started immediately.
     *
     * All Effects will only be instantiated once regardless of
     * whether they are registered once or multiple times.
     */
    EffectsModule.forFeature([LastQuotesEffects]),
    EffectsModule.forFeature([FundamentalDataEffects]),

  ],
  declarations: [],
  providers: [
    FundamentalDataService,
    LastQuotesFeedService
  ]
})
export class QuotesFeedsModule{ }
