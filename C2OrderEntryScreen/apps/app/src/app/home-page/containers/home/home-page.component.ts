import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromAuth from '@app/store/reducers/auth';
import { Observable } from 'rxjs/Observable';
import { ILoginOK } from '@app/models';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  public userInfo$: Observable<ILoginOK>;

  constructor(private store: Store<fromAuth.State>) {
    this.userInfo$ = this.store.select(fromAuth.getUser);
  }

  ngOnInit() {
  }

}
