import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrimeNgModule } from '@app//primeng.module';

import { HomeComponent } from './home/home.component';

import { SharedModule } from '@app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    PrimeNgModule,
    SharedModule,
  ],
  declarations: [HomeComponent],
  exports: [HomeComponent]
})
export class ComponentsModule { }
