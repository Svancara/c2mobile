import { Component, OnInit, Input } from '@angular/core';

import { LocalStorageService } from 'angular-2-local-storage';
import { MenuItem } from 'primeng/primeng';
import { changeTheme } from '@app/utility/changeTheme';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  @Input() userId: number;

  constructor(private localStorage: LocalStorageService) { }

  ngOnInit() {
  }

  settings() {
    alert("Not yet implemented");
  }

  menuItems: MenuItem[] = [
    {
      label: 'Themes',
      items: [
        { command: (event) => changeTheme(event, 'omega', this.localStorage), label: "Omega" },
        { command: (event) => changeTheme(event, 'bootstrap', this.localStorage), label: "Bootstrap" },
        { command: (event) => changeTheme(event, 'cupertino', this.localStorage), label: "Cupertino" },
        { command: (event) => changeTheme(event, 'cruze', this.localStorage), label: "Cruze" },
        { command: (event) => changeTheme(event, 'darkness', this.localStorage), label: "Darkness" },
        { command: (event) => changeTheme(event, 'flick', this.localStorage), label: "Flick" },
        { command: (event) => changeTheme(event, 'home', this.localStorage), label: "Home" },
        { command: (event) => changeTheme(event, 'kasper', this.localStorage), label: "Kasper" },
        { command: (event) => changeTheme(event, 'lightness', this.localStorage), label: "Lightness" },
        { command: (event) => changeTheme(event, 'ludvig', this.localStorage), label: "Ludvig" },
        { command: (event) => changeTheme(event, 'pepper-grinder', this.localStorage), label: "Pepper-Grinder" },
        { command: (event) => changeTheme(event, 'redmond', this.localStorage), label: "Redmond" },
        { command: (event) => changeTheme(event, 'rocket', this.localStorage), label: "Rocket" },
        { command: (event) => changeTheme(event, 'south-street', this.localStorage), label: "South-Street" },
        { command: (event) => changeTheme(event, 'start', this.localStorage), label: "Start" },
        { command: (event) => changeTheme(event, 'trontastic', this.localStorage), label: "Trontastic" },
        { command: (event) => changeTheme(event, 'voclain', this.localStorage), label: "Voclain" },
      ]
    }];

 

}
