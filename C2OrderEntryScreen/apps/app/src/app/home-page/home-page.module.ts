import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';

import { ComponentsModule } from './components/home-page.components.module';
import { PrimeNgModule } from '../primeng.module';
import { SharedModule } from '@app/shared/shared.module';

import { HomePageComponent } from './containers/home/home-page.component';
// import { ComponentsModule } from './components/home-page.components.module';


@NgModule({
  imports: [
    CommonModule,
    PrimeNgModule,
    ComponentsModule,
    SharedModule,
    RouterModule.forChild([
      { path: 'home', component: HomePageComponent},
      { path: '', component: HomePageComponent},
    ]),

  ],
  declarations: [
    HomePageComponent],
  exports: []
})
export class HomePageModule { }

