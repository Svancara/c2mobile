// Route names
export const HomeRoute = "home";
export const UserStrategyRoute = "user-strategy";

// Locla Storage Root Key
export const C2MOBILE = "C2MOBILE";
export const SIGNALS_LOG = "SIGNALS_LOG";
export const API_KEY = "API_KEY";

export const LAYOUT = "LAYOUT";
