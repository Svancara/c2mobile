import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { InstrumentDetailComponent } from './instrument-detail/instrument-detail.component';
import { InstrumentsListComponent } from './instruments-list/instruments-list.component';

import { PrimeNgModule } from '../../primeng.module';

export const COMPONENTS = [
  InstrumentDetailComponent,
  InstrumentsListComponent
];

@NgModule({
  imports: [
    CommonModule,
    PrimeNgModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
})
export class ComponentsModule { }
