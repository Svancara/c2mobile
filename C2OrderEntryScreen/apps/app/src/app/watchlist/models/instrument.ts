export interface Instrument {
  symbol: string,
  name : string,
  // Let's have it for ngrx Entities
  id: string, 
}
