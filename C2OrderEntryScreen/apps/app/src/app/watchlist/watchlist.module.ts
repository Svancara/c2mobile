  import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { ComponentsModule } from './components/components.module';

import { InstrumentDetailsPageComponent } from './containers/instrument-details-page/instrument-details-page.component';
import { InstrumentsListPageComponent } from './containers/instruments-list-page/instruments-list-page.component';

import { PrimeNgModule } from '../primeng.module';

import { reducers } from './reducers';

@NgModule({
  imports: [
    CommonModule,
    PrimeNgModule,
    ComponentsModule,
    RouterModule.forChild([
      { path: 'watchlist', component: InstrumentsListPageComponent},
      {
        path: ':id',
        component: InstrumentsListPageComponent,
      },
      { path: '', component: InstrumentsListPageComponent },
    ]),

    /**
     * StoreModule.forFeature is used for composing state
     * from feature modules. These modules can be loaded
     * eagerly or lazily and will be dynamically added to
     * the existing state.
     */
    StoreModule.forFeature('watchlist', reducers),

    /**
     * Effects.forFeature is used to register effects
     * from feature modules. Effects can be loaded
     * eagerly or lazily and will be started immediately.
     *
     * All Effects will only be instantiated once regardless of
     * whether they are registered once or multiple times.
     */
    EffectsModule.forFeature([
      //  BookEffects, CollectionEffects
    ]),


  ],
  declarations: [
    InstrumentDetailsPageComponent,
    InstrumentsListPageComponent,
    ]
})
export class WatchlistModule { }
