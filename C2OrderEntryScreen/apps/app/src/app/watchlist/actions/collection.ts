import { Action } from '@ngrx/store';
import { Instrument } from '../models/instrument';

export const ADD_INSTRUMENT = '[Collection] Add Instrument';
export const ADD_INSTRUMENT_SUCCESS = '[Collection] Add Instrument Success';
export const ADD_INSTRUMENT_FAIL = '[Collection] Add Instrument Fail';
export const REMOVE_INSTRUMENT = '[Collection] Remove Instrument';
export const REMOVE_INSTRUMENT_SUCCESS = '[Collection] Remove Instrument Success';
export const REMOVE_INSTRUMENT_FAIL = '[Collection] Remove Instrument Fail';
export const LOAD = '[Collection] Load';
export const LOAD_SUCCESS = '[Collection] Load Success';
export const LOAD_FAIL = '[Collection] Load Fail';

/**
 * Add Instrument to Collection Actions
 */
export class AddInstrument implements Action {
  readonly type = ADD_INSTRUMENT;

  constructor(public payload: Instrument) { }
}

export class AddInstrumentSuccess implements Action {
  readonly type = ADD_INSTRUMENT_SUCCESS;

  constructor(public payload: Instrument) { }
}

export class AddInstrumentFail implements Action {
  readonly type = ADD_INSTRUMENT_FAIL;

  constructor(public payload: Instrument) { }
}

/**
 * Remove Instrument from Collection Actions
 */
export class RemoveInstrument implements Action {
  readonly type = REMOVE_INSTRUMENT;

  constructor(public payload: Instrument) { }
}

export class RemoveInstrumentSuccess implements Action {
  readonly type = REMOVE_INSTRUMENT_SUCCESS;

  constructor(public payload: Instrument) { }
}

export class RemoveInstrumentFail implements Action {
  readonly type = REMOVE_INSTRUMENT_FAIL;

  constructor(public payload: Instrument) { }
}

/**
 * Load Collection Actions
 */
export class Load implements Action {
  readonly type = LOAD;
}

export class LoadSuccess implements Action {
  readonly type = LOAD_SUCCESS;

  constructor(public payload: Instrument[]) { }
}

export class LoadFail implements Action {
  readonly type = LOAD_FAIL;

  constructor(public payload: any) { }
}

export type Actions =
  | AddInstrument
  | AddInstrumentSuccess
  | AddInstrumentFail
  | RemoveInstrument
  | RemoveInstrumentSuccess
  | RemoveInstrumentFail
  | Load
  | LoadSuccess
  | LoadFail;
