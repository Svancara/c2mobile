import { Injectable, OnDestroy } from "@angular/core";
import { Store } from '@ngrx/store';
import { Subscription } from "rxjs";
import { Sandbox } from '../shared/sandbox/base.sandbox';
// import { UserStrategiesApiClient } from './user-strategies-ApiClient.service';

import *  as fromStore from '@app/store/reducers/user-strategies';
// import * as fromStore from '@app/store';
import { getAllStrategies, getCollectionLoading } from '@app/store/reducers/user-strategies';
import * as productsActions from '@app/store/actions/user-strategies.collection.actions';
import * as productDetailsActions from '@app/store/actions/user-strategy.actions';
import { IStrategy, ILoginOK } from '@app/models';
// import { Load } from '@app/store/actions/user-strategies.collection.actions';
// import { Select } from '@app/store/actions/user-strategy.actions';


@Injectable()
export class UserStrategiesSandbox extends Sandbox implements OnDestroy {

  public strategies$ = this.store.select(getAllStrategies);
  public loading$ = this.store.select(getCollectionLoading);

  constructor(private store: Store<any>) {
    super(store);
    this.registerEvents();
  }

  //ngOnInit() {
  //  this.store.dispatch(new Load());
  //}

  ngOnDestroy() {
    this.unregisterEvents();
  }

  showSelectedStrategy(strategy: IStrategy) {
    this.store.dispatch(new productDetailsActions.Select(strategy.id));
  }

  private subscriptions: Array<Subscription> = [];

  /*
    public products$ = this.appState$.select(store.getStrategiesEntitiesState);
    public productsLoading$ = this.appState$.select(store.getStrategiesLoading);
    public productDetails$ = this.appState$.select(store.getSelectedStrategy);
    //public productDetailsLoading$ = this.appState$.select(store.);
    public loggedUser$            = this.appState$.select(store.getLoggedUser);
  
  
    constructor(
      protected appState$: Store<store.State>,
      private productsApiClient: UserStrategiesApiClient
    ) {
      super(appState$);
      this.registerEvents();
    }
    */

  /**
   * Loads products from the server
   */
  public loadProducts(): void {
    this.store$.dispatch(new productsActions.Load())
  }

  /**
   * Loads product details from the server
   */
  //public loadProductDetails(id: number): void {
  //  this.appState$.dispatch(new productDetailsActions.LoadAction(id))
  //}

  /**
   * Dispatches an action to select product details
   */
  //public selectProduct(product: IStrategy): void {
  //  this.appState$.dispatch(new productDetailsActions.Select(product))
  //}

  /**
   * Unsubscribes from events
   */
  public unregisterEvents() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  /**
   * Subscribes to events
   */
  private registerEvents(): void {
    // Subscribes to culture
    this.subscriptions.push(this.culture$.subscribe((culture: string) => this.culture = culture));

    this.subscriptions.push(this.loggedUser$.subscribe((user: ILoginOK) => {
      if (user && user.APIkey) {
        this.loadProducts();
      }
    }));
  }
}
