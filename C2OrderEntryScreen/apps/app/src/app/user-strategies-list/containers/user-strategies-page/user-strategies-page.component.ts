import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { getAllStrategies, State, getCollectionLoading } from '@app/store/reducers/user-strategies';
import { Load } from '@app/store/actions/user-strategies.collection.actions';
import { Select } from '@app/store/actions/user-strategy.actions';
import { Strategy, IStrategy } from '@app/models';
import { UserStrategyRoute } from '@app/globals';
import { Router } from '@angular/router';
import { UserStrategiesSandbox } from '@app/user-strategies-list/user-strategies.sandbox';

@Component({
  selector: 'app-user-strategies-page',
  templateUrl: './user-strategies-page.component.html',
  styleUrls: ['./user-strategies-page.component.css']
})
export class UserStrategiesPageComponent {
  /*
  public strategies$: Observable<IStrategy[]>;
  public loading$: Observable<boolean>;

  constructor(private store: Store<State>) {
    this.strategies$ = this.store.select(getAllStrategies);
    this.loading$ = this.store.select(getCollectionLoading);
  }

  ngOnInit() {
    this.store.dispatch(new Load());
  }

  showSelectedStrategy(strategy: IStrategy) {
    this.store.dispatch(new Select(strategy.id));
  }
*/

  constructor(
    private router: Router,
    public strategiesSandbox: UserStrategiesSandbox) { }

  showSelectedStrategy(strategy: IStrategy) {
    this.strategiesSandbox.showSelectedStrategy(strategy);
  }

  /**
   * Callback function for grid select event
   * 
   * @param selected
   */
  //public onSelect({ selected }): void {
  //  this.productsSandbox.productDetails$(selected[0]);
  //  this.router.navigate(['/' + UserStrategyRoute, selected[0].id]);
  //}


}
