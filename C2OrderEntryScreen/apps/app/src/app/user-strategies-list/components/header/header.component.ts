import { Component, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-my-strategies-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MyStrategiesHeaderComponent {

  @Output() settingsEvent = new EventEmitter();

  settings() {
    this.settingsEvent.emit();
  }

}
