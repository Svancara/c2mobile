import { Component, Input, ChangeDetectionStrategy, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { IStrategy, Strategy } from '@app/models';
import { Store } from '@ngrx/store';
import * as fromOrderScreenStrategy from '@app/store/actions/order-screen-strategy.actions';

@Component({
  selector: 'app-user-strategies',
  templateUrl: './user-strategies.component.html',
  styleUrls: ['./user-strategies.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserStrategiesComponent implements OnInit {

  @Input() strategies: IStrategy[] = [];
  @Input() loading: false;
  @Output() strategySelected = new EventEmitter<IStrategy>();

  public displayDialog = false;
  public selectedForDialog: IStrategy = undefined;
  public selectedStrategy: IStrategy = undefined;

  constructor(protected store$: Store<any>,private router: Router, private actRoute: ActivatedRoute) { }


  ngOnInit() {
  }

  selectStrategy(strategyId:number, strategyName:string) {
    let selectedStrategy: Strategy = new Strategy();
    selectedStrategy.system_id = strategyId;
    selectedStrategy.systemName = strategyName;
    this.store$.dispatch(new fromOrderScreenStrategy.AddOrderScreenStrategy(new Strategy(selectedStrategy)));
    this.router.navigate(['/user-strategy', strategyId]);
  }

  showDialog(selected: IStrategy) {
    this.displayDialog = true;
    this.selectedForDialog = selected;
  }

  closeDialog(selected: IStrategy) {
    this.displayDialog = false;
  }

}
