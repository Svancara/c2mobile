import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//import { ReactiveFormsModule } from '@angular/forms';
//import { RouterModule } from '@angular/router';

import { MyStrategiesHeaderComponent } from './header/header.component';
import { UserStrategiesComponent } from './user-strategies/user-strategies.component';

import { PrimeNgModule } from '../../primeng.module';

export const COMPONENTS = [
  MyStrategiesHeaderComponent,
  UserStrategiesComponent
];

@NgModule({
  imports: [
    CommonModule,
    PrimeNgModule,
    //ReactiveFormsModule,
    //RouterModule,
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
})
export class ComponentsModule { }
