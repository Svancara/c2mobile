/*


This does not work. gridAdapter is not called from UserStrategiesApiClient


*/










import {
  Injectable,
  Inject,
  forwardRef
} from '@angular/core';
import { IStrategy, Strategy} from '@app/models';
//import { ProductsSandbox } from './products.sandbox';

@Injectable()
export class UserStrategiesService {

  //private productsSubscription;

  /**
   * Transforms grid data products recieved from the API into array of 'Product' instances
   *
   * @param products
   */
  static gridAdapter(products:any): Array<IStrategy> {
    return products.map(product => {
      //console.log("UserStrategiesService gridAdapter", product);
      return new Strategy(product);
    });

  }

  /**
   * Transforms product details recieved from the API into instance of 'Product'
   *
   * @param product
   */
  static productDetailsAdapter(product: IStrategy): IStrategy {
    return new Strategy(product);
  }
}
