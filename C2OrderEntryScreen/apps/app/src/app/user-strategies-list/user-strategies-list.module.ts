import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';


import { UserStrategiesSandbox } from '@app/user-strategies-list/user-strategies.sandbox';
import { ComponentsModule } from './components/components.module';

import { UserStrategiesPageComponent } from './containers/user-strategies-page/user-strategies-page.component';
// import { UserStrategiesApiClient } from './user-strategies-ApiClient.service';

import { PrimeNgModule } from '@app/primeng.module';

import { reducers } from '@app/store/reducers/user-strategies';
import { UserStragiesEffects } from '@app/store/effects/user-strategies.effects';
// import { SignalsService } from '@app/core/services/signals.service';

@NgModule({
  imports: [
    CommonModule,
    PrimeNgModule,
    ComponentsModule,
    RouterModule.forChild([
      { path: '', redirectTo: 'strategies-list', pathMatch: 'full' },
      { path: 'strategies-list', component: UserStrategiesPageComponent },
    ]),

    /**
     * StoreModule.forFeature is used for composing state
     * from feature modules. These modules can be loaded
     * eagerly or lazily and will be dynamically added to
     * the existing state.
     */
    StoreModule.forFeature('userStrategies', reducers),

    /**
     * Effects.forFeature is used to register effects
     * from feature modules. Effects can be loaded
     * eagerly or lazily and will be started immediately.
     *
     * All Effects will only be instantiated once regardless of
     * whether they are registered once or multiple times.
     */
    EffectsModule.forFeature([
      UserStragiesEffects
    ]),
  ],
  declarations: [
    UserStrategiesPageComponent,
],
  providers: [
   // UserStrategiesApiClient,
    UserStrategiesSandbox
    ]

})
export class UserStrategiesListModule { }
