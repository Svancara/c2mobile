/*


This does not work. gridAdapter is not called from UserStrategiesApiClient


*/




import { Injectable } from '@angular/core';
import {
  HttpService,
  POST,
  Body,
  DefaultHeaders,
  Adapter
} from '@app/shared/http';

// import { UserStrategiesSandbox } from './user-strategies.sandbox';

import { Observable } from 'rxjs/Observable';
import { UserStrategiesService } from './user-strategies.service';
import { UserStrategies } from '@app/models';

@Injectable()
@DefaultHeaders({
  'Accept': 'application/json',
  'Content-Type': 'application/json'
})
export class UserStrategiesApiClient extends HttpService {

  /**
   * Retrieves all products
   */
  @POST("/listAllSystems")
  @Adapter(UserStrategiesService.gridAdapter)
  public getProducts( @Body { apikey: string }): Observable<UserStrategies> {
    //console.log("---------------- UserStrategiesApiClient getProducts");
    return null;
  };

  /**
   * Retrieves product details by a given id
   * 
   * @param id
   */
  @POST("/listAllSystems/{id}___________this is just for education")
  @Adapter(UserStrategiesService.productDetailsAdapter)
  public getProductDetails( @Body id: number): Observable<any> { return null; };
}
