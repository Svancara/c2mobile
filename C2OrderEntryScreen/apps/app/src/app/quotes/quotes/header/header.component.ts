import { Component, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-quotes-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuotesHeaderComponent {

  @Output() settingsEvent = new EventEmitter();

  settings() {
    this.settingsEvent.emit();
  }

}
