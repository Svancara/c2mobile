import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrimeNgModule } from '@app/primeng.module';

import { QuotesComponent } from './quotes.component';
import { QuotesHeaderComponent } from './header/header.component';

import { SharedModule } from '@app/shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    PrimeNgModule,
    SharedModule ,
  ],
  declarations: [QuotesComponent, QuotesHeaderComponent],
  exports: [QuotesComponent, QuotesHeaderComponent],
})
export class QuotesComponentsModule { }
