import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { QuotesSandboxService } from '@app/quotes/quotes-sandbox.service';
import { GetLastQuote, LastQuoteData, InstrumentString, InternaSearchSymbolRequest, UnknownInstrument, FundamentalDataResponse } from '@app/models';
import { Observable } from 'rxjs/Observable';
import { Utils } from '@app/utils';
import { SymbolSearchTypeAheadSupport } from '@app/models/LastQuoteService.dtos';


@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class QuotesComponent {


  @Input()
  set lastQuote(value: LastQuoteData) {
    if (value) {
      this._lastQuote = value;
      this.symbol = value.symbol;
    }
  }
  get lastQuote(): LastQuoteData {
    return this._lastQuote
  }

  @Input() fundamentalData: FundamentalDataResponse;

  @Output() symbolSelectedEmmiter: EventEmitter<GetLastQuote> = new EventEmitter<GetLastQuote>();
  @Output() symbolSearchEmmiter: EventEmitter<InternaSearchSymbolRequest> = new EventEmitter<InternaSearchSymbolRequest>();

  public symbol: string = "";
  public market: string = "US";
  public instrument: string = UnknownInstrument;

  private _lastQuote: LastQuoteData;

  private instrumentForSearchScreen(): InstrumentString {
    if (this._lastQuote && this._lastQuote.typeofsymbol) {
      let t: InstrumentString;
      switch (this._lastQuote.typeofsymbol) {
        case InstrumentString.stock:
        case InstrumentString.forex:
        case InstrumentString.future:
        case InstrumentString.option:
        case InstrumentString.mutual:
          t = this._lastQuote.typeofsymbol;
          break;
        default: t = InstrumentString.stock;
          break;
      };
      return t;
    }

    return InstrumentString.stock;
  }

  sendRequest() {
    if (!this.symbol) {
      return;
    }
    if (this.symbol.trim()) {
      let request: GetLastQuote = new GetLastQuote();
      request.market = '';
      request.typeofsymbol = UnknownInstrument;
      request.symbol = Utils.prepSymbol(this.symbol); // this.searchedSymbol$.Symbol;
      this.symbolSelectedEmmiter.emit(request);
    }
  }

  searchsymbol() {
    // Send the  provided text to Search
    const request: InternaSearchSymbolRequest = {
      query: this.symbol,
      returnToRoute: ["/quotes"],
      instrument: this.instrumentForSearchScreen()
    };
    this.symbolSearchEmmiter.emit(request);
  }

  createRequest(): GetLastQuote {
    let result: GetLastQuote = new GetLastQuote();
    result.market = this.market;
    result.typeofsymbol = Utils.prepInstrument(this.instrument);
    result.symbol = Utils.prepSymbol(this.symbol); // this.searchedSymbol$.Symbol;
    return result;
  }


  settings() {
    alert("QuotesComponent says: Not yet implemented");
  }



}
