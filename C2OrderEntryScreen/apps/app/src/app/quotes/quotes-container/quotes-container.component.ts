import { Component } from '@angular/core';
import { QuotesSandboxService } from '@app/quotes/quotes-sandbox.service';
import { Observable } from 'rxjs/Observable';
import { LastQuoteData, GetLastQuote, InternaSearchSymbolRequest } from '@app/models';
import * as fromLastQuote from '@app/store/reducers/last-quote.reducer';
import { Subscription } from 'rxjs/Subscription';

import { Map } from 'immutable';

@Component({
  selector: 'app-quotes-container',
  templateUrl: './quotes-container.component.html',
  styleUrls: ['./quotes-container.component.css']
})
export class QuotesContainerComponent {

  constructor(public sandbox$: QuotesSandboxService) {
  }

  /**
   * Show Symbols Search Screen.
   * Pass already written text to it.
   * Symbols Search Screen returns a symbol and we call getLastQuote for it.
   * @param query
   */
  symbolSearch(query: InternaSearchSymbolRequest ) {
    this.sandbox$.symbolSearch(query);
  }

  /**
   * A user clicked "Data" in the QuotesComponent.
   * @param request
   */
  symbolSelected(request: GetLastQuote) {
    this.sandbox$.getLastQuote(request);
  }

}
