import { Injectable, OnDestroy } from "@angular/core";
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from "rxjs";
import { Sandbox } from '@app/shared/sandbox/base.sandbox';
import { Map } from 'immutable';

import { SymbolSearchTypeAheadSupport, ForexSymbol } from '@app/models/LastQuoteService.dtos';
import { GetLastQuote, LastQuoteData, FundamentalDataResponse, GetFundamentalData, InternaSearchSymbolRequest, InstrumentString } from '@app/models';

import * as fromStoreReducers from '@app/store';
import * as fromGpActions from '@app/store/actions/gp.action';
import * as fromSearchSymbol from '@app/store/reducers/symbol-search';
import * as fromForex from '@app/store/reducers/search-forex.reducer';

import { LoadData as getLastQuote } from '@app/store/actions/last-quote.actions';
import { LoadData as getFundamentalData } from '@app/store/actions/fundamental-data.actions';

import { Search } from '@app/store/actions/symbol-search/symbol-search';


@Injectable()
export class QuotesSandboxService extends Sandbox implements OnDestroy {

  public lastQuotes$: Observable<LastQuoteData>;
  public fundamentalData$: Observable<FundamentalDataResponse>;
  private lastQuoteRequest: GetLastQuote;

  private subscription1: Subscription;
  private subscription2: Subscription;
  private subscription3: Subscription;

  constructor(
    private store: Store<fromStoreReducers.State>,
    private route: ActivatedRoute,
    private router: Router) {
    super(store);

    /*
       ====================================================================
         Inner sandbox behaviour.
         When we receive a message from symbols searching mechanism,
         we catch it and ask for lastQuote and fundamental data.
         It is all we need for the Quotes screen so those data are flowing
         to container (subscribed via async in template) and component.
       ====================================================================
    */

    // React to stock, futures, mutals symbol selected. Ask for last quote and fundamental
    this.subscription1 = this.store
      .select(fromSearchSymbol.getSelectedSymbol).
      subscribe((selectedSymbol: SymbolSearchTypeAheadSupport) => {
        if (selectedSymbol) {
          this.getLastQuote(this.createRequest(selectedSymbol));
        }
      });


    // React to option symbol selected. Ask for last quote and fundamental
    this.subscription2 = this.store
      .select(fromStoreReducers.getSelectedOption)
      .subscribe((selectedSymbol: GetLastQuote) => {
        if (selectedSymbol) {
          this.getLastQuote(selectedSymbol);
        }
      });

    // React to forex symbol selected. Ask for last quote and fundamental
    this.subscription3 = this.store
      .select(fromForex.getSelectedForexSymbol)
      .subscribe((selectedSymbol: GetLastQuote) => {
        if (selectedSymbol) {
          this.getLastQuote(this.createRequestFromForex(selectedSymbol));
        }
      });

    /*
       ====================================================================
        Public behaviour
       ====================================================================
    */

    // React to getLast returning data
    this.lastQuotes$ = this.store.select(fromStoreReducers.getLastQuote).map(
      //(data: Map<string, LastQuoteData>) => {
      //  if (this.lastQuoteRequest) {
      //    return data.get(this.lastQuoteRequest.symbol);
      //  }
      //});
      (data: LastQuoteData) => data);

    // React to getFundamental returning data
    //this.fundamentalData$ = this.store.select(fromStoreReducers.getFundamentalData).map(
    //  (data: Map<string, FundamentalDataResponse>) => {
    //    if (this.lastQuoteRequest) {
    //      return data.get(this.lastQuoteRequest.symbol);
    //    }
    //  }).share();
    this.fundamentalData$ = this.store.select(fromStoreReducers.getFundamentalData).map(
      (data: FundamentalDataResponse) => data).share();
  }

  ngOnDestroy() {
    this.subscription1.unsubscribe();
    this.subscription2.unsubscribe();
    this.subscription3.unsubscribe();
  }

  createRequest(selectedSymbol: SymbolSearchTypeAheadSupport): GetLastQuote {
    let result: GetLastQuote = new GetLastQuote();
    result.market = selectedSymbol.Exchange;
    result.typeofsymbol = selectedSymbol.Instrument;
    result.symbol = selectedSymbol.Symbol;
    return result;
  }

  createRequestFromForex(selectedSymbol: GetLastQuote): GetLastQuote {
    //let result: GetLastQuote = new GetLastQuote();
    //result.market = "US";
    //result.typeofsymbol = InstrumentString.forex;
    //result.symbol = selectedSymbol.Symbol;
    //return result;
    return selectedSymbol;
  }

  /**
   * Ask For Last Quote 
   */
  public getLastQuote(request: GetLastQuote): void {
    this.lastQuoteRequest = request;
    this.store.dispatch(new getLastQuote(request));
    this.store.dispatch(new getFundamentalData(request));
  }

  /**
   * Search for symbol
   * @param request
   */
  symbolSearch(query: InternaSearchSymbolRequest) {
    this.store.dispatch(new fromGpActions.SetSearchSymbolPartialQuery(query));
    this.router.navigate(['/searchsymbol']);
    //this.router.navigate(["/searchsymbol"], { queryParams: { returnUrl: this.router.url } });
  }

}
