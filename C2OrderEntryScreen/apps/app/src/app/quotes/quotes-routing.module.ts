import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuotesContainerComponent } from './quotes-container/quotes-container.component';

const routes: Routes = [
  //{ path: 'positions', component: PositionsRoutePageComponent },
  //{
  //  path: ':id',
  //  component: PositionsRoutePageComponent,
  //},
  { path: 'quotes', component: QuotesContainerComponent},
  {
    path: ':id',
    component: QuotesContainerComponent,
  },
  { path: '', component: QuotesContainerComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuotesRoutingModule { }
