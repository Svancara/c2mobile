/*

   Quotes Screen

*/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrimeNgModule } from '../primeng.module';

import { QuotesComponentsModule } from './quotes/components.module';
import { QuotesContainerComponent } from './quotes-container/quotes-container.component';
import { QuotesRoutingModule } from './quotes-routing.module';
import { QuotesSandboxService } from '@app/quotes/quotes-sandbox.service';

import { QuotesFeedsModule} from '@app/quotes-feeds/quotes-feeds.module';
import { SymbolSearchModule } from '@app/symbol-search/symbol-search.module';


@NgModule({
  imports: [
    CommonModule,
    PrimeNgModule,
    QuotesComponentsModule,
    QuotesFeedsModule,
    QuotesRoutingModule,
    SymbolSearchModule
  ],
  declarations: [QuotesContainerComponent,],
  exports: [QuotesContainerComponent],
  providers: [QuotesSandboxService]
})
export class QuotesModule { }
