// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

import { Environment } from '@app/models';

export const environment: Environment = {
  production: false,
  C2ApiUrl: 'https://api.collective2.com/world/apiv3',
  // C2ApiUrl : "https://bob.collective2.com/world/apiv3",
  // C2ApiUrl: "https://bob.collective2.com/quovo?rm=getLastQuote_api&",
  // https://bob.collective2.com/quovo?rm=getLastQuote_api&apiKey=WbmWnNG_9M3yDHoVKfZ6UKEDEQcz6r3N918C3_tlRCiFJAgi3I&instrument=stock&symbol=MSFT&c2userid=21055024

  //C2LastQuoteApiUrl: "http://bob-home.collective2.com:8043",
  //C2SymbolSearchApiUrl: "http://bob-home.collective2.com:8043",
  //C2FundametalDataApiUrl: 'http://bob-home.collective2.com:8043',
  C2LastQuoteApiUrl: "https://c2nanex.collective2.com:8043",
  C2SymbolSearchApiUrl: "https://c2nanex.collective2.com:8043",
  C2FundametalDataApiUrl: 'https://c2nanex.collective2.com:8043',

  

  C2OrderEntryScreenGlobals: {
    c2UserId: 21055024,
    c2SessionId: '',
    c2Language: 'en',
    c2Apikey: 'WbmWnNG_9M3yDHoVKfZ6UKEDEQcz6r3N918C3_tlRCiFJAgi3I',
    testingStrategyId: 30076808
  }

};
