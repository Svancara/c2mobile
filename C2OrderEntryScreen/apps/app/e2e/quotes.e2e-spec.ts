import { browser, by, element } from 'protractor';
//import { LoginPage } from './quotesPage.po';
import { QuotesPage } from './quotesPage.po';

describe('Quotes Screen', () => {
  let page: QuotesPage;

  beforeEach(() => {
    page = new QuotesPage();
  });

  //it('should display welcome message', () => {
  //  page.navigateTo();
  //  expect(page.getParagraphText()).toEqual('Welcome to app!');
  //});

  it('should find Microsoft', () => {

    
    page.navigateTo();

    //browser.wait(5000);

    let symbolInput = page.getQuotesInput();
    expect(symbolInput).toBeDefined();

    symbolInput.sendKeys("MSFT");

    let button = page.getDataButton();
    button.click();

    let companyName = page.getCompanyName().getText();

    expect(companyName).toEqual("MIC");


  });

});
