import { browser, by, element } from 'protractor';

export class AppPage {

  navigateTo() {
    return browser.get('/');
  }

  //getParagraphText() {
  //  return element(by.css('app-root h1')).getText();
  //}

  // =====================================
  // Login page
  navigateToLogin() {
    return browser.get('/login');
  }

  getLoginPageEmail() {
    let page = this.navigateToLogin();
    return element(by.name('email'));
  }

  // =====================================
  // Quotes screen 
  // http://localhost:4200/quotes
  navigateToQuotes() {
    return browser.get('/quotes');
  }

  getQuotesInput() {
    let page = this.navigateToLogin();
    return element(by.model('symbol'));
  }


  // =====================================
  // Search symbols screen 
  // http://localhost:4200/searchsymbol
  navigateToSearchSymbol() {
    return browser.get('/searchsymbol');
  }



}
