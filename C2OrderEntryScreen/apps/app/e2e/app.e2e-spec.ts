import { AppPage } from './app.po';

xdescribe('c2-order-entry-screen App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  //it('should display welcome message', () => {
  //  page.navigateTo();
  //  expect(page.getParagraphText()).toEqual('Welcome to app!');
  //});

  it('should display login', () => {
    page.navigateTo();
    let email = page.getLoginPageEmail();
    expect(email).toBeDefined();
  });

});
