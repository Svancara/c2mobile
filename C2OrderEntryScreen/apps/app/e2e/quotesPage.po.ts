import { browser, by, element } from 'protractor';

export class QuotesPage {

  // =====================================
  // Quotes screen 
  // http://localhost:4200/quotes
  navigateTo() {
    return browser.get('/quotes');
  }

  getQuotesInput() {
    return element(by.css('input'));
  }

  getDataButton() {
    return element(by.name('submit'));
  }

  getCompanyName() {
    return element(by.className('company-name'));
  }

}
