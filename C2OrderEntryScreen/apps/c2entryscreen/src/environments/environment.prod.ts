import { Environment } from "@app/models/types";

export const environment: Environment = {
  production: true,
  C2ApiUrl : "https://api.collective2.com/world/apiv3",
  // C2ApiUrl: "https://bob.collective2.com/world/apiv3/",

  //C2LastQuoteApiUrl: "http://bob-home.collective2.com:8043",
  //C2SymbolSearchApiUrl: "http://bob-home.collective2.com:8043",
  //C2FundametalDataApiUrl: 'http://bob-home.collective2.com:8043',
  C2LastQuoteApiUrl: "https://c2nanex.collective2.com:8043",
  C2SymbolSearchApiUrl: "https://c2nanex.collective2.com:8043",
  C2FundametalDataApiUrl: 'https://c2nanex.collective2.com:8043',

  //C2OrderEntryScreenGlobals: {
  //  c2UserId: undefined,
  //  c2SessionId: undefined,
  //  c2Language: undefined,
  //  c2Apikey: undefined,
  //}
  C2OrderEntryScreenGlobals: {
    c2UserId: 0,
    c2SessionId: "",
    c2Language: "en",
    c2Apikey: "",
    testingStrategyId: 0
  }

};
