import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { SymbolSearchSandboxService } from '@app/symbol-search/symbol-search-sandbox.service';
import { SymbolSearch, SymbolSearchTypeAheadSupport, OptionSymbolsSearch, OptionSearchByExpiration, ForexSymbol, ForexPairs } from '@app/models/LastQuoteService.dtos';
import { GetLastQuote, InternaSearchSymbolRequest, InstrumentString } from '@app/models';

@Component({
  selector: 'c2scrn-symbol-search-container',
  templateUrl: './symbol-search-container.component.html',
  styleUrls: ['./symbol-search-container.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SymbolSearchContainerComponent implements OnInit {

  @Input() selectedInstrument: InstrumentString = InstrumentString.stock;

  constructor(public sandbox$: SymbolSearchSandboxService) {
  }

  ngOnInit() {
    this.sandbox$.clearResults();
  }

  /**
   * Search stocks, futures,...  But not options.
   * @param query
   */
  symbolSearch(query: SymbolSearch) {
    this.sandbox$.symbolSearch(query);
  }

  optionSearch(query: OptionSearchByExpiration) {
    this.sandbox$.optionSearch(query);
  }

  loadForexData(request: ForexPairs) {
    this.sandbox$.loadForexData(request);
  }

  symbolSelected(data: SymbolSearchTypeAheadSupport) {
    this.sandbox$.symbolSelected(data);
  }


  optionSelected(symbol: GetLastQuote) {
    this.sandbox$.optionSelected(symbol);
  }

  forexSymbolSelected(symbol: GetLastQuote) {
    this.sandbox$.forexSymbolSelected(symbol);
  }

}
