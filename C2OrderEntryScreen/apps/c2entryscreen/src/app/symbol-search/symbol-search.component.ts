import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { SymbolSearch, SymbolSearchTypeAheadSupport, OptionSymbolsSearch, OptionSearchByExpiration, OptionExpiry, OptionCallPut, OptionSymbolByExpiration, OptionSymbolByStrike, ForexSymbol, ForexPairs } from '@app/models/LastQuoteService.dtos';
import { Utils } from '@app/utils';
import { InstrumentString, InternaSearchSymbolRequest, GetLastQuote } from '@app/models';
import { SelectItem } from 'primeng/components/common/selectitem';
import { MenuItem } from 'primeng/components/common/menuitem';
import { NotificationsService } from 'angular2-notifications';
//import { SelectItem } from 'primeng/components/common/selectitem';
//import { MenuItem } from 'primeng/components/common/menuitem';


@Component({
  selector: 'c2scrn-symbol-search-component',
  templateUrl: './symbol-search.component.html',
  styleUrls: ['./symbol-search.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SymbolSearchComponent implements OnInit {

  @Input() internaSearchSymbolRequest: InternaSearchSymbolRequest; // From quotes or Entry Screen
  @Input() selectedInstrument: InstrumentString = InstrumentString.stock;
  @Input() loadingSymbols = false;
  @Input() loadingOptionsByExpiration = false;
  @Input() loadingOptionsByStrike = false;
  @Input() loadingForex = false;

  @Input() symbolsFound: SymbolSearchTypeAheadSupport[] = [];
  @Input() optionsByExpirationFound: OptionSymbolByExpiration[] = [];
  @Input() optionsByStrikeFound: OptionSymbolByStrike[] = [];
  @Input()
  set forexSymbols(value: ForexSymbol[]) {
    if (value && value.length > 0 && this.savedForexSymbols.length === 0) {
      this.savedForexSymbols = value;
      this.filterForexSymbols();
    }
  }
  get forexSymbols(): ForexSymbol[] {
    return this.workingForexSymbols;
  }

  @Output() symbolSearchEmmiter: EventEmitter<SymbolSearch> = new EventEmitter<SymbolSearch>();
  @Output() optionSearchEmmiter: EventEmitter<OptionSearchByExpiration> = new EventEmitter<OptionSearchByExpiration>();
  @Output() loadForexDataEmmiter: EventEmitter<ForexPairs> = new EventEmitter<ForexPairs>();

  @Output() symbolSelectedEmmiter: EventEmitter<SymbolSearchTypeAheadSupport> = new EventEmitter<SymbolSearchTypeAheadSupport>();
  @Output() optionSelectedEmmiter: EventEmitter<GetLastQuote> = new EventEmitter<GetLastQuote>();
  @Output() forexSelectedEmmiter: EventEmitter<GetLastQuote> = new EventEmitter<GetLastQuote>();


  private workingForexSymbols: ForexSymbol[] = [];
  private savedForexSymbols: ForexSymbol[] = [];

  // non_US_stockExchange_prefixes = LSE,ASX,TSX,GE,XETRA,MEFF,NL 
  public markets: SelectItem[] = [
    { label: 'ASX', value: 'ASX' },
    // { label: 'Comp', value: 'COMP' },
    { label: 'BDM', value: 'BDM' },
    { label: 'HKFE', value: 'HKFE' },
    { label: 'GE', value: 'GE' },
    { label: 'LSE', value: 'LSE' },
    { label: 'MEFF', value: 'MEFF' },
    { label: 'NZFOE', value: 'NZFOE' },
    { label: 'TSX', value: 'TSX' },
    { label: 'US', value: 'US' },
    { label: 'XETRA', value: 'XETRA' },
  ];

  /* CQG:
  2/19/2017 4: 52:38 AM Datasource: CQG Comp, Abbreviation: COMP Status:Available Instruments:Future, Option, Stock, Cash, Index, Report
  12/19/2017 4: 52:38 AM Datasource: ASX 24, Abbreviation: SFE Status:Available Instruments:Future, Option, Cash
  12/19/2017 4: 52:38 AM Datasource: Montreal Exchange , Abbreviation: BdM Status:Available Instruments:Future, Option, Cash, Index
  12/19/2017 4: 52:38 AM Datasource: Deutsche Boerse Xetra, Abbreviation: GE Status:Available Instruments:Stock, Cash
  12/19/2017 4: 52:38 AM Datasource: MEFF Renta Variable, Abbreviation: MEFF-RV Status: Available Instruments:Future, Option, Cash, Index
  12/19/2017 4: 52:38 AM Datasource: LSE UK Equities, Abbreviation: LSE Status:Available Instruments:Stock
  12/19/2017 4: 52:38 AM Datasource: Hong Kong Futures Exchange, Abbreviation: HKFE Status:Available Instruments:Future, Option, Cash
  12/19/2017 4: 52:38 AM Datasource: Xetra Euro Stars, Abbreviation: Xetra ES Status: Available Instruments:Stock
  12/19/2017 4: 52:38 AM Datasource: Australian Stock Exchange, Abbreviation: ASX Status:Available Instruments:Future, Option, Stock, Cash
  12/19/2017 4: 52:38 AM Datasource: RICI Indexes, Abbreviation: RIC Status:Available Instruments:Cash, Currency, Index
  12/19/2017 4: 52:38 AM Datasource: ASX 24 (NZ), Abbreviation: NZFOE Status:Available Instruments:Future, Option, Cash
  12/19/2017 4: 52:38 AM Datasource: ASX Trade, Abbreviation: ASXTrade Status:Available Instruments:Future, Option, Cash, Index
  12/19/2017 4: 52:38 AM Datasource: Dow & Key Indices, Abbreviation: DJI Status:Available Instruments:Cash, Index
  */

  //public instruments: instrumentOption[] = [
  //  { label: 'Stocks',  value: InstrumentString.stock },
  //  { label: 'Options', value: InstrumentString.option },
  //  { label: 'Futures', value: InstrumentString.future },
  //  { label: 'Forex',   value: InstrumentString.forex },
  //  { label: 'Mutual',  value: InstrumentString.mutual },
  //];

  marketsMenuItems: MenuItem[] = [
    { label: 'US', icon: 'fa fa-flag', command: (event) => { this.selectedMarket = "US" } },
    { label: 'ASX', icon: 'fa fa-flag', command: (event) => { this.selectedMarket = "ASX" } },
    { label: 'BDM', icon: 'fa fa-flag', command: (event) => { this.selectedMarket = "BDM" } },
    { label: 'HKFE', icon: 'fa fa-flag', command: (event) => { this.selectedMarket = "HKFE" } },
    { label: 'GE', icon: 'fa fa-flag', command: (event) => { this.selectedMarket = "GE" } },
    { label: 'LSE', icon: 'fa fa-flag', command: (event) => { this.selectedMarket = "LSE" } },
    { label: 'MEFF', icon: 'fa fa-flag', command: (event) => { this.selectedMarket = "MEFF" } },
    { label: 'NZFOE', icon: 'fa fa-flag', command: (event) => { this.selectedMarket = "NZFOE" } },
    { label: 'TSX', icon: 'fa fa-flag', command: (event) => { this.selectedMarket = "TSX" } },
    { label: 'XETRA', icon: 'fa fa-flag', command: (event) => { this.selectedMarket = "XETRA" } }
  ];


  /*
  public searchType = [
    { label: 'Symbol', value: 'Symbol' },
    { label: 'Name', value: 'Name' },
  ];
  */
  selectedInstrumentReceiver(instrument: InstrumentString) {
    this.selectedInstrument = instrument;
  }

  public selectedMarket: string = 'US';
  // public selectedSearchType: string = 'Symbol';

  public queryText: string = "";
  public selectedSymbol: string;
  public market: string = "";
  public minTextLength = 2;


  constructor(private notificationsService: NotificationsService) { }

  ngOnInit() {
    this.selectedInstrument = this.internaSearchSymbolRequest.instrument;
  }

  /**
   * Validate search query
   */
  private validate(): boolean {
    switch (this.selectedInstrument) {
      case InstrumentString.option: return this.validateStock();
      case InstrumentString.stock: return this.validateStock();
      case InstrumentString.future: return this.validateFutures();
      case InstrumentString.forex: return true;
      case InstrumentString.mutual: return this.validateStock();
      default: return false;
    }
  }

  /**
   * Validate stocks query
   */
  private validateStock(): boolean {
    let result = true;

    if (!this.checkEmptyText(this.internaSearchSymbolRequest.query)) {
      return false;
    }
    return result;
  }

  /**
   * Validate futures query
   */
  private validateFutures(): boolean {
    let result = true;

    if (!this.checkEmptyText(this.internaSearchSymbolRequest.query)) {
      return false;
    }

    if (this.internaSearchSymbolRequest.query.trim().length < 2) {
      this.notificationsService.warn("Too short!", "Please enter a longer text.", { timeOut: 3000, showProgressBar: false, pauseOnHover: true });
      return false;
    }

    return result;
  }

  /**
   * Check empty query text
   * @param query
   */
  private checkEmptyText(query: string) {
    if (query.trim().length == 0) {
      this.notificationsService.warn("Empty query text", "Please enter a text.", { timeOut: 3000, showProgressBar: false, pauseOnHover: true });
      return false;
    }
    return true;
  }


  createSymbolSearchRequest(): SymbolSearch {
    let result: SymbolSearch = new SymbolSearch();
    result.market = Utils.prepMarket(this.selectedMarket)
    result.instrument = this.selectedInstrument;
    result.query = this.internaSearchSymbolRequest.query.trim();
    result.returnToRoute = this.internaSearchSymbolRequest.returnToRoute;
    return result;
  }

  createOptionSearchRequest(): OptionSearchByExpiration {
    let result: OptionSearchByExpiration = new OptionSearchByExpiration();
    result.optionType = '';
    result.underlaying = this.internaSearchSymbolRequest.query.trim();
    result.returnToRoute = this.internaSearchSymbolRequest.returnToRoute;
    return result;
  }

  /**
   * This is actually called just one time. It loads all forex symbols (99 items)
   * and we store forex data in application (store or localStorage).
   */
  createForexSearchRequest(): ForexPairs {
    let result: ForexPairs = new ForexPairs();
    result.returnToRoute = this.internaSearchSymbolRequest.returnToRoute;
    return result;
  }

  searchsymbol() {
    if (this.validate()) {

      switch (this.selectedInstrument) {
        case InstrumentString.option:
          this.optionSearchEmmiter.emit(this.createOptionSearchRequest());
          break;
        case InstrumentString.forex:
          // We load 99 symbols pairs just once.
          if (this.savedForexSymbols.length === 0) {
            this.loadForexDataEmmiter.emit(this.createForexSearchRequest());
          } else {
            this.filterForexSymbols();
          }
          break;
        case InstrumentString.stock:
        case InstrumentString.future:
        case InstrumentString.mutual:
          this.symbolSearchEmmiter.emit(this.createSymbolSearchRequest());
          break;
        default: throw Error("Wrong instrument");
      }
    }
  }

  queryTextReceiver(queryText: string) {
    this.internaSearchSymbolRequest.query = queryText;
    this.searchsymbol();
  }

  settings() {
    alert("SymbolSearchComponent settings - not yet implemented");
  }

  symbolSelected(symbol: SymbolSearchTypeAheadSupport) {
    symbol.returnToRoute = this.internaSearchSymbolRequest.returnToRoute;
    this.symbolSelectedEmmiter.emit(symbol);
  }

  forexSelected(symbol: GetLastQuote) {
    symbol.returnToRoute = this.internaSearchSymbolRequest.returnToRoute;
    this.forexSelectedEmmiter.emit(symbol);
  }

  optionSelected(symbol: GetLastQuote) {
    symbol.returnToRoute = this.internaSearchSymbolRequest.returnToRoute;
    this.optionSelectedEmmiter.emit(symbol);
  }

  filterForexSymbols() {
    if (this.internaSearchSymbolRequest.query) {
      const search = this.internaSearchSymbolRequest.query.trim().toUpperCase();
      this.workingForexSymbols = this.savedForexSymbols.filter((symbol: ForexSymbol) => symbol.Symbol.includes(search) || symbol.Descrip.includes(search));
    } else {
      this.workingForexSymbols = this.savedForexSymbols;
    }
  }

}
