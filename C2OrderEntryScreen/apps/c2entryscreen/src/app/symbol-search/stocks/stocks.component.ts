import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { SymbolSearchTypeAheadSupport } from '@app/models/LastQuoteService.dtos';
import { InternaSearchSymbolRequest } from '@app/models';

@Component({
  selector: 'c2scrn-search-stocks-result',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StocksComponent {

  @Input() symbolsFound: SymbolSearchTypeAheadSupport[];
  @Input() loading: false;
  @Output() symbolSelectedEmmiter: EventEmitter<SymbolSearchTypeAheadSupport> = new EventEmitter<SymbolSearchTypeAheadSupport>();


  symbolSelected(symbol: SymbolSearchTypeAheadSupport) {
    this.symbolSelectedEmmiter.emit(symbol);
  }


}

