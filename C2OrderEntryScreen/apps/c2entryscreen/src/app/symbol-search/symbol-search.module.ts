import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { SymbolSearchEffects } from '@app/store/effects/symbol-search.effects';
import { reducers as symbolSearchReducers } from '@app/store/reducers/symbol-search/';

import { OptionSearchByExpirationsEffects } from '@app/store/effects/search-option-by-expiration.effects';
import { reducer as optionsByExpirationsReducers } from '@app/store/reducers/search-option-by-expiration.reducer';

import { OptionSearchByStrikeEffects } from '@app/store/effects/search-option-by-strike.effects';
import { reducer as optionsByStrikeReducers } from '@app/store/reducers/search-option-by-strike.reducer';

import { ForexSymbolsEffects } from '@app/store/effects/search-forex.effects';
import { reducer as forexSymbolsReducers } from '@app/store/reducers/search-forex.reducer';

import { SymbolSearchComponent } from './symbol-search.component';
import { SymbolSearchContainerComponent } from './container/symbol-search-container.component';

import { SymbolSearchModule } from '@app/symbol-search';
import { StocksComponent } from './stocks/stocks.component';
import { ForexComponent } from './forex/forex.component';
import { FuturesComponent } from './futures/futures.component';
import { OptionsComponent } from './options/options.component';
import { MutualComponent } from './mutual/mutual.component';
import { ForexSymbolsService } from '@app/core/services/forex-symbols.service';
import { SearchService } from '@app/core/services/symbol-search.service';
import { SymbolSearchSandboxService } from '@app/symbol-search/symbol-search-sandbox.service';
import { SharedModule } from '@app/shared';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    /**
    * StoreModule.forFeature is used for composing state
    * from feature modules. These modules can be loaded
    * eagerly or lazily and will be dynamically added to
    * the existing state.
    */
    StoreModule.forFeature('symbolSearch', symbolSearchReducers),
    StoreModule.forFeature('optionsByExpiration', optionsByExpirationsReducers),
    StoreModule.forFeature('optionsByStrike', optionsByStrikeReducers),
    StoreModule.forFeature('forexSymbols', forexSymbolsReducers),


    /**
     * Effects.forFeature is used to register effects
     * from feature modules. Effects can be loaded
     * eagerly or lazily and will be started immediately.
     *
     * All Effects will only be instantiated once regardless of
     * whether they are registered once or multiple times.
     */
    EffectsModule.forFeature([
      ForexSymbolsEffects,
      OptionSearchByExpirationsEffects,
      OptionSearchByStrikeEffects,
      SymbolSearchEffects,
    ]),
  ],
  declarations: [SymbolSearchComponent, SymbolSearchContainerComponent, StocksComponent, ForexComponent, FuturesComponent, OptionsComponent, MutualComponent],
  exports: [SymbolSearchContainerComponent],
  providers: [
    ForexSymbolsService,
    SearchService,
    SymbolSearchSandboxService]
})
export class C2EntrySymbolSearchModule { }
