import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { OptionExpiry, OptionCallPut, OptionStrike, SymbolSearchTypeAheadSupport, OptionSymbolByExpiration, OptionSymbolByStrike } from '@app/models/LastQuoteService.dtos';
import { SelectItem } from 'primeng/components/common/selectitem';
import { GetLastQuote, InstrumentString } from '@app/models';

import { ForexSymbol } from '@app/models/LastQuoteService.dtos';

@Component({
  selector: 'c2scrn-forex',
  templateUrl: './forex.component.html',
  styleUrls: ['./forex.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ForexComponent  {

  @Input() forexSymbols: ForexSymbol[] = [];
  @Input() loadingForex = false;

  @Output() symbolSelectedEmmiter: EventEmitter<GetLastQuote> = new EventEmitter<GetLastQuote>();


  symbolSelected(symbol: ForexSymbol): void {
    let result: GetLastQuote = new GetLastQuote();
    result.market = 'US';
    result.typeofsymbol = InstrumentString.forex;
    result.symbol = symbol.Symbol;

    this.symbolSelectedEmmiter.emit(result);
  }

}
