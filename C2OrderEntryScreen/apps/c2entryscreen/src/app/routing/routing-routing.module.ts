import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { OrderScreenContainer } from '@c2scrn/order-screen/container/container.component';


const routes: Routes = [
  //{ path: '', redirectTo: '/home', pathMatch: 'full' },
  //{ path: 'home', loadChildren: '../home/home.module#HomeModule' },
  { path: '', redirectTo: '/entryscreen', pathMatch: 'full' },
  { path: 'entryscreen', loadChildren: '@c2scrn/order-screen/order-screen.module#OrderScreenModule' },
  { path: '**', loadChildren: '../not-found-page/not-found-page.module#NotFoundPageModule' }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled',
  //  enableTracing: true  // <-- debugging purposes only
  })],
  exports: [RouterModule]
})
export class RoutingRoutingModule { }
