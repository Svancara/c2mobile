import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { ISendSignal } from '@app/models';

import * as screenReducer from '@app/store/reducers/order-screen.reducer';
import * as screenActions from '@app/store/actions/order-screen-actions';
import * as fromStrategies from '@app/store/reducers/user-strategies';
import { IStrategy, InternaSearchSymbolRequest, GetLastQuote } from '@app/models';
import { OrderScreenSandbox } from '@libs/order-screen/order-screen.sandbox';
import { SymbolSearchSandboxService } from '@app/symbol-search/symbol-search-sandbox.service';
import { ForexPairs, OptionSearchByExpiration, SymbolSearch } from '@app/models/LastQuoteService.dtos';
import { MenuItem } from 'primeng/components/common/menuitem';

@Component({
  selector: 'c2scrn-container',
  templateUrl: './order-screen-container.component.html',
  styleUrls: ['./order-screen-container.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OrderScreenContainer implements OnInit {

  constructor(public orderScreenSandbox$: OrderScreenSandbox,
    public symbolsSearchSandbox$: SymbolSearchSandboxService) { }

  /**
 * Search stocks, futures,...  But not options.
 * @param query
 */
  symbolSearch(query: SymbolSearch) {
    this.symbolsSearchSandbox$.symbolSearch(query);
  }

  optionSearch(query: OptionSearchByExpiration) {
    this.symbolsSearchSandbox$.optionSearch(query);
  }

  loadForexData(request: ForexPairs) {
    this.symbolsSearchSandbox$.loadForexData(request);
  }


  symbolSelected(request: GetLastQuote) {
    this.orderScreenSandbox$.getLastQuote(request);
  }

  sendSignalEvent(signal: ISendSignal) {
    // Do something with the notification (evt) sent by the child!
    // this.store.dispatch(new screenActions.Send(signal));
    this.orderScreenSandbox$.sendSignal(signal);
  }


  // =====================================================================================
  items: MenuItem[];

  ngOnInit() {
    this.items = [
      { label: 'Dashboard' },
      { label: 'Create strategy'},
      { label: 'My strategies'},
      { label: 'Trade Leader'},
      { label: 'Developer' },
      { label: 'and so on' },
      { label: 'and so on' },
      { label: 'and so on' },
      { label: 'and so on' },
      { label: 'and so on' },
      { label: 'and so on' },
      { label: 'and so on' },
      { label: 'and so on' },
    ];
  }

}
