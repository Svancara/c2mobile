import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Instrument, OrderScreenSymbol, OrderScreen, TIF, InstrumentString, ISendSignal, C2Action, InternaSearchSymbolRequest, GetLastQuote, LastQuoteData, FundamentalDataResponse } from '@app/models';
import { SelectItem } from 'primeng/components/common/selectitem';
//import { Observable } from 'rxjs/Observable';
import { Message } from 'primeng/components/common/message';
import { MessageService } from 'primeng/components/common/messageservice';
import { SendSignal } from '@app/models/sendSignal';
// import { OrderScreenSandbox } from '@app/order-screen/order-screen.sandbox';
import { SymbolSearchTypeAheadSupport, OptionSearchByExpiration, ForexPairs, SymbolSearch } from '@app/models/LastQuoteService.dtos';
import { Utils } from '@app/utils';
import { SignalFactory } from '@libs/order-screen/utils/signal-factory';

import { OrderType, ValidationResult } from '@libs/order-screen/model';
import { Router, ActivatedRoute } from '@angular/router';
import { SearchSymbolRequestFactory } from '@libs/search-symbols/searchRequestFactory';
import { IQFeedSymbology } from '@libs/Utils/iqfeedSymbology';

enum ScreenType { SimpleStock, AdvancedStock }

@Component({
  selector: 'c2scrn-component',
  templateUrl: './order-screen.component.html',
  styleUrls: ['./order-screen.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush // does not work
})
export class OrderScreenComponent implements OnInit {

  private _lastQuote: LastQuoteData;

  private signalFactoryForPartialValidation: SignalFactory = new SignalFactory();

  @Input()
  set lastQuote(value: LastQuoteData) {
    if (value) {
      this._lastQuote = value;
      this.symbol = value.symbol;
      this.actionAllowed(value.symbol, value.typeofsymbol as InstrumentString);
      this.selectedInstrumentReceiver(value.typeofsymbol as InstrumentString);
    }
  }
  get lastQuote(): LastQuoteData {
    return this._lastQuote
  }

  @Input() fundamentalData: FundamentalDataResponse;

  @Input() strategyId: number;
  @Input() strategyName: string;
  @Output() notifyParent: EventEmitter<ISendSignal> = new EventEmitter();

  @Output() symbolSearchEmmiter: EventEmitter<SymbolSearch> = new EventEmitter<SymbolSearch>();
  @Output() optionSearchEmmiter: EventEmitter<OptionSearchByExpiration> = new EventEmitter<OptionSearchByExpiration>();
  @Output() loadForexDataEmmiter: EventEmitter<ForexPairs> = new EventEmitter<ForexPairs>();

  @Output() symbolSelectedEmmiter: EventEmitter<GetLastQuote> = new EventEmitter<GetLastQuote>();
  @Output() instrumentSelectedEmmiter: EventEmitter<InstrumentString> = new EventEmitter<InstrumentString>();


  //  public lock: boolean = true;

  public symbol: string = "";
  public market: string = "US";

  public screenType: ScreenType = ScreenType.SimpleStock;

  public orderType: SelectItem[] = [
    { label: 'Market', value: OrderType.Market },
    { label: 'Limit', value: OrderType.Limit },
    { label: 'Stop', value: OrderType.Stop },
  ];


  public limitPriceDisabled = true;
  public stopPriceDisabled = true;

  public limitPrice: number;
  public parkuntil: Date;
  public parkuntilChecked: boolean;
  public profitTarget: number;
  public quantity: number;
  public stopLoss: number;
  public stopPrice: number;
  public tif: boolean = true;

  public msgs: Message[] = [];
  public showMessages = false;

  // --------------- Private -----------------

  private _selectedOrderType: OrderType = OrderType.Market;

  public get selectedOrderType(): OrderType {
    return this._selectedOrderType;
  }
  public set selectedOrderType(value: OrderType) {
    this._selectedOrderType = value;
    switch (value) {
      case OrderType.Market:
        this.limitPriceDisabled = true;
        this.stopPriceDisabled = true;
        break;
      case OrderType.Limit:
        this.limitPriceDisabled = false;
        this.stopPriceDisabled = true;
        break;
      case OrderType.Stop:
        this.limitPriceDisabled = true;
        this.stopPriceDisabled = false;
        break;
      default:
        this.limitPriceDisabled = true;
        this.stopPriceDisabled = true;
    }
  }

  constructor(private router: Router, private actRoute: ActivatedRoute) { }

  ngOnInit() {
    //this.router.navigate(['./', { outlets: { signals: ['toSignals'] } }], { relativeTo: this.actRoute, queryParamsHandling: 'preserve' });
    //this.router.navigate(['./', { outlets: { positions: ['positions'] } }], { relativeTo: this.actRoute, queryParamsHandling: 'preserve' });
    this.router.navigateByUrl('/entryscreen/(positions:positions//signals:toSignals)');
    this.selectedInstrument = InstrumentString.stock;
  }

  public send(action: C2Action) {

    this.msgs = [];
    this.showMessages = false;

    let validationResult = this.createSignal(action)

    if (validationResult.valid) {
      this.notifyParent.emit(validationResult.validSignal);
    } else {
      this.msgs.push(validationResult.message);
    }
    this.showMessages = true;
    //  this.lock = true;
  }

  private _selectedInstrument: InstrumentString = InstrumentString.stock;
  public set selectedInstrument(value: InstrumentString) { this._selectedInstrument = value; }
  public get selectedInstrument(): InstrumentString { return this._selectedInstrument }

  /**
   * selectedInstrumentReceiver
   * @param instrument
   */
  selectedInstrumentReceiver(instrument: InstrumentString) {
    this.selectedInstrument = instrument;
    this.actionAllowed(this.symbol,instrument);
    this.instrumentSelectedEmmiter.emit(instrument);
    if (this.selectedInstrument === InstrumentString.mutual) {
      alert("Mutuals do not work right now. (An error in C2 API.)");
    }
  }

  /**
   * Creates a signal form order screen variables, checks it and returns it if valid.
   * If not valid,  eturns a message.
   * @param action
   */
  createSignal(action: C2Action): ValidationResult {

    if (!this.actionAllowed(this.symbol, this.selectedInstrument)) {
      return { valid: false, message: { severity: "error", summary: "Entered data are not correct", detail: "" } };
    }

    let signalFactory: SignalFactory = new SignalFactory();

    signalFactory._parkUntilSecs_ = this.parkuntil;
    signalFactory.action = action;
    signalFactory.limitPrice = this.limitPrice;
    signalFactory.orderType = this.selectedOrderType;
    signalFactory.parkuntil = this.parkuntil;
    signalFactory.parkuntilChecked = this.parkuntilChecked;
    signalFactory.profitTarget = this.profitTarget;
    signalFactory.quant = this.quantity;
    signalFactory.stopPrice = this.stopPrice;
    signalFactory.stopLoss = this.stopLoss;
    signalFactory.strategyId = this.strategyId;
    signalFactory.symbol = this.symbol;
    signalFactory.tif = this.tif;
    signalFactory.typeofsymbol = this.selectedInstrument;


    return signalFactory.validate();
  }


  buy() { this.send(C2Action.BTO); }
  sell() { this.send(C2Action.STC); }
  short() { this.send(C2Action.STO); }
  cover() { this.send(C2Action.BTC); }


  //private getInstrumentType(): InstrumentString {
  //  switch (this.selectedInstrument) {
  //    case 0: return InstrumentString.stock;
  //    case 1: return InstrumentString.option;
  //    case 2: return InstrumentString.future;
  //    case 3: return InstrumentString.forex;
  //    case 4: return InstrumentString.mutual;
  //  }
  //}


  /**
   * Search symbols button clicked
   */
  searchsymbol() {
    let searchFactory: SearchSymbolRequestFactory = new SearchSymbolRequestFactory(this.selectedInstrument, "US", this.symbol);

    let validationResult: ValidationResult = searchFactory.validate();
    if (validationResult.valid) {
      switch (this.selectedInstrument) {
        case InstrumentString.option:
          this.optionSearchEmmiter.emit(searchFactory.createOptionSearchRequest());
          break;
        case InstrumentString.forex:
          this.loadForexDataEmmiter.emit(searchFactory.createForexSearchRequest());
          break;
        case InstrumentString.stock:
        case InstrumentString.future:
        case InstrumentString.mutual:
          this.symbolSearchEmmiter.emit(searchFactory.createSymbolSearchRequest());
          break;
        default: throw new Error("Wrong instrument");
      }
    }
  }

  settings() {
    alert("OrderScreenComponent says: Order Entry settings clicked");
  }


  createRequest(): GetLastQuote {
    let result: GetLastQuote = new GetLastQuote();
    result.market = this.market;

    // result.typeofsymbol = Utils.prepInstrument(this.selectedInstrument); // ToDo: typeofsymbol must be checked at first
    result.typeofsymbol = IQFeedSymbology.getInstrumentType(this.symbol); // ToDo: This is better for demo purposes.
    if (result.typeofsymbol === InstrumentString.mutual) {
      result.typeofsymbol = InstrumentString.stock;
    }

    result.symbol = Utils.prepSymbol(this.symbol); // this.searchedSymbol$.Symbol;
    return result;
  }

  sendRequest() {
    if (this.symbol.trim()) {
      this.symbolSelectedEmmiter.emit(this.createRequest());
    }
  }


  bidClicked() {
    //  this.limitPrice = this._lastQuote.bid;
  }
  lastTradeClicked(value) {
    // alert("LastBid" + value);
  }
  askClicked(value) {
    // this.stopPrice = this._lastQuote.ask;
  }


  public smallLastQuotePanel = true;
  public collapsed = true;
  toggleLastQuotePanel() {
    this.collapsed = !this.collapsed;
    this.smallLastQuotePanel = !this.smallLastQuotePanel;
  }


  public actionAllowedHint: ValidationResult;
  public symbolLooksLike: string = '';
  public lock: boolean = true;

  actionAllowed(symbolTypedIn: string, instrument: InstrumentString): boolean {

    this.signalFactoryForPartialValidation.symbol = symbolTypedIn;

    this.signalFactoryForPartialValidation._parkUntilSecs_ = this.parkuntil;
    this.signalFactoryForPartialValidation.action = undefined;
    this.signalFactoryForPartialValidation.limitPrice = this.limitPrice;
    this.signalFactoryForPartialValidation.orderType = this.selectedOrderType;
    this.signalFactoryForPartialValidation.parkuntil = this.parkuntil;
    this.signalFactoryForPartialValidation.parkuntilChecked = this.parkuntilChecked;
    this.signalFactoryForPartialValidation.profitTarget = this.profitTarget;
    this.signalFactoryForPartialValidation.quant = this.quantity;
    this.signalFactoryForPartialValidation.stopPrice = this.stopPrice;
    this.signalFactoryForPartialValidation.stopLoss = this.stopLoss;
    this.signalFactoryForPartialValidation.strategyId = this.strategyId;
    this.signalFactoryForPartialValidation.tif = this.tif;
    this.signalFactoryForPartialValidation.typeofsymbol = instrument;
    let validationResult = this.signalFactoryForPartialValidation.validateWithoutAction();

    if (validationResult.valid) {
      this.actionAllowedHint = {
        valid: true, message: { detail: "", severity: "", summary: "" }
      };
    } else {
      this.actionAllowedHint = validationResult;
    }

    this.symbolLooksLike = this.checkInstrumentType(symbolTypedIn, instrument);
    let isInstrumentOK: boolean = '' === this.symbolLooksLike;

    this.lock = !(validationResult.valid && isInstrumentOK);

    return !this.lock;
  }

  checkInstrumentType(symbolTypedIn: string, selectedInstrument: InstrumentString): string {
    if (!symbolTypedIn) {
      return "A symbol is empty";
    }

    let iqFeedInstrument = IQFeedSymbology.getInstrumentType(symbolTypedIn);
    if (iqFeedInstrument !== selectedInstrument) {
      return 'The symbol looks like ' + iqFeedInstrument;
    }

    return ''; // OK
  }

  symbolValueChange() {
    this.actionAllowed(this.symbol, this.selectedInstrument);
  }

}
