import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Instrument, OrderScreenSymbol, OrderScreen, TIF, InstrumentString, ISendSignal, C2Action, InternaSearchSymbolRequest, GetLastQuote, LastQuoteData, FundamentalDataResponse } from '@app/models';
import { SymbolSearchTypeAheadSupport, SymbolSearch, OptionSearchByExpiration, ForexPairs } from '@app/models/LastQuoteService.dtos';

@Component({
  selector: 'c2scrn-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShellComponent implements OnInit {


  @Input() strategyId: number;
  @Input() strategyName: string;
  @Input() lastQuote: LastQuoteData;
  @Input() fundamentalData: FundamentalDataResponse;

  @Output() symbolSearchEmmiter: EventEmitter<SymbolSearch> = new EventEmitter<SymbolSearch>();
  @Output() optionSearchEmmiter: EventEmitter<OptionSearchByExpiration> = new EventEmitter<OptionSearchByExpiration>();
  @Output() loadForexDataEmmiter: EventEmitter<ForexPairs> = new EventEmitter<ForexPairs>();

  @Output() symbolSelectedEmmiter: EventEmitter<GetLastQuote> = new EventEmitter<GetLastQuote>();

  @Output() notifyParent: EventEmitter<ISendSignal> = new EventEmitter();

  public selectedInstrument: InstrumentString = InstrumentString.stock;

  constructor() { }

  ngOnInit() {
  }

  symbolSearch(query: SymbolSearch) { this.symbolSearchEmmiter.emit(query); }
  optionSearch($event) { this.optionSearchEmmiter.emit($event); }
  loadForexData($event) { this.loadForexDataEmmiter.emit($event) }

  symbolSelected(request: GetLastQuote) {
    this.symbolSelectedEmmiter.emit(request);
  }

  sendSignalEvent(signal: ISendSignal) {
    // Do something with the notification (evt) sent by the child!
    // this.store.dispatch(new screenActions.Send(signal));
    this.notifyParent.emit(signal);
  }

  instrumentSelected(instrument: InstrumentString) {
    this.selectedInstrument = instrument;
  }
  symbolSelectedInSearchinMechnism(data: SymbolSearchTypeAheadSupport) {
    alert('ShellComponent.symbolSelectedInSearchinMechnism ' + data.Symbol);
    //    this.sandbox$.symbolSelected(data);
  }

}
