import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';


import { CoreModule } from '@app/core';

import { PrimeNgModule } from '@c2scrn/primeng.module';
import { reducer } from '@app/store/reducers/order-screen.reducer';

import { SharedModule } from '@app/shared';
import { OrderScreenEffects } from '@app/store/effects/order-screen.effects';
import { OrderScreenSandbox } from '@libs/order-screen/order-screen.sandbox';

import { QuotesFeedsModule } from '@app/quotes-feeds/quotes-feeds.module';

import { OrderScreenContainer } from './container/order-screen-container.component';
import { OrderScreenComponent } from './component/order-screen.component';

import { C2EntrySignalsModule, SignalsContainerComponent } from '@c2scrn/signals'
import { C2EntryPositionsModule, PositionsListPageComponent } from '@c2scrn/positions'

// import { SymbolSearchModule } from '@app/symbol-search/symbol-search.module';
import { C2EntrySymbolSearchModule } from '@c2scrn/symbol-search';
import { ShellComponent } from './shell/shell.component'

const routes: Routes = [
  {
    path: 'entryscreen', component: OrderScreenContainer,
    children: [
      {
        path: 'toSignals',
         component: SignalsContainerComponent,
        //loadChildren: '@c2scrn/signals/signals.module#C2EntrySignalsModule',
        outlet: 'signals'
      },
      {
        path: 'positions',
        component: PositionsListPageComponent ,
        // loadChildren: '@c2scrn/positions/positions.module#C2EntryPositionsModule',
        outlet: 'positions'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    PrimeNgModule,
    QuotesFeedsModule,
    CoreModule,
    C2EntrySignalsModule,
    C2EntryPositionsModule,
    C2EntrySymbolSearchModule,
    SharedModule,
    RouterModule.forChild(routes),
    StoreModule.forFeature('order-screen', reducer),

    /**
     * Effects.forFeature is used to register effects
     * from feature modules. Effects can be loaded
     * eagerly or lazily and will be started immediately.
     *
     * All Effects will only be instantiated once regardless of
     * whether they are registered once or multiple times.
     */
    EffectsModule.forFeature([
      // PositionsEffects,
      OrderScreenEffects
    ]),
  ],
  declarations: [OrderScreenComponent,OrderScreenContainer, ShellComponent],
  exports: [OrderScreenContainer],
  providers: [OrderScreenSandbox]

})
export class OrderScreenModule { }
