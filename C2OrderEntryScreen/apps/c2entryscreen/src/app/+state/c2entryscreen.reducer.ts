import {C2entryscreen} from './c2entryscreen.interfaces';
import {C2entryscreenAction} from './c2entryscreen.actions';

export function c2entryscreenReducer(state: C2entryscreen, action: C2entryscreenAction): C2entryscreen {
  switch (action.type) {
    case 'DATA_LOADED': {
      return {...state, ...action.payload};
    }
    default: {
      return state;
    }
  }
}
