import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import {of} from 'rxjs/observable/of';
import 'rxjs/add/operator/switchMap';
import {C2entryscreenState} from './c2entryscreen.interfaces';
import {LoadData, DataLoaded} from './c2entryscreen.actions';

@Injectable()
export class C2entryscreenEffects {
  /*
  @Effect() loadData = this.dataPersistence.fetch('LOAD_DATA', {
    run: (action: LoadData, state: C2entryscreenState) => {
      return {
        type: 'DATA_LOADED',
        payload: {}
      };
    },

    onError: (action: LoadData, error) => {
      console.error('Error', error);
    }
  });
  */
  constructor(private actions: Actions) {}
}
