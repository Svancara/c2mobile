import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { reducers } from '@app/store/reducers/signals';

import { SignalsCollectionEffects } from '@app/store/effects/signals.effects';
import { SignalsService } from '@libs/signals-service/';

import { SharedModule } from '@app/shared';

import { SignalsComponent } from './signals.component';
import { SignalsContainerComponent } from './signals-container.component';
import { CommonSignalsModule } from "@libs/signals";


@NgModule({
  imports: [
    CommonModule,
    CommonSignalsModule,
    SharedModule ,  
    
    /**
     * StoreModule.forFeature is used for composing state
     * from feature modules. These modules can be loaded
     * eagerly or lazily and will be dynamically added to
     * the existing state.
     */
    StoreModule.forFeature('workingSignals', reducers),

    /**
     * Effects.forFeature is used to register effects
     * from feature modules. Effects can be loaded
     * eagerly or lazily and will be started immediately.
     *
     * All Effects will only be instantiated once regardless of
     * whether they are registered once or multiple times.
     */
    EffectsModule.forFeature([
      SignalsCollectionEffects
    ]),
  ],
  declarations: [
    SignalsComponent,
    SignalsContainerComponent,
  ],
  exports:[SignalsContainerComponent],
  providers: [SignalsService]

})
export class C2EntrySignalsModule { }
