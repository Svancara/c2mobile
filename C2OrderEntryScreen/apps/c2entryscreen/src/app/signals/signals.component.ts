import { Component, Input, ChangeDetectionStrategy, OnInit, EventEmitter, Output } from '@angular/core';
import { Signal } from '@app/models';
import { Router } from '@angular/router';

@Component({
  selector: 'c2scrn-signals',
  templateUrl: './signals.component.html',
  styleUrls: ['./signals.component.css']
})
export class SignalsComponent implements OnInit {

  @Input() strategyId: number;
  @Input() strategyName = "";
  @Input() signals: Signal[] = [];
  @Input() loading: false;
  @Output() cancelSignalEmmiter: EventEmitter<Signal> = new EventEmitter<Signal>();


  public displayDialog = false;
  public selectedSignal: Signal = undefined;
  public lock = true;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  showDialog(signal: Signal) {
    this.lock = true;
    this.displayDialog = true;
    this.selectedSignal = signal;
  }

  cancelSignal(signal: Signal) {
    this.lock = true;
    this.closeDialog();
    this.cancelSignalEmmiter.emit(signal)
  }

  closeDialog() {
    this.lock = true;
    this.displayDialog = false;
  }

  settings() {
    alert("SignalsListComponent says: Signals settings clicked");
  }

  swipeRight() {
    this.router.navigate(['/user-strategy', this.strategyId]);
  }
  swipeLeft() {
    this.router.navigate(['/orderentryscreen', this.strategyId]);
  }
  
}
