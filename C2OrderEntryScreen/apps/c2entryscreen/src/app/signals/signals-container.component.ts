import { Component, OnInit } from '@angular/core';
//import { Store } from '@ngrx/store';
//import { Observable } from 'rxjs/Observable';
//import { Subscription } from 'rxjs/Subscription';
//import 'rxjs/add/operator/first';

//import * as fromSignals from '@app/store/reducers/signals';
//import { Load as loadSignalsAction } from '@app/store/actions/signals/signals.collection.actions';
//import { Remove as CancelSignalAction } from '@app/store/actions/signals/signals.collection.actions';
import { Signal } from '@app/models';

//import { IStrategy } from '@app/models';
//import * as fromStrategies from '@app/store/reducers/user-strategies';

import { SignalsSandbox } from '@libs/signals';


@Component({
  selector: 'c2scrn-signals-container',
  templateUrl: './signals-container.component.html',
  styleUrls: ['./signals-container.component.css']
})
export class SignalsContainerComponent implements OnInit {

  //public signals$: Observable<Signal[]>;
  //public loading$: Observable<boolean>;
  //public strategy$: Observable<IStrategy>;
  //private strategyId: number;

  //constructor(private store: Store<any>) {
  //  this.signals$ = this.store.select(fromSignals.getAllSignals);
  //  this.loading$ = this.store.select(fromSignals.getCollectionLoading);
  //  this.strategy$ = this.store.select(fromStrategies.getSelectedStrategy);
  //}

  //ngOnInit() {
  //  this.strategy$.first().subscribe(strategy => {
  //    this.strategyId = strategy.system_id;
  //    this.store.dispatch(new loadSignalsAction(this.strategyId));
  //  });
  //}

  //cancelSignal(signal: Signal) {
  //  this.store.dispatch(new CancelSignalAction(signal));
  //}
  constructor(public sandbox$: SignalsSandbox) { }

  ngOnInit() {
    this.sandbox$.loadSignals()
  }

  cancelSignal(signal: Signal) {
    this.sandbox$.cancelSignal(signal);
  }
}
