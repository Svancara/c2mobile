import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignalsContainerComponent } from './signals-container.component';

describe('SignalsContainerComponent', () => {
  let component: SignalsContainerComponent;
  let fixture: ComponentFixture<SignalsContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignalsContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignalsContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
