import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/exhaustMap';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/take';
import 'rxjs/add/observable/throw';
import { empty } from 'rxjs/observable/empty';
import { of } from 'rxjs/observable/of';

import {
  HttpModule,
  RequestOptions,
  XHRBackend,
  Http
} from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule} from '@angular/forms';

import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { c2entryscreenReducer } from './+state/c2entryscreen.reducer';
import { c2entryscreenInitialState } from './+state/c2entryscreen.init';
import { C2entryscreenEffects } from './+state/c2entryscreen.effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { StoreRouterConnectingModule, RouterStateSerializer, } from '@ngrx/router-store';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { PrimeNgModule } from './primeng.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateService } from '@ngx-translate/core';
import { SimpleNotificationsModule } from 'angular2-notifications';

import { CoreModule } from '@app/core';
import { AuthModule } from '@app/auth/auth.module';

import { CustomRouterStateSerializer } from '@app/shared/ngrx-utils';

import { AppComponent } from './app.component';
import { UtilityModule } from '@app/utility';
import { ConfigService } from '@app/app-config.service';
import { LoggerModule } from '@app/logger/logger.module';

import { AppRoutingModule } from './routing/routing.module';
import { OrderScreenModule } from '@c2scrn/order-screen';

// ============== FOR DEVELOPMENT ONLY ==============================
// import { UserStrategiesListModule } from '@app/user-strategies-list';
// ============================================



import * as State from '@app/store';

// Guards
import { AuthGuard } from '@app/core/services/auth-guard.service';

// AoT requires an exported function for factories
/**
 * Calling functions or calling new is not supported in metadata when using AoT.
 * The work-around is to introduce an exported function.
 *
 * The reason for this limitation is that the AoT compiler needs to generate the code that calls the factory
 * and there is no way to import a lambda from a module, you can only import an exported symbol.
 */
export function configServiceFactory(config: ConfigService) {
  return () => config.load()
}

//export function HttpLoaderFactory(http: HttpClient) {
//  return new TranslateHttpLoader(http);
//}
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    PrimeNgModule,
    FormsModule, 

    HttpClientModule,
    HttpModule,
    LoggerModule,

    // ============== FOR DEVELOPMENT ONLY ==============================
    //UserStrategiesListModule,
    // ============================================

    // Routers
    OrderScreenModule,
    AppRoutingModule,

    CoreModule.forRoot(),
    AuthModule.forRoot(),

    UtilityModule.forRoot(),

    // StoreModule.forRoot({ c2entryscreen: c2entryscreenReducer }, { initialState: { c2entryscreen: c2entryscreenInitialState } }),
    StoreModule.forRoot(State.reducers),


    EffectsModule.forRoot([C2entryscreenEffects]),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    StoreRouterConnectingModule,
    SimpleNotificationsModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),


  ],

  providers: [
    /**
     * The `RouterStateSnapshot` provided by the `Router` is a large complex structure.
     * A custom RouterStateSerializer is used to parse the `RouterStateSnapshot` provided
     * by `@ngrx/router-store` to include only the desired pieces of the snapshot.
     */
    { provide: RouterStateSerializer, useClass: CustomRouterStateSerializer },
    ConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: configServiceFactory,
      deps: [ConfigService],
      multi: true
    }
  ],

  declarations: [AppComponent],
  bootstrap: [AppComponent],

})
export class AppModule { }
