import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppSandbox } from '@app/app.sandbox';

import { Store } from '@ngrx/store';
import * as settingsActions from '@app/store/actions/settings.action';
import { TranslateService } from '@ngx-translate/core';
import { ConfigService } from '@app/app-config.service';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  host: { '[class.body-loginPage]': 'isLoginPage' },
  providers: [AppSandbox]
})
export class AppComponent implements OnInit {

  public isLoginPage: boolean;

  public notificationOptions = {
    position: ["top", "right"],
    timeOut: 5000,
    lastOnBottom: true
  }

  constructor(
    private router: Router,
    public appSandbox: AppSandbox,
    private notificationsService: NotificationsService
    //private appState$: Store<any>,
    //private translate: TranslateService,
    //private configService: ConfigService
  ) { }

  ngOnInit() {
    this.appSandbox.setupLanguage();
    // Load user from local storage into redux state
    //this.appSandbox.loadUser();
    this.registerEvents();
    //    this.setupLanguage();
  }

  /**
 * Sets up default language for the application. Uses browser default language.
 */
  /*
  public setupLanguage(): void {
    let localization: any = this.configService.get('localization');
    let languages: Array<string> = localization.languages.map(lang => lang.code);
    let browserLang: string = this.translate.getBrowserLang();

    this.translate.addLangs(languages);
    this.translate.setDefaultLang(localization.defaultLanguage);

    let selectedLang = browserLang.match(/en|hr/) ? browserLang : localization.defaultLanguage;
    let selectedCulture = localization.languages.filter(lang => lang.code === selectedLang)[0].culture;

    this.translate.use(selectedLang);
    this.appState$.dispatch(new settingsActions.SetLanguageAction(selectedLang));
    this.appState$.dispatch(new settingsActions.SetCultureAction(selectedCulture));


    // --------------------------- TEST
    this.appState$.dispatch(new TEST.Load(1));
  }
  */


  /**
   * Registers events needed for the application
   */
  private registerEvents(): void {
    // Subscribes to route change event and sets "isLoginPage" variable in order to set correct CSS class on body tag.
    this.router.events.subscribe((route) => {
      this.isLoginPage = route['url'] === '/login' ? true : false;
    });
  }
}
