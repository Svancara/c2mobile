import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SignalsService } from '@libs/signals-service/';
import { SharedModule } from '@app/shared';

import { ComponentsModule } from './components/components.module';
import { reducers } from '@app/store/reducers/positions';

import { PositionsCollectionEffects } from '@app/store/effects/positions.effects';
// import { PositionsRoutePageComponent } from './containers/positions-route-page/positions-route-page.component';

import { PositionsModule  } from '@app/positions';
import { PositionsSandbox } from '@libs/positions';
import { PositionsListPageComponent } from '@c2scrn/positions';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    SharedModule,

    /**
     * StoreModule.forFeature is used for composing state
     * from feature modules. These modules can be loaded
     * eagerly or lazily and will be dynamically added to
     * the existing state.
     */
    StoreModule.forFeature('positions', reducers),

    /**
     * Effects.forFeature is used to register effects
     * from feature modules. Effects can be loaded
     * eagerly or lazily and will be started immediately.
     *
     * All Effects will only be instantiated once regardless of
     * whether they are registered once or multiple times.
     */
    EffectsModule.forFeature([
      //PositionsEffects,
      PositionsCollectionEffects
    ]),


  ],
  declarations: [
    PositionsListPageComponent,
    // PositionsRoutePageComponent,
  ],
  exports: [PositionsListPageComponent],
  providers: [PositionsSandbox]

})
export class C2EntryPositionsModule { }
