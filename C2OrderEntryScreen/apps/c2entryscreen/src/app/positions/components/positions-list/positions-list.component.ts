import { Component, OnInit, Input, EventEmitter, ChangeDetectionStrategy, Output } from '@angular/core';
import { Position } from '@app/models';
import { Router } from '@angular/router';

@Component({
  selector: 'app-positions-list',
  templateUrl: './positions-list.component.html',
  styleUrls: ['./positions-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PositionsListComponent implements OnInit {

  @Input() strategyId: number;
  @Input() strategyName = "";
  @Input() positions: Position[] = [];
  @Input() loading: false;
  @Output() closePositionEmmiter: EventEmitter<Position> = new EventEmitter<Position>();


  public displayDialog = false;
  public selectedPosition: Position = undefined;
  public lock = true;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  showDialog(position: Position) {
    this.lock = true;
    this.displayDialog = true;
    this.selectedPosition = position;
  }


  closePosition(position: Position) {
    this.lock = true;
    this.closeDialog();
    this.closePositionEmmiter.emit(position);
  }


  closeDialog() {
    this.lock = true;
    this.displayDialog = false;
  }

  settings() {
    alert("PositionsListComponent says: Positions settings clicked");
  }

  swipeRight() {
    this.router.navigate(['/orderentryscreen', this.strategyId]);
  }
  swipeLeft() {
    this.router.navigate(['/user-strategy', this.strategyId]);
  }

}
