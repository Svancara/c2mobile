import { Component, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-positions-header',
  templateUrl: './positions-header.component.html',
  styleUrls: ['./positions-header.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PositionsHeaderComponent {

  @Input() strategyId: number;
  @Input() strategyName: string = "";
  @Output() settingsEvent = new EventEmitter();

  settings() {
    this.settingsEvent.emit();
  }
}
