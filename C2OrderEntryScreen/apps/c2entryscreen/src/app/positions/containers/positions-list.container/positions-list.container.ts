import { Component, OnInit } from '@angular/core';
//import { Store } from '@ngrx/store';
//import { Observable } from 'rxjs/Observable';
//import { Subscription } from 'rxjs/Subscription';

//import 'rxjs/add/operator/first';

import { Position} from '@app/models';

//import {
//  Load as loadPositionsAction,
//  Remove as ClosePositionAction
//} from '@app/store/actions/positions/positions.collection';

//import { getAllPositions, getCollectionLoading } from '@app/store/reducers/positions';
//import { getSelectedStrategy } from '@app/store/reducers/user-strategies/';
//import { Remove } from '@app/store/actions/positions/positions.collection';

import { PositionsSandbox } from '@libs/positions';

@Component({
  selector: 'app-positions-list-page',
  templateUrl: './positions-list.container.html',
  styleUrls: ['./positions-list.container.css'],
})
export class PositionsListPageComponent implements OnInit {

  //public positions$: Observable<Position[]>;
  //public loading$: Observable<boolean>;
  //public strategy$: Observable<IStrategy>;

  //constructor(private store: Store<any>) {
  //  this.positions$ = this.store.select(getAllPositions);
  //  this.loading$ = this.store.select(getCollectionLoading);
  //  this.strategy$ = this.store.select(getSelectedStrategy);
  //}

  //ngOnInit() {
  //  this.strategy$.first().subscribe(strategy => {
  //    this.store.dispatch(new loadPositionsAction(strategy.system_id));
  //  });
  //}

  ///**
  //* Send signal closePosition 
  //*/
  //closePosition(position: Position) {
  //  this.store.dispatch(new Remove(position));
  //}

  constructor(public sandbox$: PositionsSandbox) { }

  ngOnInit() {
    this.sandbox$.loadPositions();
  }

  closePosition(position: Position) {
    this.sandbox$.closePosition(position);
  }
   

}
