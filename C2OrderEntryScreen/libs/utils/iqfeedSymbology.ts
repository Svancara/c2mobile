import { Instrument } from "@app/models";

export class IQFeedSymbology {

//  public static readonly UNKNOWN_INSTRUMENT = 'unknown_instrument';
  public static readonly UNKNOWN_INSTRUMENT = 'unknown';

  public static readonly gAllForexSymbols: Set<string> = new Set([
    "AEDPKR",
    "AUDCAD",
    "AUDCHF",
    "AUDJPY",
    "AUDNZD",
    "AUDUSD",
    "CADCHF",
    "CADJPY",
    "CHFJPY",
    "EURAUD",
    "EURCAD",
    "EURCZK",
    "EURDKK",
    "EURGBP",
    "EURCHF",
    "EURJPY",
    "EURNOK",
    "EURNZD",
    "EURSEK",
    "EURTRY",
    "EURUSD",
    "GBPAUD",
    "GBPCAD",
    "GBPCLP",
    "GBPCSK",
    "GBPDEM",
    "GBPDKK",
    "GBPEUR",
    "GBPGRD",
    "GBPHKD",
    "GBPHUF",
    "GBPCHF",
    "GBPINR",
    "GBPJOD",
    "GBPJPY",
    "GBPKES",
    "GBPLKR",
    "GBPMYR",
    "GBPNOK",
    "GBPNZD",
    "GBPPKR",
    "GBPPLZ",
    "GBPSAR",
    "GBPSEK",
    "GBPSGD",
    "GBPTHB",
    "GBPUSD",
    "GBPZAR",
    "CHFJPY",
    "NZDCAD",
    "NZDCHF",
    "NZDJPY",
    "NZDUSD",
    "TRYJPY",
    "USDAED",
    "USDATS",
    "USDBEF",
    "USDBHD",
    "USDBSD",
    "USDCAD",
    "USDCHF",
    "USDCLP",
    "USDCNH",
    "USDCSK",
    "USDCYP",
    "USDCZK",
    "USDDEM",
    "USDDKK",
    "USDESP",
    "USDFIM",
    "USDFRF",
    "USDHKD",
    "USDHUF",
    "USDIEP",
    "USDINR",
    "USDISK",
    "USDITL",
    "USDJMD",
    "USDJOD",
    "USDJPY",
    "USDKES",
    "USDKWD",
    "USDLKR",
    "USDMXN",
    "USDMYR",
    "USDNGN",
    "USDNLG",
    "USDNOK",
    "USDOMR",
    "USDPKR",
    "USDPLZ",
    "USDPTE",
    "USDRMB",
    "USDSAR",
    "USDSEK",
    "USDSGD",
    "USDTHA",
    "USDTHB",
    "USDTTD",
    "USDTRY",
    "USDZAR",
    "XAGUSD",
    "XAUUSD",
    "ZARJPY",
    "ZMKZAR"]);

  //# ------------------------------------
  //# ----- CURRENCIES ------
  //# ------------------------------------
  //# See http://iqfeed.net/symbolguide/index.cfm
  //# Added AFN, AZN, BAM, CDE, CDF, CNH, DEM, GOF, MZN, SDR, TMT, XDR, XPD, XPT by hand
  //# Also added XAG (XAG USD | Silver Dollar Rates) and
  //# XAU (XAU USD | Gold Dollar Rates) but notice that XAG and XAU are Precious Metals! So delete it if there will be some problems with these two "currencies"
  public static readonly g_IQfeed_Currencies: Set<string> = new Set(["AED", "AFA", "AFN", "ALL", "AMD", "ANG", "AOA", "AON", "ARS", "AUD", "AWG", "AZM", "AZN", "BAD", "BAM", "BBD", "BDT", "BFF", "BGN", "BHD", "BIF", "BMD", "BND", "BOB", "BRL", "BRR", "BSD", "BTC", "BTN", "BWP", "BYN", "BYR", "BZD", "CAD", "CDE", "CDF", "CDZ", "CLF", "CLP", "CLY",
    "CNH", "CNY", "COP", "CRC", "CUP", "CVE", "CYP", "CZK", "DEM", "DJF", "DKK", "DOP", "DZD", "EEK", "EGP", "ERN", "ETB", "EUR", "FJD", "FKP", "GBP", "GBX", "GEL", "GHC", "GHS", "GIP", "GMD", "GOF", "GNF", "GNS", "GTQ", "GWP", "GYD", 'HKD', "HNL", "HRD", "HRK",
    "HTG", "HUF", "CHF", "IDR", "ILS", "INR", "IQD", "IRR", "ISK", "ITL", "JMD", "JOD", "JPY", "KES", "KGS", "KHR", "KMF", "KPW", "KRW", "KWD", "KYD", "KZT", "LAK", "LBP", "LKR", "LRD", "LSL", "LTL", "LVL", "LYD", "MAD", "MDL", "MGA", "MGF", "MKD",
    "MMK", "MNT", "MOP", "MRO", "MTL", "MUR", "MVR", "MWK", "MXN", "MXV", "MYR", "MZM", "MZN", "NAD", "NGN", "NIC", "NIO", "NOK", "NPR", "NZD", "OMR", "PAB", "PEN", "PGK", "PHP", "PKR", "PLN", "PYG", "QAR", "RMB", "ROL", "RON", "RSD", "RUB", "RUR", "RWF", "SAR",
    "SBD", "SCR", "SDD", "SDG", "SDR", "SEK", "SGD", "SHP", "SIT", "SKK", "SLL", "SOS", "SRD", "SRG", "STD", "SVC", "SYP", "SZL", "THA", "THB", "TJR", "TJS", "TMT", "TND", "TOP", "TPE", "TRL", "TRY", "TTD", "TWD", "TZS", "UAH", "UDD", "UGX", "USD", "UYU", "UZS", "VEB", "VEF",
    'VND', "VUV", "WST", "XAF", "XAG", "XAU", 'XCD', "XDR", "XGD", "XOF", "XPD", "XPF", "XPT", "YDD", "YER", "YUD", "ZAR", "ZMK", "ZMW", "ZRZ", "ZWD"]);
  // # ------------------------------------
  // # ----- CONTRIBUTORS ------
  // # ------------------------------------
  // # See http://iqfeed.net/symbolguide/index.cfm
  // # Added by hand:
  // # MBT - It has a trailing space in mktsymbols_v2.txt.
  // # BOC, BVMF, CBIC, CBOA, DNOB, OEUA, NORD, PRNT
  public static readonly g_IQfeed_forexContributors: Set<string> = new Set(["ABBA", "AIBK", "BANX", "BARC", "BDBI", "BLLI", "BLTA", "BNPF", "BOC", "BROK", "BVMF", "CBIC", "CBOA", "CITA", "CMC", "COMP", "DDBK", "DNOB", "DRES", "DSTI", "ECBF", "EFXG", "ELSP", "EUAM", "FIBI", "FCFX", "FXCM", "HAPI", "IDBT",
    "IGBK", "LBMA", "MBT", "MDBI", "MFXM", "MIGF", "MIZI", "MRIT", "MOR2", "MORN", "N2EX", "NORD", "OCO", "OEUA", "OFCF", "OKOB", "OZAR", "POTA", "PRNT", "RADA", "RTFX", "SAXO", "SURF", "TUBD", "TULT", "TUTF", "UBII", "UBSW", "UWCL", "WBLT"]);
  // # ------------------------------------
  // # ----- PRECIOUS METALS ------
  // # ------------------------------------
  // # See http://iqfeed.net/symbolguide/index.cfm
  public static readonly g_IQfeed_PreciousMetals: Set<string> = new Set(["AUG", "XAG", "XAU", "XPD", "XPG", "XPT"]);
  // # ------------------------------------
  // # ----- COINS ------
  // # ------------------------------------
  // # See http://iqfeed.net/symbolguide/index.cfm (added ARE, FRN, M5P, NBL, VRL, VRN by hand)

  public static readonly g_IQfeed_Coins: Set<string> = new Set(["ARE", "AUN", "BRI", "DOE", "FRN", "KRU", "M5P", "MAL", "MXP", "NBL", "NSO", "OSO", "VRL", "VRN"]);


  public static isInList(findwhat: string, list: Set<string>): boolean {
    return list.has(findwhat);
  }


  //    # FUTURES
  //    # http://iqfeed.net/symbolguide:
  //    # The symbology for a future contract is Future Root, Month Code, Year's last 2 digits (@ESU10 for example).
  //    # MONTH CODES
  //    # Jan-F    Feb-G    Mar-H    Apr-J    May-K    Jun-M
  //    # Jul-N    Aug-Q    Sep-U    Oct-V    Nov-X    Dec-Z
  public static isFutureSymbol(symbol: string): boolean {
    symbol = symbol.toUpperCase();

    let regex = /^XCRA\d/g;
    if (regex.test(symbol))   // # option XCRA1615G10
    {
      return false;
    }

    // #if (length($symbol) > 7 && $symbol =~ /^(XCR.\D|XCRP\d|XNG|XPW)/ )
    regex = /^(XCR|XCRP\d|XNG|XPW)/g;
    if (symbol.length > 7 && regex.test(symbol)) {
      symbol = symbol.substr(4);
    }

    if (symbol.length > 7) {
      return false;
    }

    regex = /^M\.\D\D\d\d$/g;
    if (regex.test(symbol))   // # this instrument is FORWARD
    {
      return false;
    }

    regex = /^[@+]?\w+[FGHJKMNQUVXZ]\d\d?$/g;
    if (regex.test(symbol)) {
      return true;
    }

    regex = /^@\D+#$/g;
    if (regex.test(symbol)) {
      return true;
    }

    return false;
  }

  // #=item
  // #
  //  #2013-01-09: THE isForexSymbol REMOVED. USE isForexSymbolExtended INSTEAD   
  //  #
  //  # isForexSymbol
  //  #
  //  #=cut
  //  #
  //  # sub isForexSymbol($) {
  //  # my $symbol = uc(shift);
  //  #if (length($symbol) == 6 ) {
  //  # return IQFeedSymbology.isInList( $symbol, @gAllForexSymbols );
  //  #    }
  //  #else
  //  #    {
  //  # return isForexSymbolExtended($symbol);
  //  #    }
  //  #}

  // =item
  // See http://iqfeed.net/symbolguide/index.cfm:

  // Telvent DTN provides multiple sources: TenFore(multiple contributors) and FXCM
  // .Our Premium Forex add-on service includes both Tenfore and FXCM, while the Basic Forex add-on includes only FXCM.
  // The symbology for FXCM FOREX symbols is
  // "[BASE CURRENCY][SECOND CURRENCY].FXCM"

  // For Example: USDJPY.FXCM (USD/JPY Spot)

  // The symbology for TenFore FOREX symbols is
  // "[BASE CURRENCY][SECOND CURRENCY].[CONTRIBUTOR CODE]"

  // For Example: USDJPY.COMP (USD/JPY Composite symbol for all contributors) OR USDJPY.ABBA(USD/JPY for only ABBA contributor)

  // =cut

  public static isForexSymbolExtended(symbol: string): boolean {
    symbol = symbol.toUpperCase();

    if (symbol.length == 6) {
      return IQFeedSymbology.isInList(symbol, this.gAllForexSymbols);
    }

    let r: RegExp;
    // # Is there a contributor part?
    r = /\./;
    if (r.test(symbol)) {
      const dotIndex = symbol.indexOf('.');
      const contributor = symbol.substr(dotIndex + 1).trim();
      // contributor = ~s /\s *$//;    # there are trailing spaces in mktsymbols_v2.txt
      if (!IQFeedSymbology.isInList(contributor, this.g_IQfeed_forexContributors)) {
        return false;
      }

      // # Trim the contributor part
      symbol = symbol.substr(0, symbol.indexOf('.'));
    }

    if (symbol.length == 6) {
      const firstCurrency = symbol.substr(0, 3);
      const secondCurrency = symbol.substr(3, 3);
      return IQFeedSymbology.isInList(firstCurrency, this.g_IQfeed_Currencies) && IQFeedSymbology.isInList(secondCurrency, this.g_IQfeed_Currencies);
    }

    if (symbol.length == 7 && symbol.substr(0, 1) == "B") {

      // # BUSDXCD    BAR USD XCD Spot    BARCLAYS    BARCLAYS    FOREX
      const firstCurrency = symbol.substr(1, 3);
      const secondCurrency = symbol.substr(4, 3);
      return IQFeedSymbology.isInList(firstCurrency, this.g_IQfeed_Currencies) && IQFeedSymbology.isInList(secondCurrency, this.g_IQfeed_Currencies);
    }

    if (symbol.length == 8) {

      //# USDSEKON.TULT Exchange = TENFORE Market=TENFORE Descr=TULT USD SEK Overnight
      //# JPYCHF2M.COMP
      const firstCurrency = symbol.substr(0, 3);
      const secondCurrency = symbol.substr(3, 3);
      const inLists = IQFeedSymbology.isInList(firstCurrency, this.g_IQfeed_Currencies) && IQFeedSymbology.isInList(secondCurrency, this.g_IQfeed_Currencies);

      r = /^[A-Z]{6}(\d\D)|(TN)|(SN)|(ON)$/;
      if (inLists && r.test(symbol)) {
        return true;
      }
    }

    if (symbol.length == 9) {
      //# CADJPY12M.COMP
      //# USDSAR10Y.COMP
      //# CADUSDNOR.BOC   BANK OF CANADA CAD USD Noon Rate    DTN DTN FOREX
      //# EURCADIM3.COMP    COMP EUR CAD 3rd IMM Contract D     FOREX   FOREX   TENFORE
      const firstCurrency = symbol.substr(0, 3);
      const secondCurrency = symbol.substr(3, 3);
      const inLists = IQFeedSymbology.isInList(firstCurrency, this.g_IQfeed_Currencies) && IQFeedSymbology.isInList(secondCurrency, this.g_IQfeed_Currencies);
      r = /^[A-Z]{6}(\d\d[MY])|(NOR)|(IM3)$/;
      if (inLists && r.test(symbol)) {
        return true;
      }
    }

    if (symbol.length == 9 && symbol.substr(0, 1) == "B") {

      //# BUSDJPY2W   BAR USD JPY 2 Weeks BARCLAYS    BARCLAYS    FOREX
      //# BUSDSEKTN   BAR USD SEK Tomorrow    BARCLAYS    BARCLAYS    FOREX
      //# BUSDCHFSN, 'BAR USD CHF Spot Next', 'BARCLAYS', 'BARCLAYS', 'FOREX', '', '', ''
      //# BUSDCADON   BAR USD CAD Overnight   BARCLAYS    BARCLAYS    FOREX
      const firstCurrency = symbol.substr(1, 3);
      const secondCurrency = symbol.substr(4, 3);
      const inLists = IQFeedSymbology.isInList(firstCurrency, this.g_IQfeed_Currencies) && IQFeedSymbology.isInList(secondCurrency, this.g_IQfeed_Currencies);

      // # ToDo: Optimize this:
      r = /^B[A-Z]{6}(\d\D)|(TN)|(SN)|(ON)$/;
      if (inLists && (r.test(symbol))) {
        return true;
      }
    }
    if (symbol.length == 10 && symbol.substr(0, 1) == "B") {

      // # BUSDSEK10Y  BAR USD SEK 10 Years    BARCLAYS    BARCLAYS    FOREX
      const firstCurrency = symbol.substr(1, 3);
      const secondCurrency = symbol.substr(4, 3);
      const inLists = IQFeedSymbology.isInList(firstCurrency, this.g_IQfeed_Currencies) && IQFeedSymbology.isInList(secondCurrency, this.g_IQfeed_Currencies);
      r = /^B[A-Z]{6}\d\d\D$/;
      if (inLists && r.test(symbol)) {
        return true;
      }
    }

    return false;
  }


  //=item

  // Stocks

  //=cut

  public static isStockSymbol(symbol: string): boolean {
    symbol = symbol.toUpperCase();

    let r: RegExp;
    r = /\s/;
    if (r.test(symbol)) {
      return false;
    }

    r = /^[0-9]+/;
    if (r.test(symbol)) {
      return false;
    }

    r = /\@/;
    if (r.test(symbol)) {
      return false;
    }

    r = /\#/;
    if (r.test(symbol)) {
      return false;
    }

    r = /\$/;
    if (r.test(symbol)) {
      return false;
    }

    r = /,/;
    if (r.test(symbol)) // iqfeed error
    {
      return false;
    }

    r = /((\.PK)|(\.TEST))$/;
    if (r.test(symbol)) {
      return true;
    }

    if (IQFeedSymbology.isMoneyMarketFund(symbol)
      || IQFeedSymbology.isMutualFund(symbol)
      || IQFeedSymbology.isIndexSymbol(symbol)
      || IQFeedSymbology.isMarketStatisticsSymbol(symbol)
      || IQFeedSymbology.isBondSymbol(symbol)
      || IQFeedSymbology.isForexSymbolExtended(symbol)
      || IQFeedSymbology.isFutureSymbol(symbol)
      || IQFeedSymbology.isForward(symbol)
    ) {
      return false;
    }

    if (IQFeedSymbology.isCanadianSymbol(symbol)) // # includes Canadian bonds  
    {
      return true;
    }

    //# Exceptions found in IQFeed list of symbols mktsymbols_v2.txt
    //# ACP.R*  AVENUE INCOME CREDIT STRATEGY   NYSE    NYSE    EQUITY          0
    //# C.ACO.X ATCO LTD    CANADIAN    TSE EQUITY          0
    //# C.CU.X  CANADIAN UTILITIES LTD  CANADIAN    TSE EQUITY          0
    //# C.YCM.X COMMERCE SPLIT CORP CLASS A CANADIAN    TSE EQUITY          0
    //# NYCB-U 	NEW YORK COMMUNITY CAP TRUST V 	NYSE 	EQUITY 	NYSE
    //# and so on... 
    if (
      //   symbol == "ACP.R*"
      //|| symbol == "C.ACO.X"
      //|| symbol == "C.CU.X"
      //|| symbol == "C.YCM.X"
      //|| symbol == "ALIST.TE"
      //|| symbol == "COLE.W"
      //|| symbol == "NNNN.TES"
      //|| symbol == "OOOO.TES"
      //|| symbol == "COLE.W"
      //|| symbol == "SDOPRACL"
      //|| symbol == "SDOPRBCL"
      //|| symbol == "SDOPRCCL"
      //|| symbol == "SDOPRHCL"
      symbol == "CNUNQA"
      || symbol == "FLASRD"
      || symbol == "NYLD/A"
      || symbol == "ORRRY23"
      || symbol == "VALE/P"
    ) { return true; }

    //if (
    //    symbol == "AMG16"
    //    || symbol == "AMH16"
    //    || symbol == "AMJ16"
    //    )
    //{
    //    return false;
    //}

    r = /(\D){4}[-\.][ABCDEFGHIPU]/;
    if ((symbol.length == 6) && (r.test(symbol))) {
      return true;
    }

    r = /\+/;
    if (r.test(symbol)) // KODK+A
    {
      return true;
    }

    if (symbol.length >= 6) {
      return false;
    }

    return true;
  }

  //=item

  //The symbology for TenFore Precious Metals symbols is
  //"[METAL CODE][CURRENCY][SIZE].[CONTRIBUTOR]"

  //For Example: XAGUSDO.COMP (Silver Ounce USD Composite)
  //=cut

  public static isPreciousMetalSymbol(symbol: string): boolean {
    symbol = symbol.toUpperCase();

    if (symbol.length < 7) {
      return false;
    }

    let r: RegExp;

    // # Is there a contributor part?
    r = /\./;
    if (r.test(symbol)) {
      let dotIndex = symbol.indexOf('.');
      let contributor = symbol.substr(dotIndex + 1).trim();
      // $contributor = ~s /\s *$//;    # there are trailing spaces in mktsymbols_v2.txt
      if (!IQFeedSymbology.isInList(contributor, this.g_IQfeed_forexContributors)) {
        return false;
      }

      // # Trim the contributor part
      symbol = symbol.substr(0, symbol.indexOf('.'));
    }

    if (symbol.length > 7) {
      return false;
    }

    let metalCode = symbol.substr(0, 3);
    if (!IQFeedSymbology.isInList(metalCode, this.g_IQfeed_PreciousMetals)) {
      return false;
    }

    let currency = symbol.substr(3, 3);
    if (!IQFeedSymbology.isInList(currency, this.g_IQfeed_Currencies)) {
      return false;
    }

    // # Size is 'O' or 'K' today, 3/21/2012
    if (symbol.length > 6) {
      let size = symbol.substr(6, 1);
      if (size != "O" && size != "K") {
        return false;
      }
    }
    else {
      return false;
    }

    return true;
  }

  //=item

  //The symbology for TenFore Coin symbols is
  //"[COIN CODE][CURRENCY]C.[CONTRIBUTOR]"

  //For Example: BRIUSDC.COMP (Britannia Coin USD Composite)
  //=cut

  public static isCoinSymbol(symbol: string): boolean {
    symbol = symbol.toUpperCase();

    let r: RegExp;

    // # Is there a contributor part?
    r = /\./;

    if (r.test(symbol)) {
      let dotIndex = symbol.indexOf('.');
      let contributor = symbol.substr(dotIndex + 1).trim();
      // $contributor =~ s/\s*$//;    # there are trailing spaces in mktsymbols_v2.txt
      if (!IQFeedSymbology.isInList(contributor, this.g_IQfeed_forexContributors)) {
        return false;
      }

      // # Trim the contributor part
      symbol = symbol.substr(0, symbol.indexOf('.'));
    }

    if (symbol.length > 7) {
      return false;
    }

    let coinCode = symbol.substr(0, 3);
    if (!IQFeedSymbology.isInList(coinCode, this.g_IQfeed_Coins)) {
      return false;
    }

    let currency = symbol.substr(3, 3);
    if (!IQFeedSymbology.isInList(currency, this.g_IQfeed_Currencies)) {
      return false;
    }

    if (symbol.length > 6) {
      let C = symbol.substr(6, 1);
      if (C != "C") {
        return false;
      }
    }
    else {
      return false;
    }
    return true;
  }

  //=item
  //IQFeed.net Options Symbols

  //The Options Symbology key consists of the following data elements:

  //Security Symbol
  //The security symbol will have a maximum of 5 characters and may contain digits.
  //In those instances where there is a 5 character underlying with a corporate
  //action, the symbol will be converted into a 5 character symbol.

  //Expiration Year
  //The expiration year will have a maximum of 2 digits (example: year 2009 = 09)

  //Expiration Date
  //The expiration date will have a maximum of 2 digits.

  //Expiration Month
  //The expiration month is a single character code and represents month/put/call.

  //The expiration month is a single character code and represents month/put/call.

  //Month Codes
  //# my $callsMonths = '[ABCDEFGHIJKL]';
  //# my $putsMonths  = '[MNOPQRSTUVWX]';

  //Strike Price
  //The strike price will have a maximum of 7 characters with leading/trailing 0s
  //trimmed and decimal point included if necessary

  //Here are some examples of the current options symbol format.

  //SYMBOL  DESCRIPTION
  //MOT0917J7
  //    Motorola October Call at 7.00 Expiring on 10/17/09
  //FDX0918S22.5
  //    Fed Ex August Put at 22.50 Expiring on 07/18/09
  //CSCO1016M25
  //    Cisco January Put at 25.00 Expiring on 01/16/10
  //AMZN1017D110
  //    Amazon April Call at 110.00 Expiring on 04/17/10
  //MSFT1020B25
  //    Microsoft February Call at 25.000 Expiring on 02/20/10

  //=cut

  public static isOptionSymbol(symbol: string): boolean {
    const r: RegExp = /^\w{1,5}\d\d\d\d([ABCDEFGHIJKL]|[MNOPQRSTUVWX])[\d\.]{1,7}$/;
    return r.test(symbol.toUpperCase());
  }

  //=item

  //IQFeed isMutualFund:

  //U.S. Mutual Funds
  //-----------------
  //5 letter symbols that end in the letter 'X'
  //Examples:
  //GTGEX
  //FIIIX
  //BEONX
  //=cut

  public static isMutualFund(symbol: string): boolean {
    const r: RegExp = /^[A-Z]{3}[A-WYZ]X$/;
    return r.test(symbol.toUpperCase());
  }

  //=item

  //IQFeed isMoneyMarketFund:

  //U.S. Money Market Funds
  //-----------------------
  //5 letter symbols that end in the letters "XX"
  //Examples:
  //FRTXX
  //FMDXX
  //MRRXX 
  //=cut

  public static isMoneyMarketFund(symbol: string): boolean {
    const r: RegExp = /^[A-Z]{3}XX$/;
    return r.test(symbol.toUpperCase());
  }

  //=item

  //IQFeed isFutureRoot

  //=cut

  public static isFutureRoot(symbol: string): boolean {
    symbol = symbol.toUpperCase();
    return symbol.substr(symbol.length - 1, 1) == "#";
  }

  /// <summary>
  /// Finds the future root. Usable only for symbols having 2 years digits!
  /// Original perl procedure works with Collective2 symbols having 1 year digit.
  /// </summary>
  /// <param name="symbol">The symbol.</param>
  /// <returns></returns>
  public static findFutureRoot(symbol: string): string {
    // Contract.Assume(iqfeed_symbology.isFutureSymbol(symbol), String.Format("C2Explorer future symbol expected: {0}", symbol));

    let length = symbol.length;

    if (length < 2) {
      return (symbol);
    }

    let root = symbol.substr(0, length - 3);

    return (root);
  }

  //=item

  //IQFeed help: Most Index symbols end with a ".X". If the Index is traded on the CBOE, 
  //then the symbol ends in ".XO". Calculated Indicators and Market Statistics end in ".Z". 

  //=cut

  public static isIndexSymbol(symbol: string): boolean {
    symbol = symbol.toUpperCase();
    let len = symbol.length;

    let result =
      (symbol.length > 2 && (symbol.substr(len - 2) == ".X")) ||

      (symbol.length > 3) && (
        (symbol.substr(len - 3) == ".XO")
        // # Well, IQFeed documentation is stale. '.XA', '.XI' and '.XC' are indexes too. (Except 'C.XI')
        || (symbol.substr(len - 3) == ".XA") || (symbol.substr(len - 3) == ".XC") || (symbol.substr(len - 3) == ".XI")
      );
    //# Exceptions found in IQFeed list of symbols mktsymbols_v2.txt on 2012-03-21
    //#'C.ACO.X', 'ATCO LTD', 'CANADIAN', 'TSE', 'EQUITY', '', '', '0'
    //#'C.CU.X', 'CANADIAN UTILITIES LTD', 'CANADIAN', 'TSE', 'EQUITY', '', '', '0'
    //#'C.X', 'TSX GROUP INC', 'CANADIAN', 'TSE', 'EQUITY', '', '', '0'
    //#'C.YCM.X', 'COMMERCE SPLIT CORP CLASS A', 'CANADIAN', 'TSE', 'EQUITY', '', '', '0'
    //#'C.XI', 'EXCLAMATION INVESTMENTS CORP', 'CANADIAN', 'CVE', 'EQUITY', '', '', '0'
    if (symbol == "C.ACO.X" || symbol == "C.CU.X" || symbol == "C.X" || symbol == "C.YCM.X" || symbol == "C.XI") {
      result = false;
    }
    return result;
  }

  //=item

  //IQFeed help: Most Index symbols end with a ".X". If the Index is traded on the CBOE, 
  //then the symbol ends in ".XO". Calculated Indicators and Market Statistics end in ".Z". 

  //=cut

  public static isMarketStatisticsSymbol(symbol: string): boolean {
    symbol = symbol.toUpperCase();

    //# Exceptions found in IQFeed list of symbols mktsymbols_v2.txt on 2012-03-21
    //# 'C.Z', 'STRONGHOLD METALS INC', 'CANADIAN', 'CVE', 'EQUITY', '', '', '0'
    return symbol.length > 2 && (symbol.substr(symbol.length - 2) == ".Z") && (symbol != "C.Z");
  }

  public static isBondSymbol(symbol: string): boolean {
    symbol = symbol.toUpperCase();
    if (symbol == "C.DBV") {
      return false;
    }
    let r: RegExp;
    r = /^M\.\d/;
    if (r.test(symbol)) {
      return true;
    }
    r = /(\.CB|\.DB|\.DB[A-KUV])$/;
    return r.test(symbol);
  }

  //# Update 2013-09-06:
  //# See http://usa.collective2.com/cgi-perl/board.mpl?want=listmsgs&boardid=9323885&message=82862722&fromelse=1#msgdirect82862722
  //# tom balabanov wrote: there seems to be something wrong with the order system for canadian unit trusts. 
  //# I try to BTO TSX.BYD.UN, this is found but when I put the order in, it thinks it is a Forex and i cannot go further
  //# it fails for all canadian unit trusts. These are stocks just like the regular ones and are traded as such.
  public static isCanadianBondSymbol(symbol: string): boolean {
    symbol = symbol.toUpperCase();

    if (symbol.substr(0, 2) != "C.") {
      return false;
    }

    if (this.isBondSymbol(symbol)) {
      return false;
    }

    //            int dotIndex = rindex( $symbol, '.');

    let dotIndex = symbol.lastIndexOf('.');
    let suffix = symbol.substr(dotIndex + 1);
    return (suffix == "DB")
      || (suffix == "DBA")
      || (suffix == "DBB")
      || (suffix == "DBC")
      || (suffix == "DBH")
      || (suffix == "DBU")
      ;
  }

  public static isCanadianSymbol(symbol: string) {
    symbol = symbol.toUpperCase();
    if (IQFeedSymbology.isBondSymbol(symbol) || IQFeedSymbology.isIndexSymbol(symbol)) {
      return false;
    }

    if (symbol == "C.NMI,DB") {
      return false;
    }

    return symbol.length > 2 && symbol.substr(0, 2) == "C.";
  }

  public static isForward(symbol: string): boolean {
    const r: RegExp = /^M\.\w/
    return r.test(symbol.toUpperCase());
  }


  //=item

  //getInstrumentType
  //Returns
  //        'stock'
  //        'forex'
  //        'future'
  //        'option'
  //        'mutual'
  //        or empty string for other instruments (if you need them you can add them easily) 
  //        or empty string if the instrument cannot be determined.
  //=cut


  public static getInstrumentType(symbol: string): string {
    symbol = symbol.toUpperCase();
    if (IQFeedSymbology.isStockSymbol(symbol)) {
      return "stock";
    }
    else if (IQFeedSymbology.isForexSymbolExtended(symbol)) {
      return "forex";
    }
    else if (IQFeedSymbology.isFutureSymbol(symbol)) {
      return "future";
    }
    else if (IQFeedSymbology.isOptionSymbol(symbol)) {
      return "option";
    }
    else if (IQFeedSymbology.isMutualFund(symbol)) {
      return "mutual";
    }
    else if (IQFeedSymbology.isIndexSymbol(symbol)) {
      return "index";
    }
    else if (IQFeedSymbology.isForward(symbol)) {
      return "forward";
    }
    return IQFeedSymbology.UNKNOWN_INSTRUMENT;
  }

  public static getInstrumentEnum(symbol: string): Instrument {
    symbol = symbol.toUpperCase();
    if (IQFeedSymbology.isStockSymbol(symbol)) {
      return Instrument.stock;
    }
    else if (IQFeedSymbology.isForexSymbolExtended(symbol)) {
      return Instrument.forex;
    }
    else if (IQFeedSymbology.isFutureSymbol(symbol)) {
      return Instrument.future;
    }
    else if (IQFeedSymbology.isOptionSymbol(symbol)) {
      return Instrument.option;
    }
    else if (IQFeedSymbology.isMutualFund(symbol)) {
      return Instrument.mutual;
    }
    else if (IQFeedSymbology.isIndexSymbol(symbol)) {
      return Instrument.index;
    }
    else if (IQFeedSymbology.isForward(symbol)) {
      return Instrument.forward;
    }
    return Instrument.unknown;
  }


}
