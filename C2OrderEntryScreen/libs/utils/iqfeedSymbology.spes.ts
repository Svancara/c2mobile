import { Regex } from "../../node_modules/typescript-dotnet-commonjs/System/Text/RegularExpressions";
import { IQFeedSymbology } from "./iqfeedSymbology";

describe('IQFeed Symbology', () => {
    
  describe('should recognize futures', () => {

    it('expects a valid future', () => {
      expect(IQFeedSymbology.isFutureSymbol("@ZWTN18")).toBeTruthy();
      expect(IQFeedSymbology.isFutureSymbol("@ABN13")).toBeTruthy();
      expect(!IQFeedSymbology.isFutureSymbol("XCRA1615G10")).toBeTruthy();
      expect(IQFeedSymbology.isFutureSymbol("+BOF13")).toBeTruthy();
      expect(IQFeedSymbology.isFutureSymbol("ZSK12")).toBeTruthy();
      expect(!IQFeedSymbology.isFutureSymbol("QCLG3")).toBeTruthy();
      expect(!IQFeedSymbology.isFutureSymbol("@ACG13-@ACJ13")).toBeTruthy();
    });
    
  });
});

