import { Regex } from '../../node_modules/typescript-dotnet-commonjs/System/Text/RegularExpressions';
import { IQFeedSymbology } from './iqfeedSymbology';

describe('IQFeed Symbology', () => {

  describe('isMutualFund_test()', () => {

    it('expects a valid test', () => {
      // # AACTX    AMERICAN FDS 2020 TARGET DATE RETIREMENT FD CL A    NASDAQ  NASDAQ  MUTUAL
      expect(IQFeedSymbology.isMutualFund("AACTX")).toBeTruthy();
      expect(!IQFeedSymbology.isMutualFund("A1CTX")).toBeTruthy();
      expect(!IQFeedSymbology.isMutualFund("AACXX")).toBeTruthy();
      expect(!IQFeedSymbology.isMutualFund("AACTY")).toBeTruthy();
      expect(!IQFeedSymbology.isMutualFund("AACX")).toBeTruthy();
      expect(!IQFeedSymbology.isMutualFund("AACXXX")).toBeTruthy();
    });
  });


  describe('isMoneyMarketFund_test()', () => {

    it('expects a valid test', () => {
      //# AACXX   ACTIVE ASSETS CALIFORNIA TAX-FREE TRUST NASDAQ  NASDAQ  MONEY
      expect(IQFeedSymbology.isMoneyMarketFund("AACXX")).toBeTruthy();
      expect(IQFeedSymbology.isMoneyMarketFund("FRTXX")).toBeTruthy();
      expect(!IQFeedSymbology.isMoneyMarketFund("A1AXX")).toBeTruthy();
      expect(!IQFeedSymbology.isMoneyMarketFund("AACYX")).toBeTruthy();
      expect(!IQFeedSymbology.isMoneyMarketFund("AACXZ")).toBeTruthy();
      expect(!IQFeedSymbology.isMoneyMarketFund("AACZZ")).toBeTruthy();
      expect(!IQFeedSymbology.isMoneyMarketFund("AAXX")).toBeTruthy();
      expect(!IQFeedSymbology.isMoneyMarketFund("AAXXXX")).toBeTruthy();
      expect(!IQFeedSymbology.isMoneyMarketFund("AAXXXXX")).toBeTruthy();
    });
  });

  describe('isFutureRoot_test()', () => {

    it('expects a valid test', () => {
      //# +BO#  SOYBEAN OIL MARCH 2012  CBOT    CBOT    FUTURE
      //# ZS# LME-SGX ZINC FEBRUARY 2012  SGX SGX FUTURE
      expect(IQFeedSymbology.isFutureRoot("+BO#")).toBeTruthy();
      expect(IQFeedSymbology.isFutureRoot("ZS#")).toBeTruthy();
      expect(!IQFeedSymbology.isFutureRoot("ZS#X")).toBeTruthy();
    });
  });

  describe('isStockSymbol_test()', () => {

    it('expects a valid test', () => {
      expect(IQFeedSymbology.isStockSymbol("AIG+")).toBeTruthy();

      expect(!IQFeedSymbology.isStockSymbol("AMG16")).toBeTruthy();
      expect(IQFeedSymbology.isStockSymbol("BBT3814714.PK")).toBeTruthy();
      expect(IQFeedSymbology.isStockSymbol("AFSI-D")).toBeTruthy();
      expect(!IQFeedSymbology.isStockSymbol("AEDPKR.COMP")).toBeTruthy();
      expect(IQFeedSymbology.isStockSymbol("A")).toBeTruthy();
      expect(IQFeedSymbology.isStockSymbol("AEP-A")).toBeTruthy();
      expect(IQFeedSymbology.isStockSymbol("GLAD-A")).toBeTruthy();
      expect(IQFeedSymbology.isStockSymbol("BRK.A")).toBeTruthy();
      expect(IQFeedSymbology.isStockSymbol("C.CLS")).toBeTruthy();
      expect(!IQFeedSymbology.isStockSymbol("AACTX")).toBeTruthy();
      expect(!IQFeedSymbology.isStockSymbol("AACXX")).toBeTruthy();
      expect(!IQFeedSymbology.isStockSymbol("ZS#")).toBeTruthy();
      expect(!IQFeedSymbology.isStockSymbol("ZS#X")).toBeTruthy();
      expect(IQFeedSymbology.isStockSymbol("C.ACO.X")).toBeTruthy();
      expect(IQFeedSymbology.isStockSymbol("C.CU.X")).toBeTruthy();
      expect(IQFeedSymbology.isStockSymbol("C.X")).toBeTruthy();
      expect(IQFeedSymbology.isStockSymbol("C.YCM.X")).toBeTruthy();

      expect(IQFeedSymbology.isStockSymbol("C.AA")).toBeTruthy();
      expect(!IQFeedSymbology.isStockSymbol("C.10B.CB")).toBeTruthy();
    });
  });

  describe('isIndexSymbol_test()', () => {

    it('expects a valid test', () => {
      expect(IQFeedSymbology.isIndexSymbol("ABC.X")).toBeTruthy();
      expect(IQFeedSymbology.isIndexSymbol("ABC.XO")).toBeTruthy();
      expect(!IQFeedSymbology.isIndexSymbol("ABC.Z")).toBeTruthy();
      expect(!IQFeedSymbology.isIndexSymbol("C.ACO.X")).toBeTruthy();
      expect(!IQFeedSymbology.isIndexSymbol("C.ACO.XX")).toBeTruthy();
      expect(!IQFeedSymbology.isIndexSymbol("C.ACO.XXX")).toBeTruthy();
      expect(!IQFeedSymbology.isIndexSymbol("C.X")).toBeTruthy();
      expect(!IQFeedSymbology.isIndexSymbol("MSFT")).toBeTruthy();
    });
  });

  describe('isMarketStatisticsSymbol_test()', () => {

    it('expects a valid test', () => {
      expect(IQFeedSymbology.isMarketStatisticsSymbol("A.Z")).toBeTruthy();
      expect(!IQFeedSymbology.isMarketStatisticsSymbol("A.X")).toBeTruthy();
      expect(!IQFeedSymbology.isMarketStatisticsSymbol("C.Z")).toBeTruthy();
      expect(!IQFeedSymbology.isMarketStatisticsSymbol("C.ZZ")).toBeTruthy();
      expect(!IQFeedSymbology.isMarketStatisticsSymbol("MSFT")).toBeTruthy();

    });
  });

  describe('isBondSymbol_test()', () => {

    it('expects a valid test', () => {
      expect(IQFeedSymbology.isBondSymbol("A13.CB")).toBeTruthy();
      //expect(IQFeedSymbology.isCanadianBondSymbol("C.CUS.DBC"), "C.CUS.DBC is Canadian");

    });
  });

  describe('isCanadianSymbol_test()', () => {

    it('expects a valid test', () => {
      expect(IQFeedSymbology.isCanadianSymbol("C.AA")).toBeTruthy();
      expect(!IQFeedSymbology.isCanadianSymbol("C.10B.CB")).toBeTruthy();
    });
  });

  describe('isCoinSymbol_test()', () => {

    it('expects a valid test', () => {
      expect(IQFeedSymbology.isCoinSymbol("ARECHFC.UBSW")).toBeTruthy();
      expect(IQFeedSymbology.isCoinSymbol("M5PUSDC.UBSW")).toBeTruthy();
      expect(!IQFeedSymbology.isCoinSymbol("M5PUSD.UBSW")).toBeTruthy();
      expect(!IQFeedSymbology.isCoinSymbol("BLABLA.UBSW")).toBeTruthy();
    });
  });

  describe('isPreciousMetal_test()', () => {

    it('expects a valid test', () => {
      expect(IQFeedSymbology.isPreciousMetalSymbol("XAGUSDO.COMP")).toBeTruthy();
      expect(IQFeedSymbology.isPreciousMetalSymbol("XAGUSDO")).toBeTruthy();
      expect(!IQFeedSymbology.isPreciousMetalSymbol("XAGUSD")).toBeTruthy();
      expect(!IQFeedSymbology.isPreciousMetalSymbol("XAGCX")).toBeTruthy();
      expect(!IQFeedSymbology.isPreciousMetalSymbol("XAGDX")).toBeTruthy();
      expect(!IQFeedSymbology.isPreciousMetalSymbol("XAGUSD.COMP")).toBeTruthy();
    });
  });

  describe(' isFutureSymbol_test()', () => {

    it('expects a valid test', () => {
      expect(IQFeedSymbology.isFutureSymbol("@ZWTN18")).toBeTruthy();
      expect(IQFeedSymbology.isFutureSymbol("@ABN13")).toBeTruthy();
      expect(!IQFeedSymbology.isFutureSymbol("XCRA1615G10")).toBeTruthy();
      expect(IQFeedSymbology.isFutureSymbol("+BOF13")).toBeTruthy();
      expect(IQFeedSymbology.isFutureSymbol("ZSK12")).toBeTruthy();
      expect(!IQFeedSymbology.isFutureSymbol("QCLG3")).toBeTruthy();
      expect(!IQFeedSymbology.isFutureSymbol("@ACG13-@ACJ13")).toBeTruthy();
    });
  });

  describe('isOptionSymbol_test()', () => {

    it('expects a valid test', () => {
      expect(IQFeedSymbology.isOptionSymbol("MOT0917J7")).toBeTruthy();
      expect(IQFeedSymbology.isOptionSymbol("FDX0918S22.5")).toBeTruthy();
      expect(IQFeedSymbology.isOptionSymbol("CSCO1016M25")).toBeTruthy();
      expect(IQFeedSymbology.isOptionSymbol("AMZN1017D110")).toBeTruthy();
      expect(IQFeedSymbology.isOptionSymbol("MSFT1020B25")).toBeTruthy();
      expect(IQFeedSymbology.isOptionSymbol("MSFT1020B25.")).toBeTruthy();

      expect(!IQFeedSymbology.isOptionSymbol("MSFT1020B12345678")).toBeTruthy(); //, "MSFT1020B12345678 is not OPTION");
      expect(!IQFeedSymbology.isOptionSymbol("MSFTXX1020B12")).toBeTruthy(); //, "MSFTXX1020B12 is not OPTION");
      expect(!IQFeedSymbology.isOptionSymbol("MSFTA020B12")).toBeTruthy(); //, "MSFTA020B12 is not OPTION");
      expect(!IQFeedSymbology.isOptionSymbol("MSFT102BB12")).toBeTruthy(); //, "MSFT102BB12 is not OPTION");
    });
  });

  describe('getInstrumentType_test()', () => {

    it('expects a valid test', () => {
      expect("stock").toBe(IQFeedSymbology.getInstrumentType("AEP-A"));//, "AEP-A is EQUITY");
      expect("stock").toBe(IQFeedSymbology.getInstrumentType("BRK.A"));//, "BRK.A is EQUITY");
      expect("stock").toBe(IQFeedSymbology.getInstrumentType("C.CLS"));//, "C.CLS is EQUITY");
      expect(IQFeedSymbology.UNKNOWN_INSTRUMENT).toBe(IQFeedSymbology.getInstrumentType("1MSFT"));//, "1MSFT is not EQUITY");
      expect(IQFeedSymbology.UNKNOWN_INSTRUMENT).toBe(IQFeedSymbology.getInstrumentType("ZS#"));//, "ZS# is not EQUITY");
      expect("forex").toBe(IQFeedSymbology.getInstrumentType("AUDUSD"));//, "AUDUSD is FOREX"));//;
    expect(IQFeedSymbology.UNKNOWN_INSTRUMENT).toBe(IQFeedSymbology.getInstrumentType("USDUSD"));//, "USDUSD is not FOREX");
    expect("future").toBe(IQFeedSymbology.getInstrumentType("+BOF13"));//, "+BOF13 is FUTURE");
    expect(IQFeedSymbology.UNKNOWN_INSTRUMENT).toBe(IQFeedSymbology.getInstrumentType("@ACG13-@ACJ13"));//, "@ACG13-@ACJ13# is not FUTURE (but SPREAD)");
    expect("option").toBe(IQFeedSymbology.getInstrumentType("MOT0917J7"));//, "MOT0917J7 is OPTION");
    expect(IQFeedSymbology.UNKNOWN_INSTRUMENT).toBe(IQFeedSymbology.getInstrumentType("MSFT1020B12345678"));//, "MSFT1020B12345678 is not OPTION");
    expect("mutual").toBe(IQFeedSymbology.getInstrumentType("AACTX"));//, "AACTX is MUTUAL");
      //Assert.AreEqual(IQFeedSymbology.UNKNOWN_INSTRUMENT, IQFeedSymbology.getInstrumentType("A1CTX"), "A1CTX is not MUTUAL");
    });
  });


  describe('should recognize futures', () => {

    it('expects a valid future', () => {
      expect(IQFeedSymbology.isFutureSymbol('@ZWTN18')).toBeTruthy();
      expect(IQFeedSymbology.isFutureSymbol('@ABN13')).toBeTruthy();
      expect(!IQFeedSymbology.isFutureSymbol('XCRA1615G10')).toBeTruthy();
      expect(IQFeedSymbology.isFutureSymbol('+BOF13')).toBeTruthy();
      expect(IQFeedSymbology.isFutureSymbol('ZSK12')).toBeTruthy();
      expect(!IQFeedSymbology.isFutureSymbol('QCLG3')).toBeTruthy();
      expect(!IQFeedSymbology.isFutureSymbol('@ACG13-@ACJ13')).toBeTruthy();
    });
  });

  describe('should recognize forex - part 1', () => {

    it('expects forex  - part 1', () => {
      expect(IQFeedSymbology.isForexSymbolExtended("UDDMYRSN.COMP")).toBeTruthy();
      expect(IQFeedSymbology.isForexSymbolExtended("AEDPKR.COMP")).toBeTruthy();
      expect(IQFeedSymbology.isForexSymbolExtended("AUDUSD")).toBeTruthy();
      expect(IQFeedSymbology.isForexSymbolExtended("AUDCAD")).toBeTruthy();
      expect(IQFeedSymbology.isForexSymbolExtended("USDSEK")).toBeTruthy();
      expect(IQFeedSymbology.isForexSymbolExtended("ZMKZAR.EUAM")).toBeTruthy();
      expect(IQFeedSymbology.isForexSymbolExtended("USDEUR.COMP")).toBeTruthy();
      expect(IQFeedSymbology.isForexSymbolExtended("CADJPY12M.COMP")).toBeTruthy();
      expect(!IQFeedSymbology.isForexSymbolExtended("USDEUR")).toBeTruthy();
      expect(!IQFeedSymbology.isForexSymbolExtended("MSFT")).toBeTruthy();
      expect(!IQFeedSymbology.isForexSymbolExtended("USDEURX.COMP")).toBeTruthy();
    });
  });

  describe('should recognize forex - part 2', () => {

    it('expects forex - part 2', () => {
      expect(IQFeedSymbology.isForexSymbolExtended("AUDUSD")).toBeTruthy();
      expect(IQFeedSymbology.isForexSymbolExtended("AUDCAD")).toBeTruthy();
      expect(IQFeedSymbology.isForexSymbolExtended("USDSEK")).toBeTruthy();
      expect(!IQFeedSymbology.isForexSymbolExtended("USDEUR")).toBeTruthy();
      expect(IQFeedSymbology.isForexSymbolExtended("USDEUR.COMP")).toBeTruthy();
      expect(IQFeedSymbology.isForexSymbolExtended("CADJPY12M.COMP")).toBeTruthy();
      expect(IQFeedSymbology.isForexSymbolExtended("USDSEK.MBT")).toBeTruthy();
      expect(IQFeedSymbology.isForexSymbolExtended("ZMKZAR.EUAM")).toBeTruthy();
      expect(IQFeedSymbology.isForexSymbolExtended("BUSDXCD")).toBeTruthy();
      expect(IQFeedSymbology.isForexSymbolExtended("BUSDJPY2W")).toBeTruthy();
      expect(IQFeedSymbology.isForexSymbolExtended("ZARMZN.EUAM")).toBeTruthy();
      expect(IQFeedSymbology.isForexSymbolExtended("USDSEK.MBT ")).toBeTruthy();
      expect(IQFeedSymbology.isForexSymbolExtended("BUSDSEKTN")).toBeTruthy();
      expect(IQFeedSymbology.isForexSymbolExtended("BUSDSEKSN")).toBeTruthy();
      expect(IQFeedSymbology.isForexSymbolExtended("BUSDSEKON")).toBeTruthy();
      expect(IQFeedSymbology.isForexSymbolExtended("CADUSDNOR.BOC")).toBeTruthy();
      expect(!IQFeedSymbology.isForexSymbolExtended("MSFT")).toBeTruthy();
      expect(!IQFeedSymbology.isForexSymbolExtended("USDEURX.COMP")).toBeTruthy();
      expect(!IQFeedSymbology.isForexSymbolExtended("USDEURXXX.COMP")).toBeTruthy();
      expect(!IQFeedSymbology.isForexSymbolExtended("USDEUR.X")).toBeTruthy();
    });
  });
});

