import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PositionsSandbox } from './positions.sandbox';

@NgModule({
  imports: [
    CommonModule
    ],
  providers: [PositionsSandbox]

})
export class CommonPositionsModule {
}
