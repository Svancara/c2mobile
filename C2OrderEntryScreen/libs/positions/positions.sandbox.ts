import { Injectable } from "@angular/core";
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { Sandbox } from '@app/shared/sandbox/base.sandbox';

import {
  Load as loadPositionsAction,
  Remove as ClosePositionAction
} from '@app/store/actions/positions/positions.collection';

import * as fromStoreReducers from '@app/store';
import { getAllPositions, getCollectionLoading } from '@app/store/reducers/positions';
import { getSelectedStrategy } from '@app/store/reducers/user-strategies/';
import { Remove } from '@app/store/actions/positions/positions.collection';

import { Position, IStrategy, Strategy } from '@app/models';

@Injectable()
export class PositionsSandbox extends Sandbox {

  // For C2 Entry Screen (desktop version)
  public orderScreenStrategyId$: Observable<number>;

  // For C2 Mobile 
  public positions$: Observable<Position[]>;

  public loading$: Observable<boolean>;
  public strategy$: Observable<IStrategy>;


  constructor(private store: Store<any>) {
    super(store);

    // Subscribe to strategy ID and name
    this.orderScreenStrategyId$ = this.store.select(fromStoreReducers.getOrderScreenStrategy).map((state: Strategy) => state.system_id);

    this.positions$ = this.store.select(getAllPositions);
    this.loading$ = this.store.select(getCollectionLoading);
    this.strategy$ = this.store.select(getSelectedStrategy);
 
  }

  loadPositions() {
    this.orderScreenStrategyId$.first().subscribe(strategy  => {
      this.store.dispatch(new loadPositionsAction(strategy));
    });
  }

  closePosition(position: Position) {
    this.store.dispatch(new Remove(position));
  }

}
