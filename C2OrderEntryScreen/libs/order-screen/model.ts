import { Message } from "primeng/components/common/message";
import { ISendSignal } from "@app/models";


export enum OrderType { Market, Limit, Stop }
export type Action = "Buy" | "Sell" | "Short" | "Cover";

export interface ValidationResult { valid: boolean; message?: Message; validSignal?: ISendSignal }
