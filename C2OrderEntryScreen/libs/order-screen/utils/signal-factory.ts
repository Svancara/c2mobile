import { C2Action, ISendSignal, SendSignal, InstrumentString, OK } from "@app/models";
import { OrderType, ValidationResult } from "@libs/order-screen/model";
import { Message } from "primeng/components/common/message";

type ValidatorFun = (signal: ISendSignal) => ValidationResult;

export class SignalFactory {

  public _parkUntilSecs_: Date;
  public action: C2Action;
  public limitPrice: number = 0;
  public orderType: OrderType = OrderType.Market;
  public parkuntil: Date;
  public parkuntilChecked: boolean = false;
  public profitTarget: number = 0;
  public quant: number = 0;
  public stopPrice: number = 0;
  public stopLoss: number = 0;
  public strategyId: number = 0;
  public symbol: string = "";
  public tif: boolean = true;
  public typeofsymbol: InstrumentString;

  private validators: ValidatorFun[] = [];

  // Validates data without still unknown signal.action
  private withoutActionValidators: ValidatorFun[] = [];

  constructor() {
    this.withoutActionValidators.push(this.isValidSymbol);
    this.withoutActionValidators.push(this.isValidQuantity);
    this.withoutActionValidators.push(this.isLimitFilled);
    this.withoutActionValidators.push(this.isStopFilled);
    this.withoutActionValidators.push(this.isValidLimit);
    this.withoutActionValidators.push(this.isValidStop);
    this.withoutActionValidators.push(this.isLimitOrStopExclusive);
    this.withoutActionValidators.push(this.isMarketOrderSuspected);

    this.validators.push(this.isValidSymbol);
    this.validators.push(this.isValidQuantity);
    this.validators.push(this.isLimitFilled);
    this.validators.push(this.isStopFilled);
    this.validators.push(this.isValidLimit);
    this.validators.push(this.isValidStop);
    this.validators.push(this.isLimitOrStopExclusive);
    this.validators.push(this.isMarketOrderSuspected);
    this.validators.push(this.isBuyStopLossLessThanStopPrice);
    this.validators.push(this.isBuyStopLossLessThanLimitPrice);
    this.validators.push(this.isBuyProfitTargetGreaterThanLimitPrice);
    this.validators.push(this.isBuyProfitTargetGreaterThanStopPrice);
    this.validators.push(this.isSellStopLossGreaterThanStopPrice);
    this.validators.push(this.isSellStopLossGreaterThanLimitPrice);
    this.validators.push(this.isSellProfitTargetlessThanLimitPrice);
    this.validators.push(this.isSellProfitTargetlessThanStopPrice);

  }


  /**
   * Create a signal fro orde screen data
   */
  public createSignal(): ISendSignal | undefined {

    let result: ISendSignal = new SendSignal();

    result.action = this.action;
    result.strategyId = this.strategyId;
    result.symbol = this.symbol;
    result.quant = this.quant;
    result.typeofsymbol = this.typeofsymbol;




    //% Collective2 does not like market orders posted within seance a day before it is valid for.
    //% (Discussed with MK a few years ago.)
    //% So we send EVERY MARKET order as GTC
    if (this.orderType === OrderType.Market) {
      result.duration = "GTC";
    } else {
      switch (this.tif) {
        case true: result.duration = "DAY"; break;
        case false: result.duration = "GTC"; break;
      }
    }

    result.market = this.orderType === OrderType.Market ? "1" : "0";

    if (this.orderType === OrderType.Limit && this.limitPrice > 0) {
      result.limit = this.limitPrice;
    }

    if (this.orderType === OrderType.Stop && this.stopPrice > 0) {
      result.stop = this.stopPrice;
    }

    if (this.parkuntilChecked) {
      result.parkUntilYYYYMMDDHHMM = this.formatDate(new Date(this.parkuntil));
    }

    if (this.stopLoss > 0) {
      result.stoploss = this.stopLoss;
    }

    if (this.profitTarget > 0) {
      result.profittarget = this.profitTarget;
    }

    return result;
  }

  private formatDate(d: Date): string {
    return ("0" + d.getDate()).slice(-2) + "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" +
      d.getFullYear() + " " + ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2);
  }

  /*
  =====================================================
                    Validation
  =====================================================
   */

  /**
   * Validate 
   */
  validate(): ValidationResult {
    let result: ValidationResult = { valid: false };
    const signal: ISendSignal = this.createSignal();
    result = this.allValid(signal);
    if (result.valid) {
      result.validSignal = signal;
    }
    return result;
  }

  /**
   * Validate without action 
  */
  validateWithoutAction(): ValidationResult {
    let result: ValidationResult = { valid: false };
    const signal: ISendSignal = this.createSignal();
    result = this.allWithoutActionValid(signal);
    if (result.valid) {
      result.validSignal = signal;
    }
    return result;
  }

  /**
   * Run all validators. Stop if any returns error. Return that error.
   * @param signal
   */
  public allValid(signal: ISendSignal): ValidationResult {
    let result: ValidationResult = { valid: true, validSignal: signal };
    this.validators.some(fun => {
      let valResult: ValidationResult = fun(signal);
      if (!valResult.valid) {
        result = valResult;
        return true; // break 'some' cycle
      }
    });
    return result;
  }

  /**
 * Run all validators. Stop if any returns error. Return that error.
 * @param signal
 */
  public allWithoutActionValid(signal: ISendSignal): ValidationResult {
    let result: ValidationResult = { valid: true, validSignal: signal };
    this.withoutActionValidators.some(fun => {
      let valResult: ValidationResult = fun(signal);
      if (!valResult.valid) {
        result = valResult;
        return true; // break 'some' cycle
      }
    });
    return result;
  }

  //public isValid(signal: ISendSignal): ValidationResult {

  //  let result: ValidationResult = this.isValidSymbol(signal);
  //  if (!result.valid) { return result; }

  //  result = this.isValidQuantity(signal)
  //  if (!result.valid) { return result; }

  //  return { valid: true };

  //}

  public isValidSymbol = (signal: ISendSignal): ValidationResult => {
    if (!signal.symbol || signal.symbol.trim().length === 0) {
      return { valid: false, message: { severity: 'error', summary: 'No symbol selected', detail: '' } };
    }
    return { valid: true };
  }

  public isValidQuantity = (signal: ISendSignal): ValidationResult => {

    if (!signal.quant || signal.quant == 0) {
      return { valid: false, message: { severity: 'error', summary: 'Quantity is empty', detail: '' } };
    }

    if (signal.quant < 0) {
      return { valid: false, message: { severity: 'error', summary: 'Negative quantity', detail: '' } };
    }
    return { valid: true };
  }

  public isLimitFilled = (signal: ISendSignal): ValidationResult => {
    if (this.orderType === OrderType.Limit && (!signal.limit || (signal.limit == 0))) {
      return { valid: false, message: { severity: 'error', summary: 'A limit price is not provided for the limit order', detail: 'Limit price must be a positive number' } };
    }
    return { valid: true };
  }

  public isStopFilled = (signal: ISendSignal): ValidationResult => {
    if (this.orderType === OrderType.Stop && (!signal.stop || (signal.stop == 0))) {
      return { valid: false, message: { severity: 'error', summary: 'A stop price is not provided for the stop order', detail: 'Stop price must be a positive number' } };
    }
    return { valid: true };
  }

  public isValidLimit = (signal: ISendSignal): ValidationResult => {
    if (this.orderType === OrderType.Limit && (!signal.limit || (signal.limit && signal.limit <= 0))) {
      return { valid: false, message: { severity: 'error', summary: 'Wrong limit price', detail: 'Limit price must be a positive number' } };
    }
    return { valid: true };
  }

  public isValidStop = (signal: ISendSignal): ValidationResult => {
    if (this.orderType === OrderType.Stop && (!signal.stop || (signal.stop && signal.stop <= 0))) {
      return { valid: false, message: { severity: 'error', summary: 'Wrong stop price', detail: 'Stop price must be a positive number' } };
    }
    return { valid: true };
  }


  public isLimitOrStopExclusive = (signal: ISendSignal): ValidationResult => {
    if (this.orderType === OrderType.Stop && (signal.limit && signal.limit != 0)) {
      return { valid: false, message: { severity: 'error', summary: 'Suspicious data', detail: 'A limit price filled for the stop order.' } };
    }
    if (this.orderType === OrderType.Limit && (signal.stop && signal.stop != 0)) {
      return { valid: false, message: { severity: 'error', summary: 'Suspicious data', detail: 'A stop price filled for the limit order.' } };
    }
    return { valid: true };
  }

  public isMarketOrderSuspected = (signal: ISendSignal): ValidationResult => {

    if (this.orderType === OrderType.Market && signal.stop && signal.stop != 0) {
      return { valid: false, message: { severity: 'warn', summary: 'Suspicious data', detail: 'Market order, but a stop price entered.' } };
    }
    if (this.orderType === OrderType.Market && signal.limit && signal.limit != 0) {
      return { valid: false, message: { severity: 'warn', summary: 'Suspicious data', detail: 'Market order, but a limit price entered.' } };
    }

    return { valid: true };
  }


  public isBuyStopLossLessThanLimitPrice = (signal: ISendSignal): ValidationResult => {
    /*
           /----- Profit target
          /--- Stop 
    Buy -<-------------------------------
          \--- Limit
           \---- Stop loss
    */

    if (signal.action === C2Action.BTC || signal.action === C2Action.BTO) {
      if (this.orderType === OrderType.Limit // then limit is filled
        && (signal.stoploss && signal.stoploss > 0)
        && signal.stoploss >= signal.limit) {
        return {
          valid: false, message: {
            severity: 'warn',
            summary: 'Wrong stop loss price for the stop order',
            detail: `The stop loss price ${signal.stoploss} is ${signal.stoploss > signal.limit ? 'greater than' : 'equal to'} the limit price ${signal.limit}`
          }
        };
      }
    }

    return { valid: true };
  }

  public isBuyStopLossLessThanStopPrice = (signal: ISendSignal): ValidationResult => {
    /*
           /----- Profit target
          /--- Stop 
    Buy -<-------------------------------
          \--- Limit
           \---- Stop loss
    */

    if (signal.action === C2Action.BTC || signal.action === C2Action.BTO) {
      if (this.orderType === OrderType.Stop // then stop is filled
        && (signal.stoploss && signal.stoploss > 0)
        && signal.stoploss >= signal.stop) {
        return {
          valid: false, message: {
            severity: 'warn',
            summary: 'Wrong stop loss price for the stop order',
            detail: `The stop loss price ${signal.stoploss} is ${signal.stoploss > signal.stop ? 'greater than' : 'equal to'} the stop price ${signal.stop}`
          }
        };
      }
    }
    return { valid: true };
  }

  public isBuyProfitTargetGreaterThanLimitPrice = (signal: ISendSignal): ValidationResult => {
    /*
           /----- Profit target
          /--- Stop 
    Buy -<-------------------------------
          \--- Limit
           \---- Stop loss
    */
    if (signal.action === C2Action.BTC || signal.action === C2Action.BTO) {
      if (this.orderType === OrderType.Limit // then limit is filled
        && (signal.profittarget && signal.profittarget > 0)
        && signal.profittarget <= signal.limit) {
        return {
          valid: false, message: {
            severity: 'warn',
            summary: 'Wrong profit target price for the limit order',
            detail: `The profit target price ${signal.profittarget} is ${signal.profittarget < signal.limit ? 'lesser than' : 'equal to'} the limit price ${signal.limit}`
          }
        };
      }
    }

    return { valid: true };
  }

  public isBuyProfitTargetGreaterThanStopPrice = (signal: ISendSignal): ValidationResult => {
    /*
           /----- Profit target
          /--- Stop 
    Buy -<-------------------------------
          \--- Limit
           \---- Stop loss
    */

    if (signal.action === C2Action.BTC || signal.action === C2Action.BTO) {
      if (this.orderType === OrderType.Stop // then stop is filled
        && (signal.profittarget && signal.profittarget > 0)
        && signal.profittarget <= signal.stop) {
        return {
          valid: false, message: {
            severity: 'warn',
            summary: 'Wrong profit target price for the stop order',
            detail: `The profit target price ${signal.profittarget} is ${signal.profittarget < signal.stop ? 'lesser than' : 'equal to'} the stop price ${signal.stop}`
          }
        };
      }
    }

    return { valid: true };
  }

  public isSellStopLossGreaterThanLimitPrice = (signal: ISendSignal): ValidationResult => {
    /*
           /----- Stop loss
          /--- Limit
    Sell -<-------------------------------
          \--- Stop
           \---- Profit target
    */

    if (signal.action === C2Action.STC || signal.action === C2Action.STO || signal.action === C2Action.SSHORT) {
      if (this.orderType === OrderType.Limit // then limit is filled
        && (signal.stoploss && signal.stoploss > 0)
        && signal.stoploss <= signal.limit) {
        return {
          valid: false, message: {
            severity: 'warn',
            summary: 'Wrong stop loss price for the limit order',
            detail: `The stop loss price ${signal.stoploss} is ${signal.stoploss < signal.limit ? 'lesser than' : 'equal to'} the limit price ${signal.limit}`
          }
        };
      }
    }

    return { valid: true };
  }


  public isSellStopLossGreaterThanStopPrice = (signal: ISendSignal): ValidationResult => {
    /*
           /----- Stop loss
          /--- Limit
    Sell -<-------------------------------
          \--- Stop
           \---- Profit target
    */
    if (signal.action === C2Action.STC || signal.action === C2Action.STO || signal.action === C2Action.SSHORT) {
      if (this.orderType === OrderType.Stop // then stop is filled
        && (signal.stoploss && signal.stoploss > 0)
        && signal.stoploss <= signal.stop) {
        return {
          valid: false, message: {
            severity: 'warn',
            summary: 'Wrong stop loss price for the stop order',
            detail: `The stop loss price ${signal.stoploss} is ${signal.stoploss < signal.stop ? 'lesser than' : 'equal to'} the stop price ${signal.stop}`
          }
        };
      }
    }
    return { valid: true };
  }


  public isSellProfitTargetlessThanLimitPrice = (signal: ISendSignal): ValidationResult => {
    /*
           /----- Stop loss
          /--- Limit
    Sell -<-------------------------------
          \--- Stop
           \---- Profit target
    */

    if (signal.action === C2Action.STC || signal.action === C2Action.STO || signal.action === C2Action.SSHORT) {
      if (this.orderType === OrderType.Limit // then limit is filled
        && (signal.profittarget && signal.profittarget > 0)
        && signal.profittarget >= signal.limit) {
        return {
          valid: false, message: {
            severity: 'warn',
            summary: 'Wrong profit target price for the limit order',
            detail: `The profit target price ${signal.profittarget} is ${signal.profittarget > signal.limit ? 'greater than' : 'equal to'} the limit price ${signal.limit}`
          }
        };
      }
    }

    return { valid: true };
  }


  public isSellProfitTargetlessThanStopPrice = (signal: ISendSignal): ValidationResult => {
    /*
           /----- Stop loss
          /--- Limit
    Sell -<-------------------------------
          \--- Stop
           \---- Profit target
    */

    if (signal.action === C2Action.STC || signal.action === C2Action.STO || signal.action === C2Action.SSHORT) {
      if (this.orderType === OrderType.Stop // then stop is filled
        && (signal.profittarget && signal.profittarget > 0)
        && signal.profittarget >= signal.stop) {
        return {
          valid: false, message: {
            severity: 'warn',
            summary: 'Wrong profit target price for the stop order',
            detail: `The profit target price ${signal.profittarget} is ${signal.profittarget > signal.stop ? 'greater than' : 'equal to'} the stop price ${signal.stop}`
          }
        };
      }
    }

    return { valid: true };
  }

}
