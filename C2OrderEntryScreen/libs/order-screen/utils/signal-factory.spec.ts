import { SignalFactory} from '@libs/order-screen/utils/signal-factory';
import { SendSignal } from "@app/models";
import { Message } from 'primeng/components/common/message';
import { ValidationResult } from '@libs/order-screen/model';



describe('Entry Screen Validator', () => {

  let signal: SendSignal;
  let validator: SignalFactory;

  beforeEach(() => {
    signal = new SendSignal();
    validator = new SignalFactory();
  })

  describe('checks signal', () => {
    
    it('has valid symbol', () => {
      let result: ValidationResult;

      result = validator.isValidSymbol(signal);
      expect(result.valid).toBeFalsy();
      expect(result.message).toEqual({ severity: 'error', summary: 'No symbol selected', detail: '' });

      signal.symbol = "MSFT";
      result = validator.isValidSymbol(signal);
      expect(result.valid).toBeTruthy();

    });
    
    it('has valid quantity', () => {
      let result: ValidationResult;

      result = validator.isValidQuantity(signal);
      expect(result.valid).toBeFalsy();
      expect(result.message).toEqual({ severity: 'error', summary: 'Quantity is empty', detail: '' });

      signal.quant = 0.0000;
      result = validator.isValidQuantity(signal);
      expect(result.valid).toBeFalsy();
      expect(result.message).toEqual({ severity: 'error', summary: 'Quantity is empty', detail: '' });

      signal.quant = -100;
      result = validator.isValidQuantity(signal);
      expect(result.valid).toBeFalsy();
      expect(result.message).toEqual({ severity: 'error', summary: 'Negative quantity', detail: '' });

      signal.quant = 1;
      result = validator.isValidQuantity(signal);
      expect(result.valid).toBeTruthy();
    });
    
  });

});


