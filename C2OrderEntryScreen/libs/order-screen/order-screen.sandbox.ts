import { Injectable } from "@angular/core";
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from "rxjs";
import { Sandbox } from '@app/shared/sandbox/base.sandbox';
import { Map } from 'immutable';

import { SignalsService } from '@libs/signals-service/';

import { ISendSignal } from "@app/models";
import { SymbolSearchTypeAheadSupport, ForexSymbol } from '@app/models/LastQuoteService.dtos';
import { GetLastQuote, LastQuoteData, FundamentalDataResponse, GetFundamentalData, InternaSearchSymbolRequest, InstrumentString, IStrategy, Strategy } from '@app/models';

import * as fromStoreReducers from '@app/store';
import * as screenActions from "@app/store/actions/order-screen-actions";
import * as fromGp from '@app/store/actions/gp.action';
import * as fromSearchSymbol from '@app/store/reducers/symbol-search';
import * as fromForex from '@app/store/reducers/search-forex.reducer';
import * as fromStrategies from '@app/store/reducers/user-strategies';
import * as fromOrderScreenStrategy from '@app/store/reducers/order-screen-strategy.reducer';

import { LoadData as getLastQuote } from '@app/store/actions/last-quote.actions';
import { LoadData as getFundamentalData } from '@app/store/actions/fundamental-data.actions';
import { Search } from '@app/store/actions/symbol-search/symbol-search';

@Injectable()
export class OrderScreenSandbox extends Sandbox {

  // For C2 Entry Screen (desktop version)
  public orderScreenStrategyId$: Observable<number>;
  public orderScreenSame$: Observable<string>;

  // For Mobile version (pro)
  public strategyId$: Observable<number>;
  public strategyName$: Observable<string>;

  public lastQuotes$: Observable<LastQuoteData>;
  public fundamentalData$: Observable<FundamentalDataResponse>;
  
  private subscription1: Subscription;
  private subscription2: Subscription;
  private subscription3: Subscription;


  // private lastQuoteRequest: GetLastQuote;


  constructor(
    private c2ApiClient: SignalsService,
    private store: Store<fromStoreReducers.State>,
    private route: ActivatedRoute,
    private router: Router) {
    super(store);

    // Subscribe to strategy ID and name
    this.orderScreenStrategyId$ = this.store.select(fromStoreReducers.getOrderScreenStrategy).map((state : Strategy) => state.system_id);
    this.orderScreenSame$ = this.store.select(fromStoreReducers.getOrderScreenStrategy).map((state: Strategy)  => state.systemName);
    this.strategyId$ = this.store.select(fromStrategies.getSelectedStrategy).map(state => state.system_id);
    this.strategyName$ = this.store.select(fromStrategies.getSelectedStrategy).map(state => state.systemName);

    /*
     ====================================================================
       Inner sandbox behaviour.
       When we receive a message from symbols searching mechanism,
       we catch it and ask for lastQuote and fundamental data.
       It is all we need for the Quotes screen so those data are flowing
       to container (subscribed via async in template) and component.
     ====================================================================
  */

    // React to stock, futures, mutals symbol selected. Ask for last quote and fundamental
    this.subscription1 = this.store
      .select(fromSearchSymbol.getSelectedSymbol).
      subscribe((selectedSymbol: SymbolSearchTypeAheadSupport) => {
        if (selectedSymbol) {
          this.getLastQuote(this.createRequest(selectedSymbol));
        }
      });


    // React to option symbol selected. Ask for last quote and fundamental
    this.subscription2 = this.store
      .select(fromStoreReducers.getSelectedOption)
      .subscribe((selectedSymbol: GetLastQuote) => {
        if (selectedSymbol) {
          this.getLastQuote(selectedSymbol);
        }
      });

    // React to forex symbol selected. Ask for last quote and fundamental
    this.subscription3 = this.store
      .select(fromForex.getSelectedForexSymbol)
      .subscribe((selectedSymbol: GetLastQuote) => {
        if (selectedSymbol) {
          this.getLastQuote(this.createRequestFromForex(selectedSymbol));
        }
      });

    /*
       ====================================================================
        Public behaviour
       ====================================================================
    */

    // React to getLast returning data
    this.lastQuotes$ = this.store.select(fromStoreReducers.getLastQuote).map(
      // (data: Map<string, LastQuoteData>) => {
        //if (this.lastQuoteRequest) {
        //  return data.get(this.lastQuoteRequest.symbol);
        //}
      (data: LastQuoteData) => data);

    // React to getFundamental returning data
    //this.fundamentalData$ = this.store.select(fromStoreReducers.getFundamentalData).map(
    //  (data: Map<string, FundamentalDataResponse>) => {
    //    if (this.lastQuoteRequest) {
    //      return data.get(this.lastQuoteRequest.symbol);
    //    }
    //  }).share();
    this.fundamentalData$ = this.store.select(fromStoreReducers.getFundamentalData).map(
      (data: FundamentalDataResponse) => data).share();
}


  /**
   * Ask For Last Qupote 
   */
  public getLastQuote(request: GetLastQuote): void {
    // this.lastQuoteRequest = request;
    this.store.dispatch(new getLastQuote(request));
    this.store.dispatch(new getFundamentalData(request));
  }

  createRequest(selectedSymbol: SymbolSearchTypeAheadSupport): GetLastQuote {
    let result: GetLastQuote = new GetLastQuote();
    result.market = selectedSymbol.Exchange;
    result.typeofsymbol = selectedSymbol.Instrument;
    result.symbol = selectedSymbol.Symbol;
    return result;
  }

  createRequestFromForex(selectedSymbol: GetLastQuote): GetLastQuote {
    //let result: GetLastQuote = new GetLastQuote();
    //result.market = "US";
    //result.typeofsymbol = InstrumentString.forex;
    //result.symbol = selectedSymbol.Symbol;
    //return result;
    return selectedSymbol;
  }

  /**
   * Search for symbol
   * @param request
   */
  symbolSearch(query: InternaSearchSymbolRequest, navigate: boolean) {

    alert("OrderScreenSandbox.symbolSearch " + query.query);

    this.store.dispatch(new fromGp.SetSearchSymbolPartialQuery(query));
    if (navigate) {
      this.router.navigate(['/searchsymbol']);
    }
    //this.router.navigate(["/searchsymbol"], { queryParams: { returnUrl: this.router.url } });
  }

  /**
   * Send signal
   */
  public sendSignal(signal: ISendSignal): void {
    this.store$.dispatch(new screenActions.Send(signal));
  }
}
