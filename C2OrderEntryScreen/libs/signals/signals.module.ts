import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignalsSandbox } from './signals.sandbox';
@NgModule({
  imports: [CommonModule],
  providers: [SignalsSandbox]
})
export class CommonSignalsModule {
}
