import { Injectable } from "@angular/core";
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { Sandbox } from '@app/shared/sandbox/base.sandbox';

import * as fromStoreReducers from '@app/store';
import * as fromSignals from '@app/store/reducers/signals';
import * as fromStrategies from '@app/store/reducers/user-strategies';
import { Load as loadSignalsAction } from '@app/store/actions/signals/signals.collection.actions';

import { Remove as CancelSignalAction } from '@app/store/actions/signals/signals.collection.actions';

import { Signal, IStrategy, Strategy } from '@app/models';

@Injectable()
export class SignalsSandbox extends Sandbox {

  // For C2 Entry Screen (desktop version)
  public orderScreenStrategyId$: Observable<number>;

  // For C2 Mobile 
  public signals$: Observable<Signal[]>;

  public loading$: Observable<boolean>;
  public strategy$: Observable<IStrategy>;


  constructor(private store: Store<any>) {
    super(store);

    // Subscribe to strategy ID and name
    this.orderScreenStrategyId$ = this.store.select(fromStoreReducers.getOrderScreenStrategy).map((state: Strategy) => state.system_id);

    this.signals$ = this.store.select(fromSignals.getAllSignals);
    this.loading$ = this.store.select(fromSignals.getCollectionLoading);
    this.strategy$ = this.store.select(fromStrategies.getSelectedStrategy);
  
  }

  loadSignals() {
    this.orderScreenStrategyId$.first().subscribe(strategy  => {
      this.store.dispatch(new loadSignalsAction(strategy));
    });
  }

  cancelSignal(signal: Signal) {
    this.store.dispatch(new CancelSignalAction(signal));
  }

}
