import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignalsService } from '@libs/signals-service/';

@NgModule({
  imports: [CommonModule],
  providers: [SignalsService]
})
export class SignalServicesModule {
}
