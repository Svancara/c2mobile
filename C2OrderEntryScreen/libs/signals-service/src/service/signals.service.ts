import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { Signal, SignalsWorking, UserStrategies } from '@app/models';

import { environment } from '@env/environment';
import { Environment, CancelSignalResponse, ICancelSignal, CancelSignalHttpException, ISendSignal, SendSignalPayload, ISendSignalResponse, RequestTradesResponse } from '@app/models';
import { ApiSystemRequest } from '@app/models/apiKey_systemId';
import { State as AuthState } from '@app/store/reducers/auth/auth';
import * as authReducers from '@app/store/reducers/auth';

import { NotificationsService } from 'angular2-notifications';
import { Utils } from '@app/utils';

@Injectable()
export class SignalsService {

  private config: Environment;

  constructor(
    private store: Store<AuthState>,
    private http: HttpClient,
    private notificationsService: NotificationsService) {
    this.config = environment;
  }

  private getApiKey(): string {
    let apikey: string = '';
    this.store.select(authReducers.getApiKey).first().subscribe(apikeyState => apikey = apikeyState);
    return apikey;
    //return Utils.getApiKey(this.store);
  }
  

  /**
   * getPositions
   * @param strategyId
   */
  public getPositions(strategyId: number): Observable<RequestTradesResponse> {
    const url = this.config.C2ApiUrl + "/requestTrades";
    const payload: ApiSystemRequest = { systemid: strategyId, apikey: this.getApiKey() };
    return this.http
      .post<RequestTradesResponse>(url, payload)
      .map(response => {
        let result = response;
        result.strategyId = strategyId;
        return result;
      })
      // No errors handling here. Must be catched in the @Effect.
      ;
  }

  /**
   * getStrategies
   */
  public getStrategies(): Observable<UserStrategies> {
    const url = this.config.C2ApiUrl + "/listAllSystems";
    const payload = { apikey: this.getApiKey() };
    return this.http
      .post<UserStrategies>(url, payload)
      .map(response => response)
      // no .catch(err => this.handleError(err)) no
      ;
  }

  public sendSignal(signal: ISendSignal): Observable<ISendSignalResponse> {
    const url = this.config.C2ApiUrl + '/submitSignal';
    const payload: SendSignalPayload = { systemid: signal.strategyId, apikey: this.getApiKey(), signal: signal };
    return this.http
      .post<ISendSignalResponse>(url, payload)
      .map(response => response)
      // no .catch(err => this.handleError(err)) no
      ;
  }

  public retrieveSignalsWorking(strategyId: number): Observable<SignalsWorking> {
    const url = this.config.C2ApiUrl + "/retrieveSignalsWorking";
    const payload: ApiSystemRequest = { systemid: strategyId, apikey: this.getApiKey() };
    return this.http
      .post<SignalsWorking>(url, payload)
      .map(response => {
        let result = response;
        result.strategyId = strategyId;
        return result;
      })
      // no .catch(err => this.handleError(err))
      ;
  }

  public cancelSignal(signal: Signal): Observable<CancelSignalResponse> {
    const url = this.config.C2ApiUrl + "/cancelSignal";
    const payload: ICancelSignal = { systemid: signal.strategyId, apikey: this.getApiKey(), signalid: signal.signal_id };
    return this.http
      .post<CancelSignalResponse>(url, payload)
      .map(response => response)
      //no .catch(err => this.handleError(err))
      ;
  }


  //private handleError(status: HttpErrorResponse): Observable<any> {
  //  this.notificationsService.error(status.name, status.message, { timeOut: 5000, showProgressBar: true, pauseOnHover: true });
  //  return Observable.throw(status);
  //}

}
