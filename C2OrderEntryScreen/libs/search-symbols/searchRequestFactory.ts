import { InstrumentString } from "@app/models";
import { ValidationResult } from "@libs/order-screen/model";
import { SymbolSearch, OptionSearchByExpiration, ForexPairs } from "@app/models/LastQuoteService.dtos";
import { Utils } from "@app/utils";

export class SearchSymbolRequestFactory {

  /**
  * Instrument must be determined  by the user
  */
  private instrument: InstrumentString;

  /**
  * 'US' for now
  */
  private market: string;

  /**
  * What we are searching for.
  */
  private query: string;

  constructor(instrument: InstrumentString, market: string, query: string) {
    this.instrument = instrument;
    this.market = market;
    this.query = query;
  }


  /**
   * Validate search query
   */
  public validate(): ValidationResult {
    let result: ValidationResult;

    switch (this.instrument) {
      case InstrumentString.option: result = this.validateStock(); break;
      case InstrumentString.stock: result = this.validateStock(); break;
      case InstrumentString.future: result = this.validateFutures(); break;
      case InstrumentString.forex: result = { valid: true }; break;
      case InstrumentString.mutual: result = this.validateStock(); break;
      default: result = { valid: false, message: { severity: 'error', summary: "Symbol is not valid.", detail: "" } }; break;
    };

    return result;
  }

  /**
   * Validate stocks query
   */
  private validateStock(): ValidationResult {
    let result = this.checkEmptyText(this.query);
    if (!result.valid) {
      return result;
    }
    return { valid: true };
  }

  /**
   * Validate futures query
   */
  private validateFutures(): ValidationResult {
    let result: ValidationResult;

    result = this.checkEmptyText(this.query);
    if (!result.valid) {
      return result;
    }

    if (this.query.trim().length < 2) {
      return { valid: false, message: { severity: 'error', summary: "Too short!", detail: "Please enter a longer text." } };
    }

    return { valid: true };
  }

  /**
   * Check empty query text
   * @param query
   */
  private checkEmptyText(query: string): ValidationResult {

    if (!query || query.trim().length == 0) {
      return { valid: false, message: { severity: 'warn', summary: "Empty query text", detail: "Please enter a text." } };
    }

    return { valid: true };
  }


  createSymbolSearchRequest(): SymbolSearch {
    let result: SymbolSearch = new SymbolSearch();
    result.market = Utils.prepMarket(this.market)
    result.instrument = this.instrument;
    result.query = this.query.trim();
    //result.returnToRoute = this.returnToRoute;
    return result;
  }

  createOptionSearchRequest(): OptionSearchByExpiration {
    let result: OptionSearchByExpiration = new OptionSearchByExpiration();
    result.optionType = '';
    result.underlaying = this.query.trim();
    //result.returnToRoute = this.returnToRoute;
    return result;
  }

  /**
   * This is actually called just one time. It loads all forex symbols (99 items)
   * and we store forex data in application (store or localStorage).
   */
  createForexSearchRequest(): ForexPairs {
    let result: ForexPairs = new ForexPairs();
    //result.returnToRoute = this.returnToRoute;
    return result;
  }

}
